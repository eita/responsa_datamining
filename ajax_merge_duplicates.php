<?php
/*
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);
*/
require("functions.php");
require("config.php");

if (!isset($_POST['ref']) || !isset($_POST['dup']) || !isset($_POST['mergeOptions'])) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("Not enough parameters to procede");
} else if (!check_login()) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("Login failed");

} else {
	include("init_external_data.php");

	$ref = $_POST['ref'];
	$dup = $_POST['dup'];
	$ref = get_single_initiative($ref['source'],$ref['local_id']);
	$dup = get_single_initiative($dup['source'],$dup['local_id']);
	$mergeOptions = $_POST['mergeOptions'];
	$mergedItem = $ref;
	foreach ($mergeOptions as $field=>$target) {
		if ($target == 'dup') {
			$mergedItem->{$field} = $dup->{$field};
		}
	}
	
	$refs = $config->data_sources;
	foreach ($refs as $f=>$r) {
		$refs[$f] = array();
	}
	$hidden = $mergedData = $refs;
	
	$refs[$ref->source[0]->source][$ref->local_id] = $dup->source;
	$hidden[$dup->source[0]->source][$dup->local_id] = true;
	$mergedData[$ref->source[0]->source][$ref->local_id] = $mergedItem;
	
	$ret = new StdClass();

	$updated = update_data($refs, $hidden, $mergedData, false);
	if ($updated) {
		$ret->success = true;
		$ret->msg = _("Duplicates merged!");
	} else {
		$ret->error = true;
		$ret->msg = _("There was an error doing the merge!");
	}	
	
}

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
