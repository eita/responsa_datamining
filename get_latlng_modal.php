<?php
?>
    <div class="map_addr_canvas"></div>
    
    <form>
      <input id="geocomplete" type="text" placeholder="<?= _("Type in an address") ?>" value="111 Broadway, New York, NY" />
      <input id="find" type="button" value="find" />
      
      <fieldset>
        <label><?= _("Latitude") ?></label>
        <input name="lat" type="text" value="">
      
        <label><?= _("Longitude") ?></label>
        <input name="lng" type="text" value="">
      
        <label><?= _("Formatted address") ?></label>
        <input name="formatted_address" type="text" value="">
      </fieldset>
      
      <a id="reset" href="#" style="display:none;">Reset Marker</a>
    </form>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBK1OcNx-U53bd6SPhrpLb_DL3qWlDSncI&libraries=places"></script>
    <script src="js/jquery.geocomplete.js"></script>
    
    <script>
      $(function(){
        $("#geocomplete").geocomplete({
          map: ".map_addr_canvas",
          details: "form ",
          markerOptions: {
            draggable: true
          }
        });
        
        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
          $("input[name=lat]").val(latLng.lat());
          $("input[name=lng]").val(latLng.lng());
          $("#reset").show();
        });
        
        
        $("#reset").click(function(){
          $("#geocomplete").geocomplete("resetMarker");
          $("#reset").hide();
          return false;
        });
        
        $("#find").click(function(){
          $("#geocomplete").trigger("geocode");
        }).click();
      });
    </script>
    
<?php

