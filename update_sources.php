<?php
/*********************************************/
/*********************************************/
/*********************************************/
/*             Update sources                */
/*********************************************/
/*********************************************/
/*********************************************/
function update_sources() {
	global $config;
	if (isset($_REQUEST['do_it'])) {
		if ($config->isAdmin || $config->valid_actions['update_sources']!='admin') {
			do_update_sources();
			return;
		} else {
			show_failed_login_alert();
		}
	}

	$data_sources_txt = (isset($_REQUEST['data_sources']) && $_REQUEST['data_sources'])
		? "&data_sources=".$_REQUEST['data_sources']
		: "";
	$actionUrl = '?action=update_sources'.$data_sources_txt.'&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Yes, I want to update data from source(s) now!").'</a>';
	?>
	<p><?= sprintf(_("Whenever you update sources, you change the database! Are you sure you want to continue and update the source(s) <i>%s</i>?"),implode(', ',$config->sources)) ?></p>
	<?php
	show_login($actionUrl, $btn);
}

function do_update_sources($singleSource="", $silent=false) {
	global $config;
	$unlinked = false;
	foreach ($config->data_sources as $source=>$v) {
		if ($singleSource && $source!=$singleSource) {
			continue;
		}
		if (in_array($source,$config->sources)) {
			if (!$silent) {
				?>
				<h2><?= sprintf(_("Updating %s"),$v) ?>...</h2>
				<?php
			}
			$data = new StdClass();
			$data->fetched_at = date('Y-m-d');
			$data->data = call_user_func("get_data_$source");
			if (!$data->data || isset($data->data->error)){
				if (!isset($data->data->error)) {
					$data->data = new StdClass();
					$data->data->msg = "failed";
					$data->data->error = true;
				}
				if (!$silent) {
					?>
					<div class="alert alert-danger"><?= $data->data->msg ?></div>
					<?php
				}
			} else {
				$data->data = remove_out_of_boundaries($data->data);
				if (file_exists("data/data$v.json")) {
					unlink("data/data$v.json");
				}
				if (!$unlinked && file_exists("data/data.json")) {
					unlink("data/data.json");
					$unlinked = true;
				}
				save_data("rawData$v.json", $data, !$silent);
				$config->data = get_all_initiatives("raw");
				clean_duplicates($source);

				// If only responsa is being updated, territories should be assigned immediately.
				if ($singleSource=='responsa') {
					include('assign_territory.php');
					do_assign_territory('responsa',$silent);
				}
			}
		}
	}
}


//////////// FUNCTIONS TO GET DATA FROM SOURCES //////////////////


/************************************************/
/************************************************/
/************************************************/
/*            Experiências de EAN               */
/************************************************/
/************************************************/
/************************************************/
function get_data_experiencias_ean() {
	global $config;
	$urlRoot = "http://www.ideiasnamesa.unb.br/";
	// First the name, id, title, avatar and description
	$url = $urlRoot."index.php?r=site/ExperienciasIndex&page=";
	$page = 0;
	$nextPage = true;
	$data = array();
	while ($nextPage) {
		$page++;
		$doc = new DOMDocument();
		$html = mb_convert_encoding(file_get_contents($url.$page), 'UTF-8', 'UTF-8');
		$html = str_replace(array("<br />","<br>","</br>"),"",$html);
		$doc->loadHTML($html);
		$wrapper = $doc->getElementById('experienciaslistview');
		if (!$wrapper->childNodes) {
			pR(_("I wasn\'t succesful retrieving data from")." $url");
			return false;
		}
		foreach ($wrapper->childNodes as $childNode) {
			if ($childNode->tagName=='div' && $childNode->getAttribute('class')=='items') {
				foreach ($childNode->childNodes as $item) {
					if ($item->tagName=='div') {
						$t = new StdClass();
						$t->type = 'initiative';
						$area = false;
						$experienceType = false;
						$subitens = $item->getElementsByTagName('tr');
						foreach ($subitens as $tr) {
							foreach ($tr->childNodes as $td) {
								foreach ($td->childNodes as $subTD) {
									switch ($subTD->tagName) {
										case 'a':
											if ($subTD->firstChild->tagName=='img') {
												$t->avatar = $urlRoot.$subTD->firstChild->getAttribute('src');
											}
										break;
										case 'span':
											if ($subTD->getAttribute('class')=='tituloExperiencia') {
												$t->url = $urlRoot.$subTD->firstChild->getAttribute('href');
												$t->local_id = substr($t->url,strpos($t->url,"id=")+3);
												$t->title = $subTD->nodeValue;
											} elseif ($subTD->getAttribute('class')=='subTituloExperiencia') {
												if (!isset($t->area)) {
													$t->area = true;
												} else {
													$t->experienceType = true;
												}
											}
										break;
										case 'i':
											$territory = str_replace(',',' - ',$subTD->nodeValue);
											list($t->city,$t->state,$t->country)=explode(' - ',$territory);
											$t->country = get_country_iso3166_2(trim($t->country));
										break;
										case 'p':
											$p = trim($subTD->nodeValue);
											if ($p && !$t->description) {
												$t->description = $p;
											}
										break;
										default:
											$c = trim($subTD->textContent);
											if ($c!="") {
												if (isset($t->experienceType) && !$experienceType) {
													$t->description .= ". Tipo de experiência: $c";
													$experienceType = true;
												} elseif (isset($t->area) && !$area) {
													$t->description .= " Área: $c";
													$area = true;
												}
											}
										break;
									}
								}
							}
						}
						if ($data[$t->local_id]) {
							$nextPage = false;
							break;
						} else {
							unset($t->experienceType, $t->area);
							$data[$t->local_id] = $t;
						}
					}
				}
				break;
			}
		}
	}
	// Now the latlng data:
	$url="http://www.ideiasnamesa.unb.br/index.php?r=site/MapaExperiencias";
	$html = file_get_contents($url);
	$i=strpos($html, "var jmarkers = eval(\"");
	if ($i) {
		$i+=21;
		$f = strpos($html,"\$('#map').gMap(") - $i;
		$json = substr(trim(substr($html,$i,$f)),0,-2);
		$items = my_json_decode($json);
		foreach ($items as $item) {
			preg_match('/href=(.*?) /',$item->html, $d);
			$local_id = substr($d[1],strpos($d[1],"id=")+3);
			if ($data[$local_id]) {
				$data[$local_id]->lat = $item->latitude;
				$data[$local_id]->lng = $item->longitude;
			}
		}
	}

	//pR($data);exit;
	return array_values($data);
}

/************************************************/
/************************************************/
/************************************************/
/*                  CIRANDAS                    */
/************************************************/
/************************************************/
/************************************************/
function get_data_cirandas() {
	$url = "http://cirandas.net/plugin/fbes/responsa/initiatives?per_page=all";
	$data = array();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url.$page);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$dataRaw = curl_exec($ch);
	curl_close($ch);
	$data = json_decode($dataRaw);
	foreach ($data as $i=>$item) {
		$data[$i]->type = 'initiative';
	}
	return $data;
}


/************************************************/
/************************************************/
/************************************************/
/*           Feiras Orgânicas IDEC              */
/************************************************/
/************************************************/
/************************************************/
function get_data_feiras_organicas() {
  $url = "https://feirasorganicas.org.br/wp-json/wp/v2/feiras";
	$dataRaw = curl_json($url);
	$data = array();
	foreach ($dataRaw as $i=>$item) {
		if (!isset($item->tipo) || !$item->tipo || $item->tipo=='indefinido') {
			continue;
		}
    $t = new StdClass();
		switch ($item->tipo) {
			case "feira":
				$t->category = "feiras";
			break;
			case "ponto-de-comercializacao":
				$t->category = "lojas";
			break;
			case "grupo":
				$t->category = "grupos-de-consumo-responsavel";
			break;
		}

		if (isset($item->avatar)) {
			$t->avatar = $item->avatar;
		}
		$t->url = $item->link;
		$t->local_id = $item->id;
		$t->title = $item->title->rendered;
		$t->city = $item->cidade;
		$t->state = $item->uf;
		$t->country = "BR";
		if (isset($item->horario) && $item->horario) {
			$t->description = "Dias e horários: ".$item->horario;
		}
		$t->address = $item->address;
		$t->lat = $item->lat;
		$t->lng = $item->lng;
		$data[] = $t;
	}
	return $data;
}

function curl_json($url, $per_page = 100) {
	$n = -1;
	$page = 1;
	$data = array();
	while ($n<0 || $n==100) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url."/?per_page=".$per_page."&page=".$page);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$dataRaw = curl_exec($ch);
		curl_close($ch);
		$data_page = json_decode($dataRaw);
		$data = array_merge($data, $data_page);
		$n = count($data_page);
		$page++;
	}
	return $data;
}





function get_data_feiras_organicas_antigo() {
	// Only lat/lng data:
	$html = file_get_contents($urlRoot);
	$i = strpos($html,"//OVERLAYS")+10;
	$json = substr($html,$i);
	$f = strpos($json,"//END OVERLAY");
	$json = "{".trim(substr($json,0,$f))."}";
	$json = json_clean($json);
	$items = my_json_decode($json);
	if (!$items)
		return false;
	$data = array();
	foreach ($items->overlay->values as $item) {
		$t = new StdClass();
		$t->type = 'initiative';
		$t->lat = $item->latLng[0];
		$t->lng = $item->latLng[1];
		$doc = new DOMDocument();
		$doc->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">'.$item->options->content);
		$content = $doc->getElementsByTagName('body')->item(0)->childNodes->item(0)->childNodes->item(0);
		$t->local_id = trim(str_replace("idec_marker_infos_","",$content->getAttribute('id')));
		foreach ($content->childNodes as $d) {
			if (method_exists($d,"getAttribute")) {
				$class = $d->getAttribute('class');
				$value = trim($d->nodeValue);
				switch ($class) {
					case "type_info":
						$t->description = $value;
					break;
					case "address":
						$t->address = $value;
					break;
					case "city-uf":
						list($t->city,$t->state)=explode(" - ",$value);
						$t->country = "BR";
					break;
					case "map_overlay_footer clearfix":
						$t->url = $d->getElementsByTagName('a')->item(0)->getAttribute('href');
					break;
					default:
						if ($d->tagName == "h1") {
							$t->title = $value;
						}
					break;
				}
			}
		}
		$data[] = $t;
	}
	return $data;

	// Detailed data about each initiative
	/*
	$url = $urlRoot."item/page/";
	$nextPage = true;
	$page = 0;
	while ($nextPage) {
		$page++;
		$html = file_get_contents($url.$page);
		$doc = new DOMDocument();
		$doc->loadHTML($html);
		$body = $doc->getElementsByTagName('body');
		foreach ($body as $b) {
			foreach ($b->childNodes as $bodyChild) {
				if ($bodyChild->tagName == 'div' && $bodyChild->getAttribute('class')=='contents left-side') {
					$data = array();
					foreach ($bodyChild->childNodes as $item) {
						if ($item->tagName == 'div' && strpos($item->getAttribute('class'),'feira')!==false) {
							$t = new StdClass();
							$description = new StdClass();
							foreach ($item->childNodes as $sItem) {
								if ($sItem->tagName == 'div' && $sItem->getAttribute('class')!='clearfix') {
									foreach ($sItem->childNodes as $ssItem) {
										if ($ssItem->tagName=='div') {
											switch($ssItem->getAttribute('class')) {
												case 'item-content':
													foreach ($ssItem->childNodes as $sssItem) {
														$class = (method_exists($sssItem,'getAttribute'))
															? $sssItem->getAttribute('class')
															: "";
														if (strpos($class,'title')!==false) {
															$description->type = trim($sssItem->nodeValue);
														} else if ($sssItem->tagName=='h1') {
															$t->title = trim($sssItem->childNodes->item(0)->nodeValue);
															$description->original_url = trim($sssItem->childNodes->item(1)->getAttribute('href'));
														} else if ($class=='address') {
															$t->address = trim($sssItem->nodeValue);
														} else if ($class=='city-uf') {
															list($t->city,$t->state)=explode('-',trim($sssItem->nodeValue));
															$t->country = 'BR';
														} else if ($sssItem->tagName=='p' && $sssItem->previousSibling->nodeValue=='Produtos') {
															$description->products = trim($sssItem->nodeValue);
														} else if ($class=='update_at') {
															$t->updated_at = str_replace(array('Atualizado em ','.'),'',trim($sssItem->nodeValue));
														} else if (strpos($class,'rating-result')!==false) {
															$t->local_id = trim(str_replace(array('rating-result','-'),'',$class));
															$t->url = $urlRoot."?p=".$t->local_id;
														} else if ($class=='image') {
															$t->avatar = $sssItem->firstChild->getAttribute('src');
														}
													}
												break;
												case 'image':
													$t->avatar = $ssItem->getElementsByTagName('img')->item(0)->getAttribute('src');
												break;
											}
										}
									}
								}
							}
							if ($description->type) {
								$t->description .= "Tipo: ".$description->type;
							}
							if ($description->products) {
								$t->description .= "<br />Produtos disponíveis: ".$description->products;
							}
							if ($description->original_url) {
								$t->description .= "<br />Página onde consta a informação original: ".$description->original_url;
							}
							$data[] = $t;
						}
					}
					break;
				}
			}
		}
	}
	return $data;
	*/
}





/************************************************/
/************************************************/
/************************************************/
/*                    MUDA                      */
/************************************************/
/************************************************/
/************************************************/
function get_data_muda() {
	global $config;
	$loc = get_territories();
	$url = "https://www.google.com/maps/d/viewer?mid=1ib0uq174w_SedPLwXAsDr51B2eQ";
	$html = file_get_html($url);
	$ret = $html->find('script', 3);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataDecoded = my_json_decode($pageDataRaw, false);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData do MUDA. O erro foi <i>$erro</i>.";
	} else {
		$finalAll = array();
		$cats = $pageDataDecoded[1][6];
		$item = array();
		$itemDetails = array();
		for ($i=0;$i<count($cats);$i++) {
			$item = $cats[$i][12][0][13][0];
			for ($j=0;$j<count($item);$j++) {
				$itemDetails = $item[$j][5];
				$finalItem = new StdClass();
				$finalItem->type = 'initiative';
				$finalItem->local_id = $item[$j][0];
				$finalItem->lat = $item[$j][1][0][0][0];
				$finalItem->lng = $item[$j][1][0][0][1];
				$finalItem->title = str_replace('\\u0026', '&', $itemDetails[0][1][0]);
				$finalItem->description = "";
				$finalItem->category = $cats[$i][2];
				for ($k=0;$k<count($itemDetails[3]);$k++) {
					$key = $itemDetails[3][$k][0];
					$value = trim($itemDetails[3][$k][1][0]);
					switch($key) {
						case 'Descrição':
						case 'Tipo':
							$finalItem->sub_category = $value;
						break;
						case 'ENDEREÇO':
						case 'Endereço':
							$finalItem->address = $value;
						break;
						case 'CIDADE, UF':
						case 'Cidade-UF':
							$city_uf = explode(',',$value);
							$finalItem->city = trim($city_uf[0]);
							$finalItem->state = trim($city_uf[1]);
							$finalItem->country = "BR";
						break;
						case 'TELEFONE':
						case 'Telefone':
							$finalItem->phone1 = trim(str_replace('tel:','',$value));
						break;
						case 'E-MAIL':
						case 'Email':
							$finalItem->email = $value;
						break;
						case 'SITE':
						case 'Site':
							$finalItem->website = $value;
						break;
						case 'DESCRIÇÃO':
						case 'Observação':
							$finalItem->description = str_replace('\\u0026', '&', $value);
						break;
						default:
							$finalItem->{$itemDetails[3][$k][0]} = $value;
						break;
					}
				}
				$finalItem->description = "Tipo da iniciativa: ".$finalItem->sub_category." (".$finalItem->category."). ".$finalItem->description;
				unset($finalItem->category, $finalItem->sub_category);
				$finalAll[] = $finalItem;
			}
		}

	}
	//pR($finalAll);exit;
	return $finalAll;
}

/************************************************/
/************************************************/
/************************************************/
/* Rede Brasileira de Comercialização Solidária */
/************************************************/
/************************************************/
/************************************************/
function get_data_comsol() {
	global $config;
	$states = get_territories(array('state'));
	$UFs = array();
	foreach ($states->territories as $id=>$state) {
		$UFs[] = " ".$state->alias;
		$UFs[] = " ".$state->name;
	}

	$url="https://www.google.com/maps/d/u/0/viewer?mid=zC1VoA74cPOI.kqiuH4LwN4VM";
	$html = file_get_html($url);
	$ret = $html->find('script', 3);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataDecoded = my_json_decode($pageDataRaw, false);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData do COMSOL. O erro foi <i>$erro</i>.";
	} else {
		$finalAll = array();
		$cats = $pageDataDecoded[1][6];
		$item = array();
		$itemDetails = array();
		for ($i=0;$i<count($cats);$i++) {
			$item = $cats[$i][12][0][13][0];
			for ($j=0;$j<count($item);$j++) {
				$itemDetails = $item[$j][5];
				$finalItem = new StdClass();
				$finalItem->type = 'initiative';
				$finalItem->local_id = $item[$j][0];
				$finalItem->lat = $item[$j][1][0][0][0];
				$finalItem->lng = $item[$j][1][0][0][1];
				$finalItem->title = $itemDetails[0][1][0];
				$finalItem->category = $cats[$i][2];
				for ($k=0;$k<count($itemDetails[3]);$k++) {
					$key = $itemDetails[3][$k][0];
					$value = trim($itemDetails[3][$k][1][0]);
					switch($key) {
						case 'Modalidade':
							$finalItem->description = "Iniciativa de comercialização solidária. Modalidade: $value";
						break;
						case 'Município':
							$finalItem->city = trim(str_replace($UFs,"",$value));
						break;
						case 'UF':
							$finalItem->state = trim($value);
						break;
						case 'Região':
							$finalItem->region = trim($value);
						break;
						default:
							$finalItem->{$itemDetails[3][$k][0]} = $value;
						break;
					}
				}
				$finalItem->description .= " (".$finalItem->category.")";
				unset($finalItem->category);
				$finalAll[] = $finalItem;
			}
		}
	}

	//pR($finalAll);exit;
	return $finalAll;
}


/*********************************************/
/*********************************************/
/*********************************************/
/*      Grupos de Consumo Responsável        */
/*********************************************/
/*********************************************/
/*********************************************/
function get_data_gcrs() {
	$url="https://www.google.com/maps/d/u/0/embed?hl=pt-BR&mid=zq_08AdWVTR8.k59MN_mqRYGo";
	$data = array();
	$html = file_get_html($url);
	$ret = $html->find('script', 0);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataRaw = str_replace(array("\\\"","\\\\n", "\\n","\\\\u"),array("\"","\\n","","\\u"),$pageDataRaw);

	while (strpos($pageDataRaw,',,')>0) {
		$pageDataRaw = str_replace(array(",,","[,"), array(",\"\",", "[\"\","), $pageDataRaw);
	}
	//pR($pageDataRaw);exit;
	$pageDataDecoded = json_decode($pageDataRaw);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData dos GCRs. O erro foi <i>$erro</i>.";
	} else {
		//pR($pageDataDecoded);exit;
		$key = "grupos-de-consumo-responsavel";
		$pageData = busca_recursiva_array($pageDataDecoded, $key);
		//pR($pageDataTmp);exit;
		/*$busca = false;
		foreach ($pageDataTmp as $p) {
			if ($busca && is_array($p))	{
				$pageData = busca_recursiva_array($p, $key);
				if ($pageData)
					break;
			}
			if (is_string($p) && sanitize($p)==$key)
				$busca = true;
		}*/
		//pR($pageData[12][0][13][0]);exit;
		$data = array();
		if (isset($pageData[12][0][13][0]) && is_array($pageData[12][0][13][0])) {
			foreach ($pageData[12][0][13][0] as $d) {
				//pR($d);exit;
				$t = new StdClass();
				$t->type = 'initiative';
				$t->local_id = $d[0];
				$t->lat = $d[1][0][0][0];
				$t->lng = $d[1][0][0][1];
				$t->country = "BR";
				foreach ($d[5] as $e) {
					if (is_string($e[0]) && sanitize($e[0])=='grupo-de-consumo-responsavel') {
						//pR(sanitize($e[0]));
						$t->title = $e[1][0];
					} else
					if (is_array($e[0])) {
						foreach ($e as $f) {
							//pR(sanitize($f[0]));
							switch (sanitize($f[0])) {
								case "grupo-de-consumo-responsavel":
									$t->title = $f[1][0];
								break;
								case "endereco-principal":
									$t->address = $f[1][0];
								break;
								case "cidade-estado":
									list($t->city,$t->state) = explode(" - ",$f[1][0]);
								break;
								case "site":
									$t->website = "http://" . str_replace(array("http://", "https://"),"",$f[1][0]);
								break;
							}
						}
					}
				}
				//pR($t);exit;
				$data[] = $t;
			}
		}
		//pR($data);exit;
	}
	return $data;
}




/*********************************************/
/*********************************************/
/*********************************************/
/*        Agroecologia em Rede               */
/*********************************************/
/*********************************************/
/*********************************************/
function get_data_agrorede() {
	$url="http://www.agroecologiaemrede.org.br/mapas/gera_xml_agrorede.php?c=1&f=pts&nomeFrm=frm_exp_geral&mostratabela=1&mostraTodos=1&include_images=1";
	if ($xml = simplexml_load_file($url)) {
		$data = array();
		foreach ($xml->markers->pt as $d) {
			$country = get_country_iso3166_2((string)$d['pais']);
			if ($country=='BR') {
				$t = new stdClass();
				$t->type = 'initiative';
				$t->local_id = (string)$d['id'];
				$t->url = (string)$d['url'];
				$t->title = (string)$d['nome'];
				$t->description = (string)$d['descricao'];
				$t->lat = (float)$d['lat'];
				$t->lng = (float)$d['lng'];
				$t->city = (string)$d['cidade'];
				$t->state = (string)$d['estado'];
				$t->country = $country;
				$t-> created_at = (string)$d['criacao'];
				if (isset($d['imgs']) && $d['imgs']) {
					$avatars = explode(',',(string)$d['imgs']);
					foreach ($avatars as $avatar) {
						if (in_array(substr(strtolower($avatar),-4),array('.jpg','.png','.bmp'))) {
							$t->avatar = "http://agroecologiaemrede.org.br/upload/arquivos/".$avatar;
						}
					}
				}
				$data[] = $t;
			}
		}
	}
	return $data;
}





/************************************************/
/************************************************/
/************************************************/
/*           Hortas de São Paulo                */
/************************************************/
/************************************************/
/************************************************/
function get_data_hortas_sp() {
	global $config;
	$states = get_territories(array('state'));
	$UFs = array();
	foreach ($states->territories as $id=>$state) {
		$UFs[] = " ".$state->alias;
		$UFs[] = " ".$state->name;
	}

	$url="https://www.google.com/maps/d/viewer?mid=1Jv4a5V6_DJq7c9OJSCLyF182LzU";
	$html = file_get_html($url);
	$ret = $html->find('script', 3);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataDecoded = my_json_decode($pageDataRaw, false);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData do HortasSP. O erro foi <i>$erro</i>.";
	} else {
		$finalAll = array();
		$cats = $pageDataDecoded[1][6];
		$item = array();
		$itemDetails = array();
		for ($i=0;$i<count($cats);$i++) {
			$item = $cats[$i][12][0][13][0];
			for ($j=0;$j<count($item);$j++) {
				$categoryTitle = $cats[$i][2];
				// Don't add cooperados cooperAPAs:
				if (sanitize($categoryTitle) == 'cooperados-cooperapas') {
					continue;
				}

				$itemDetails = $item[$j][5];
				$finalItem = new StdClass();
				$finalItem->type = 'initiative';
				$finalItem->local_id = $item[$j][0];
				$finalItem->lat = $item[$j][1][0][0][0];
				$finalItem->lng = $item[$j][1][0][0][1];
				$finalItem->title = $itemDetails[0][1][0];
				$finalItem->category = $categoryTitle;
				$finalItem->city = "São Paulo";
				$finalItem->state = "São Paulo";
				if (is_array($itemDetails[3])) {
					$itD = $itemDetails[3];
				} else {
					$itD = $itemDetails;
				}
				for ($k=0;$k<count($itD);$k++) {
					$key = $itD[$k][0];
					$value = trim($itD[$k][1][0]);
					switch(sanitize($key)) {
						case 'nome-da-horta':

						break;
						case 'educativacomercial':
							$finalItem->description = "Horta $value";
						break;
						case 'regiao':

						break;
						case 'bairro':
							$finalItem->bairro = $value;
						break;
						case 'endereco':
							$finalItem->address = $value;
						break;
						case 'contato':
							$w = trim(str_replace(array("Facebook ", "facebook ", "(", ")"),"", $value));
							if (filter_var($w, FILTER_VALIDATE_URL)) {
								$finalItem->website = $w;
							}
						break;
						case 'descricao':
							$value = str_replace(array("\n", chr(13), chr(10)), ". ", $value);
							while (strpos($value, "  ")>0) {
								$value = str_replace("  ", " ",$value);
							}
							$finalItem->description = $value;
						break;
						default:
							if ($key) {
								$finalItem->{$key} = $value;
							}
						break;
					}
				}

				/*if (sanitize($finalItem->category)=='associacao-de-produtores-da-zona-leste') {
					pR($itemDetails);
					pR($finalItem);exit;
				}*/

				$finalItem->description .= ($finalItem->description)
					? " (".$finalItem->category.")"
					: $finalItem->category;
				if (isset($finalItem->bairro) && $finalItem->bairro) {
					$finalItem->address .= ", ".$finalItem->bairro;
					unset($finalItem->bairro);
				}
				unset($finalItem->category);
				//pR($finalItem);	if ($j==11) { exit; }
				$finalAll[] = $finalItem;
			}
		}
	}

	return $finalAll;
}









/************************************************/
/************************************************/
/************************************************/
/*           Roteiros EcoSampa                  */
/************************************************/
/************************************************/
/************************************************/
function get_data_ecosampa() {
	global $config;
	$states = get_territories(array('state'));
	$UFs = array();
	foreach ($states->territories as $id=>$state) {
		$UFs[] = " ".$state->alias;
		$UFs[] = " ".$state->name;
	}

	$url="https://www.google.com/maps/d/viewer?mid=1-2XUw3Y3uQ9dFXHrNBG1nSHfvlM";
	$html = file_get_html($url);
	$ret = $html->find('script', 3);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataDecoded = my_json_decode($pageDataRaw, false);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData do EcoSampa. O erro foi <i>$erro</i>.";
	} else {
		$finalAll = array();
		$cats = $pageDataDecoded[1][6];
		$item = array();
		$itemDetails = array();
		for ($i=0;$i<count($cats);$i++) {
			$item = $cats[$i][12][0][13][0];
			for ($j=0;$j<count($item);$j++) {
				$categoryTitle = $cats[$i][2];
				// Only consider "pontos ecosampa":
				if (sanitize($categoryTitle) != 'pontos-ecosampa') {
					continue;
				}

				$itemDetails = $item[$j][5];
				$finalItem = new StdClass();
				$finalItem->type = 'initiative';
				$finalItem->local_id = $item[$j][0];
				$finalItem->lat = $item[$j][1][0][0][0];
				$finalItem->lng = $item[$j][1][0][0][1];
				$finalItem->title = str_replace('\\u0026', '&', $itemDetails[0][1][0]);
				$finalItem->city = "São Paulo";
				$finalItem->state = "São Paulo";
				if (is_array($itemDetails[3])) {
					$itD = $itemDetails[3];
				} else {
					$itD = $itemDetails;
				}
				for ($k=0;$k<count($itD);$k++) {
					$key = $itD[$k][0];
					$value = trim($itD[$k][1][0]);
					switch(sanitize($key)) {
						case 'local':
						break;
						case 'endereco':
							$finalItem->address = $value;
						break;
						case 'telefone':
							$finalItem->phone1 = $value;
						break;
						case 'site-email':
							if (strpos($value,"@")>0) {
								$finalItem->email = $value;
							} else {
								$finalItem->website = $value;
							}
						break;
						case 'descricao':
							while (strpos($value, "  ")>0) {
								$value = str_replace("  ", " ",$value);
							}
							$finalItem->description = str_replace('\\u0026', '&', $value);
						break;
						default:
							if ($key) {
								$finalItem->{$key} = $value;
							}
						break;
					}
				}
				$finalAll[] = $finalItem;
			}
		}
	}

	return $finalAll;
}







/************************************************/
/************************************************/
/************************************************/
/*        Orgânicos no campo e na mesa          */
/************************************************/
/************************************************/
/************************************************/
function get_data_organicos_no_campo_e_na_mesa() {
	global $config;
	$states = get_territories(array('state'));
	$UFs = array();
	foreach ($states->territories as $id=>$state) {
		$UFs[] = " ".$state->alias;
		$UFs[] = " ".$state->name;
	}

	$url="https://www.google.com/maps/d/viewer?mid=1sDjsmqOvshSUjVhaggCOA39Y2Ps";
	$html = file_get_html($url);
	$ret = $html->find('script', 3);
	$pageDataRaw = substr($ret, strpos($ret,'_pageData = "')+13,strpos($ret,'";')-strpos($ret,'_pageData = "')-13);
	$pageDataDecoded = my_json_decode($pageDataRaw, false);
	if (!$pageDataDecoded) {
		$erro = json_last_error_msg();
		echo "Não consegui fazer o parse da variável _pageData do OrganicosCampoMesa. O erro foi <i>$erro</i>.";
	} else {
		$finalAll = array();
		$cats = $pageDataDecoded[1][6];
		$item = array();
		$itemDetails = array();
		for ($i=0;$i<count($cats);$i++) {
			$item = $cats[$i][12][0][13][0];
			for ($j=0;$j<count($item);$j++) {
				$categoryTitle = $cats[$i][2];
				// Only consider "restaurantes":
				if (!in_array(sanitize($categoryTitle), array('restaurantes-bares-e-lanchonetes'))) {
					continue;
				}

				$itemDetails = $item[$j][5];
				$finalItem = new StdClass();
				$finalItem->type = 'initiative';
				$finalItem->local_id = $item[$j][0];
				$finalItem->lat = $item[$j][1][0][0][0];
				$finalItem->lng = $item[$j][1][0][0][1];
				$finalItem->title = $itemDetails[0][1][0];
				$finalItem->city = "São Paulo";
				$finalItem->state = "São Paulo";
				if (is_array($itemDetails[3])) {
					$itD = $itemDetails[3];
				} else {
					$itD = $itemDetails;
				}
				for ($k=0;$k<count($itD);$k++) {
					$key = $itD[$k][0];
					$value = trim($itD[$k][1][0]);
					switch(sanitize($key)) {
						case 'restaurante':
						break;
						case 'regiao':
						break;
						case 'regiao-zona':
						break;
						case 'facebook':
							$finalItem->facebook = $value;
						break;
						case 'endereco':
							$finalItem->address = $value;
						break;
						case 'telefone':
							$finalItem->phone1 = $value;
						break;
						case 'site':
							$finalItem->website = $value;
						break;
						case 'contato':
							$finalItem->email = strtolower($value);
						break;
						case 'descricao':
							while (strpos($value, "  ")>0) {
								$value = str_replace("  ", " ",$value);
							}
							$finalItem->description = $value;
						break;
						default:
							if ($key) {
								$finalItem->{$key} = $value;
							}
						break;
					}
				}
				//pR($finalItem);exit;
				$finalAll[] = $finalItem;
			}
		}
	}

	return $finalAll;
}




/*********************************************/
/*********************************************/
/*********************************************/
/*                Responsa                   */
/*********************************************/
/*********************************************/
/*********************************************/
function get_data_responsa() {
	global $config;
	$params = new StdClass();
	$params->types = array('initiative');
	$params->sources = array('responsa');
	$params->published = true;
	$data = get_initiatives_from_DB($params, 'api');
	if (!isset($data->error) && is_array($data) && $data) {
		$items = array();
		foreach ($data as $item) {
			$t = new StdClass();
			foreach($config->fieldsToUpdate as $dbField=>$dataminingField) {
				$t->{$dataminingField} = $item->{$dataminingField};
			}
			$items[] = $t;
		}
		return $items;
	} else {
		return $data;
	}
}

/************************************************/
/************************************************/
/************************************************/
/*          AGRICULTURA URBANA                  */
/************************************************/
/************************************************/
/************************************************/
function get_data_agricultura_urbana() {
	$url = "http://movimentonossabrasilia.eita.org.br/wp-json/wp/v2/experiencias_au?_embed";
	$data = array();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url.$page);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$dataRaw = curl_exec($ch);
	curl_close($ch);
	$dataRaw = json_decode($dataRaw);
	$data = array();
	foreach ($dataRaw as $i=>$item) {
	    $t = new StdClass();
		$t->type = 'initiative';
		$t->local_id = $item->id;
		$t->url = $item->guid->rendered;
		$t->title = $item->title->rendered;
		$t->description = strip_tags($item->content->rendered);
		$t->lat = $item->acf->location->lat;
		$t->lng = $item->acf->location->lng;
		$t->address = $item->acf->location->address;
		$t->phone1 = $item->acf->telefone;
		$t->email = $item->email;
		$t->website = $item->acf->website;
		$t->facebook = $item->acf->facebook;
		$t->city = $item->acf->cidade;
		$t->state = $item->acf->uf;
		$t->created_at = $item->date;
		$t->avatar = $item->_embedded->{'wp:featuredmedia'}[0]->media_details->sizes->large->source_url;
		$data[] = $t;
	}
	pR($data);exit;
	return $data;
}




/*********************************************/
/*********************************************/
/*********************************************/
/*    Fóruns de Economia Solidária           */
/*********************************************/
/*********************************************/
/*********************************************/
function get_data_fbes() {
	$url="http://www.fbes.org.br/farejadores/gera_xml_foruns.php?Q3=0|mapa=1";
	if ($xml = simplexml_load_file($url)) {
		$data = array();
		foreach ($xml->markers->pt as $d) {
			$t = new stdClass();
			$t->type = 'initiative';
			$m = new stdClass();
			list($t->title,$t->state,$t->created_at,$abrangencia,$has_executive_secretariat,$ees,$eaf,$gestores,$contacts_name,$contacts_phone,$contacts_email,$details,$t->updated_at,$t->url) = explode('|',(string)$d['msg']);
			$contacts_name = explode("\n",$contacts_name);
			$contacts_phone = explode("\n",$contacts_phone);
			$contacts_email = explode("\n",$contacts_email);
			$contacts = array();
			foreach ($contacts_name as $i=>$name) {
				$ctc = new stdClass();
				$ctc->name = $name;
				$ctc->phone = $contacts_phone[$i];
				$ctc->email = $contacts_email[$i];
				$contacts[] = $ctc;
			}
			$t->title = "Fórum de Economia Solidaria ".$abrangencia.": ".$t->title;

			$tmp = trim(str_replace(array("economia","solidaria","--","forum","de","da","do",$abrangencia),"",sanitize($t->title)));
			pR($tmp);

			$t->description = "";
			if (count($contacts)) {
				$t->description .= "Contatos: ";
				$cont = array();
				foreach ($contacts as $i=>$contact) {
					if ($contact->name) {
						$cont[$i] = $contact->name;
						if ($contact->phone || $contact->email) {
							$conttmp = array();
							if ($contact->phone) {
								$conttmp[] = "Tel: ".$contact->phone;
							}
							if ($contact->email) {
								$conttmp[] = "Email: ".$contact->email;
							}
							$cont[$i] .= " (".implode(" - ",$conttmp).")";
						}
					}
				}
				$t->description .= implode(', ',$cont)."\n";
			}
			if ($has_executive_decretariat) {
				$t->description .= "Secretaria executiva? ".$has_executive_secretariat."\n";
			}
			if ($details) {
				$t->description .= "Observações: ".$details."\n";
			}
			$t->local_id = (string)$d['id'];
			$t->lat = (float)$d['lat'];
			$t->lng = (float)$d['lng'];
			$t->country = "BR";
			$data[] = $t;
		}
	}
	return $data;
}

?>
