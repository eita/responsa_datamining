<?php

function review_challenge_answers() {
	global $config;
	?>
		<h1><?= _("Review challenge answers") ?></h1>
	<?php
	if ($config->isAdmin || $config->valid_actions['review_challenge_answers']!='admin') {
		do_review_challenge_answers();
		return;
	} elseif (isset($_REQUEST['do_it'])) {
		show_failed_login_alert();
	}
	?>
		<p><?= _("Here you can see the latest answers to challenges") ?></p>
	<?php
	$actionUrl = '?action=review_challenge_answers&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Review challenge answers").'</a>';
	show_login($actionUrl, $btn);
}

function do_review_challenge_answers() {
	global $config;
	$items = show_challenge_answers_grouped_by_challenge();
	if (isset($items->error)) {
		?>
		<div class="alert alert-danger">
  		Erro! <i><?= $items->error ?></i>
		</div>
		<?php
		return $items;
	}
}
?>
