<?php
if ((!isset($_GET['dup_local_id']) && !$_GET['dup_local_id']) ||
			(!isset($_GET['dup_source']) && !$_GET['dup_source']) ||
			(!isset($_GET['ref_local_id']) && !$_GET['ref_local_id']) ||
			(!isset($_GET['ref_source']) && !$_GET['ref_source'])
	) {
	return false;
}
$dup = array($_GET['dup_source'], $_GET['dup_local_id']);
$ref = array($_GET['ref_source'], $_GET['ref_local_id']);
include("functions.php");
include("config.php");
include("init_external_data.php");
$dup = get_single_initiative($dup[0],$dup[1]);
$ref = get_single_initiative($ref[0],$ref[1]);

$ret = make_html_duplicate_merger_modal($ref, $dup);

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
