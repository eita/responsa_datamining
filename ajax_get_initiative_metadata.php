<?php
if (!isset($_GET['unique_id']) && !$_GET['unique_id']) {
	return false;
}
list($source, $local_id) = explode('__',$_GET['unique_id']);
include("config.php");
include("functions.php");
include("init_external_data.php");
$item = get_single_initiative($source,$local_id);
$ret = generate_metadata($item);

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
