<?php

function find_duplicates() {
	global $config;
	?>
	<h1><?= _("Find duplicated items") ?></h1>
	<?php
	$do_it = isset($_REQUEST['do_it']);

	if ($do_it) {
		if ($config->isAdmin || $config->valid_actions['find_duplicates']!='admin') {
			foreach($config->data_sources as $f=>$v) {
				if (!$config->data[$f]->territory_assignment) {
					?>
					<div class="alert alert-danger">
						<p><?= _("There is at least one of your databases from the sources that has not been assigned territories yet. Please assign the territories before looking for duplicates in and among databases.") ?></p>
						<p><a class="btn btn-lg btn-success" href="?action=assign_territory"><?= _("Assign territories") ?></a></p>
					</div>
					<?php
					return;
				}
			}
			do_find_duplicates();
			return;
		} else {
			show_failed_login_alert();
		}
	}

	?>
	<p><?= sprintf(_("Here we will check all the items from all <label>%s databases</label> to see if there are duplicates in and among the databases. We will then give an opportunity for you to decide which one will be the \"parent\" of the duplicate(s) and then we will save this update to the json files that serve both the wordpress and the Responsa app."), count($config->data)) ?></p>
	<?php
		$actionUrl = '?action=find_duplicates&do_it=1';
		$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Find duplicate items now!").'</button>';
		$manual_chooser_html = "<div class='checkbox'><label><input type='checkbox' name='manual' value='1'> "._('Search manually only')."</label></div>";
		show_login($actionUrl, $btn, $manual_chooser_html);
	?>
	<!-- It's too dangerous to let the user click the 2 buttons below...
	<p>
		<a class="btn btn-lg btn-success" href="?action=find_duplicates&do_it=1&reset_duplicates=1"><?= _("Reset marked duplicates and find them") ?></a>
		<a class="btn btn-lg btn-success" href="?action=find_duplicates&do_it=1&clean_duplicates=1"><?= _("Clean duplicates file against database") ?></a>
	</p>
	-->
	<?php
}

function do_find_duplicates() {
	global $config;
	$loc = get_territories();
	if (isset($_GET['reset_duplicates'])) {
		reset_duplicates();
	} elseif (isset($_GET['clean_duplicates'])) {
		show_alert(clean_all_duplicates());
		return true;
	}
	$manual = (isset($_REQUEST['manual']) && $_REQUEST['manual']);
	$single_territory = (isset($_GET['single_territory'])) ? $_GET['single_territory'] : false;

	// Here I get initiatives organized per city
	$items = get_all_initiatives_organized_by_territory();
	// If the user chose to do the search manually, we have to show ALL items, but this is too much. So we first show the territorys and the user picks the territory she wants to check:
	if ($manual && !$single_territory) {
		do_choose_territory($items, $loc);
		return true;
	}

	// Let's show a dropdown to define the narrowing percentage of the initiatives:
	$max_percentage_for_duplicates = (isset($_GET['max_percentage_for_duplicates']) && $_GET['max_percentage_for_duplicates']>0)
		? $_GET['max_percentage_for_duplicates']
		: $config->max_percentage_for_duplicates;
	if (!$manual) {
		?>
		<form id="select_percentage" class="form-horizontal">
			<div class="form-group">
				<label for="inputPercentageLabel" class="col-sm-2 control-label"><?= _("Degree of similarity") ?>:</label>
				<div class="col-sm-10">
					<select class="form-control" name="max_percentage_for_duplicates" id="max_percentage_for_duplicates">
						<?php for($p=60;$p<=100;$p+=10) { ?>
							<?php $sel = ($p==$max_percentage_for_duplicates) ? " selected = 'selected'" : ""; ?>
							<option value="<?= $p ?>"<?= $sel ?>><?= $p ?>%</option>
						<?php } ?>
					</select>
					<input type="hidden" name="action" value="find_duplicates">
					<input type="hidden" name="do_it" value="1">
				</div>
			</div>
		</form>
		<?php
	}

	?>
	<div class="modal fade modal-wide" id="duplicates-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title text-primary" id="feature-title"><?= _('Merge duplicates') ?></h4>
        </div>
        <div class="modal-body" id="duplicates-body"></div>
        <div class="modal-footer">
        	<button class="btn btn-sm btn-success" id="duplicates-modal-submit"><?= _("Merge now!") ?></button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
	<?php


	// Let's check duplicates in same territory_id:
	$refs = $config->data_sources;
	foreach ($refs as $i=>$r) {
		$refs[$i] = array();
	}
	$hidden = $refs;
	$state = "";
	foreach ($items as $city_id=>$local_items) {
		if ($single_territory && $city_id != $single_territory) {
			continue;
		}

		if (count($local_items)>1) {
			$duplicates = $possible_duplicates = $found = array();
			foreach ($local_items as $i=>$item_i) {
				for ($j=$i+1;$j<count($local_items);$j++) {
					$item_j = $local_items[$j];
					similar_text($item_i->title_slug,$item_j->title_slug,$percentage);
						/*if ($percentage==100) {
							if (isset($duplicates[$i])) {
								$duplicates[$i][$j] = true;
							} else if (isset($duplicates[$j])) {
								$duplicates[$j][$i] = true;
							} else {
								$duplicates[$i] = array();
								$duplicates[$i][$j] = true;
							}
						} else */ if ($percentage>=$max_percentage_for_duplicates && !$manual) {
							if (
								(isset($possible_duplicates[$i])) &&
								(!isset($possible_duplicates[$j]) || !$possible_duplicates[$j][$i])
							) {
								$possible_duplicates[$i][$j] = true;
							} else if (isset($possible_duplicates[$j])) {
								$possible_duplicates[$j][$i] = true;
							} else if (!isset($possible_duplicates[$j][$i]) || !$possible_duplicates[$j][$i]) {
								$possible_duplicates[$i] = array();
								$possible_duplicates[$i][$j] = true;
							}
						} else if ($manual && $single_territory) {
							if (
								(isset($possible_duplicates[$i])) &&
								(!isset($possible_duplicates[$j]) || !$possible_duplicates[$j][$i])
							) {
								$possible_duplicates[$i][$j] = true;
							} else if (isset($possible_duplicates[$j])) {
								$possible_duplicates[$j][$i] = true;
							} else if (!$possible_duplicates[$j][$i]) {
								$possible_duplicates[$i] = array();
								$possible_duplicates[$i][$j] = true;
							}
						}
				}
			}
			if ($possible_duplicates) {
				// First we might have to clean up the possible_duplicates array:
				foreach ($possible_duplicates as $i=>$ds) {
					foreach ($ds as $j=>$bool) {
						foreach ($ds as $jj=>$booll) {
							if (isset($possible_duplicates[$j][$jj])) {
								unset ($possible_duplicates[$j]);
							}
						}
					}
				}

				$grid = array();
				$found[$city_id] = true;
				foreach ($possible_duplicates as $i=>$ds) {
					$dup_local_id = $local_items[$i]->local_id;
					$dup_source = $local_items[$i]->source[0]->source;
					$t = array();
					$t[] = make_html_card($local_items[$i], array('<input data-territory-id="'.$city_id.'" data-dup-source="'.$dup_source.'" data-dup-local-id="'.$dup_local_id.'" data-list-highlight="true" data-list-value-completion="true" data-list-filter="!" class="static-remote" type="text" />'));
					foreach ($ds as $j=>$bool) {
						$dup_local_id = $local_items[$j]->local_id;
						$dup_source = $local_items[$j]->source[0]->source;
						$t[] = make_html_card($local_items[$j], array('<input data-territory-id="'.$city_id.'" data-dup-source="'.$dup_source.'" data-dup-local-id="'.$dup_local_id.'" data-list-highlight="true" data-list-value-completion="true" data-list-filter="!" class="static-remote" type="text" />'));
					}
					$grid[] = $t;
				}
			}
			if ($duplicates) {
				// First we might have to clean up the duplicates array:
				foreach ($duplicates as $i=>$ds) {
					foreach ($ds as $j=>$bool) {
						foreach ($ds as $jj=>$booll) {
							if (isset($duplicates[$j][$jj])) {
								unset ($duplicates[$j]);
							}
						}
					}
				}
				foreach ($duplicates as $i=>$ds) {
					$ref_i = $config->duplicates_hierarchy[$local_items[$i]->source[0]->source];
					$ref = $ref_i;
					$ref_item = $i;
					foreach ($ds as $j=>$bool) {
						$ref_j = $config->duplicates_hierarchy[$local_items[$j]->source[0]->source];
						if ($ref_j>$ref) {
							$ref = $ref_j;
							$ref_item = $j;
						}
					}
					$sources = array();
					if ($i!=$ref_item) {
						$t = new StdClass();
						$t->source = $local_items[$i]->source[0]->source;
						$t->local_id = $local_items[$i]->local_id;
						$sources[] = $t;
						$hidden[$t->source][$t->local_id] = true;
					}
					foreach ($ds as $j=>$bool) {
						if ($j!=$ref_item) {
							$t = new StdClass();
							$t->source = $local_items[$j]->source[0]->source;
							$t->local_id = $local_items[$j]->local_id;
							$sources[] = $t;
							$hidden[$t->source][$t->local_id] = true;
						}
					}
					$ref_source = $local_items[$ref_item]->source[0]->source;
					$ref_local_id = $local_items[$ref_item]->local_id;
					if (isset($refs[$ref_source][$ref_local_id])) {
						$refs[$ref_source][$ref_local_id] = array_merge($refs[$ref_source][$ref_local_id],$sources);
					} else {
						$refs[$ref_source][$ref_local_id] = $sources;
					}
				}
			}
		}
		// Let's now show the possible duplicates, which might or not be real duplicates:
		if (isset($found[$city_id]) && $found[$city_id]) {
			$current_state = substr($city_id,0,2);
			if (!$single_territory && $state!=$current_state) {
				$state = $current_state;
				?>
				<hr />
				<h2><?= $loc->territories[$state]->name ?></h2>
				<?php
			}
			?>
			<div class="box col-lg-12" style="margin-bottom:40px">
				<div class="box-inner">
					<div class="box-header well">
						<h2 class="title">
							<?= $loc->territories[$city_id]->name ?>/<?= $loc->territories[$state]->alias ?>
						</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php show_grid($grid); ?>
					</div>
				</div>
			</div>
		<?php
		}
	}
	?>
	<script>
		$(document).ready(function () {
			$('input.static-remote').remoteList({
				source: 'ajax_search_initiatives.php',
				select: function(){
					if(window.console){
						var ref = $(this).remoteList('selectedData');
						var dup = {"source": $(this).attr('data-dup-source'), "local_id": $(this).attr('data-dup-local-id')};
						if (ref.source==dup.source && ref.local_id==dup.local_id) {
							noty({"text":"<?= _('You can\'t merge two identical items!') ?>","layout":"top","type":"error","timeout":1000});
						} else {
							openDuplicatesModal(ref, dup);
						}
					}
				}
			});
		});
		</script>
	<?php
	/* Save information about duplicates among different databases and also visibility (remove duplicates from same database):
	?>
	<div class="row">
		<div class="col-lg-12">
			<?php update_data($refs, $hidden, null); ?>
		</div>
	</div>
	<?php
	*/
	if ($manual) {
		do_choose_territory($items, $loc);
	}

}

function do_choose_territory($items, $loc=false) {
	global $config;
	if (!$loc) {
		$loc = get_territories();
	}
	?>
	<h2><?= _("Choose territory to look for duplicates") ?></h2>
		<?php
		$state = "";
		foreach ($items as $city_id=>$local_items) {
			if (count($local_items)<=1) {
				continue;
			}

			$current_state = substr($city_id,0,2);
			if ($state != $current_state) {
				if ($state) {
					?></div><?php
				}
				$state = $current_state;
				?>
				<h3><?= $loc->territories[$current_state]->name ?></h3>
				<div class="clearfix">
				<?php
			}
			?>
			<div style="display:inline-block;margin:5px;"><a class="btn btn-small btn-warning" href="?action=find_duplicates&do_it=1&manual=1&single_territory=<?= $city_id ?>"><?= $loc->territories[$city_id]->name ?></a></div>
			<?php
		}
}

?>
