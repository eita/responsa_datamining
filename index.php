<?php
/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);*/
	require('header.php');
	$con = connect();
	if (!$con->error) {
		$config->number_initiatives = count_initiatives();
	}
?>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Início</a>
        </li>
    </ul>
</div>
<?php if (!isset($config->action)) { ?>
	<?php if (!$config->login) { ?>
		<div class="row">
			<div class="col-md-12">
				<?php
				$actionUrl = '';
				$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Enter").'</a>';
				show_login($actionUrl, $btn);
				?>
			</div>
		</div>
	<?php } else { ?>
		<div class=" row" id="dashboardDataLoading">
			<div class="col-md-12 text-center">
				<div class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> <?= _("loading data...") ?></div><br />
			</div>
		</div>
		<div class=" row" id="dashboardData">
				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Show initiatives")?>" class="well top-block" href="?action=show_initiatives">
				        <i class="glyphicon glyphicon-star blue"></i>
				        <div>
				        	<span id="dashboardDataInitiativesCount"></span> 
				        	<?=_("initiatives")?> <?= _("in") ?> 
				        	<span id="dashboardDataInitiativesCitiesCount"></span> 
				        	<?= _("cities") ?> <?= _("from") ?>
				        	<span id="dashboardDataInitiativesSourcesCount"></span> 
				        	<?= _("sources") ?>
				        </div>
				        <div class="small"><?= _("Latest approved/edited") ?>:<br /> <span id="dashboardDataInitiativesStatus"></span></div>
				    </a>
				</div>

				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Show users")?>" class="well top-block" href="?action=show_users">
				        <i class="glyphicon glyphicon-user blue"></i>
				        <div>
				        	<span id="dashboardDataUsersCount"></span> 
				        	<?=_("users")?> <?= _("from") ?> 
				        	<span id="dashboardDataUsersCitiesCount"></span> 
				        	<?= _("cities") ?>
				        </div>
				        <div class="small"><?= _("Latest user") ?>:<br /> <span id="dashboardDataUsersStatus"></span>, <span id="dashboardDataUsersTime"></span></div>
				    </a>
				</div>
				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Review challenge answers")?>" class="well top-block" href="?action=review_challenge_answers">
				        <i class="glyphicon glyphicon-flash yellow"></i>
				        <div>
				        	<span id="dashboardDataChallengesCount"></span> 
				        	<?=_("challenges")?> <?= _("with") ?> 
				        	<span id="dashboardDataChallengeAnswersCount"></span> 
				        	<?= _("answers") ?>
				        </div>
				        <div class="small"><?= _("Latest challenge answer") ?>:<br /> <span id="dashboardDataChallengeAnswersStatus"></span>, <span id="dashboardDataChallengeAnswersTime"></span></div>
				    </a>
				</div>
				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Review comments")?>" class="well top-block" href="?action=review_comments">
				        <i class="glyphicon glyphicon-comment yellow"></i>
				        <div>
				        	<span id="dashboardDataCommentsCount"></span> 
				        	<?=_("comments")?>
				        </div>
				        <div class="small"><?= _("Latest comment") ?>:<br /> <span id="dashboardDataCommentsStatus"></span>, <span id="dashboardDataCommentsTime"></span></div>
				    </a>
				</div>
				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Review initiatives")?>" class="well top-block" href="?action=review_initiatives">
				        <i class="glyphicon glyphicon-time green"></i>
				        <div>
				        	<span id="dashboardDataUnreviewedInitiativesCount"></span> 
				        	<?=_("unreviewed initiatives")?>
				        </div>
				        <div class="small"><?= _("Latest suggestion") ?>:<br /> <span id="dashboardDataUnreviewedInitiativesStatus"></span>, <span id="dashboardDataUnreviewedInitiativesTime"></span></div>
				    </a>
				</div>
				<div class="col-md-4 col-sm-12">
				    <a data-toggle="tooltip" title="<?=_("Review feedbacks")?>" class="well top-block" href="?action=review_feedbacks">
				        <i class="glyphicon glyphicon-question-sign white"></i>
				        <div>
				        	<span id="dashboardDataPendingFeedbacksCount"></span> 
				        	<?=_("pending feedbacks")?>
				        </div>
				        <div class="small"><?= _("Latest feedback") ?>:<br /> <span id="dashboardDataPendingFeedbacksStatus"></span>, <span id="dashboardDataPendingFeedbacksTime"></span></div>
				    </a>
				</div>
		</div>
		<div>
				<ul class="breadcrumb">
				    <li>
				        <a href="#"><?= _("Initiatives per source") ?></a>
				    </li>
				</ul>
		</div>
		<div class="row">
		<?php 
			require('init_external_data.php');
			$i=0;
			foreach ($config->data as $sourceSlug=>$data) {
				$i++;
				/*$newLine = ($i==0 || intval($i/4) == $i/4);
				if ($newLine) { ?>
					<div class="row">
				<?php } */?>
				<div class="col-md-3 col-sm-6 col-xs-12">
						<a data-toggle="tooltip" title="" class="well top-block" href="#">
						    <i class="glyphicon  blue"></i>
						    <div><?=_($config->data_sources[$sourceSlug])?></div>
						    <div><?= number_format_i18(count($data->data)) ?></div>
						</a>
				</div>
				<?php /*if ($newLine) { ?>
					</div>
				<?php } */?>
		<?php } ?>
		</div>
	<?php } ?>
<?php } ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><?= ($config->action) ? $config->main_menu[$config->action][0] : "Início" ?></h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                		<?php require('init.php'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php'); ?>
