<?php

/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);
*/
header("Access-Control-Allow-Origin: *"); 

include("functions.php");
include("config.php");
//$res = get_all_initiatives("geoJSON");
$params = new StdClass();
$params->published = 1;
$params->types = array('initiative');
if (isset($_REQUEST['territoryId']) && $_REQUEST['territoryId']) {
	$params->territoryId = $_REQUEST['territoryId'];
}
$type = (isset($_REQUEST['raw']))
	? "geoJSONRaw"
	: "geoJSON";
	
if (isset($_REQUEST['categories'])) {
	$params->categories = explode(",",$_REQUEST['categories']);
}


$res = get_initiatives_from_DB($params, $type);
if (isset($_GET['callback']) && $_GET['callback']) {
	echo $_GET['callback']."(".json_encode($res).");";
} else if (isset($_GET['download']) && $_GET['download']) {
	$date = new DateTime();
	$fDate = $date->format('Y-m-d');
	header("Content-type: application/text; charset=utf-8");
  header("Content-Disposition: attachment; filename=\"dados_responsa_$fDate.js\"");
  echo json_encode($res);
} else {
	@header("Content-type: application/json; charset=utf-8");
	echo json_encode($res);
}
?>
