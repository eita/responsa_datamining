<?php

function review_initiatives() {
	global $config;
	?>
		<h1><?= _("Review initiatives") ?></h1>
	<?php
	if ($config->isAdmin || $config->valid_actions['review_initiatives']!='admin') {
		do_review_initiatives();
		return;
	} elseif (isset($_REQUEST['do_it'])) {
		show_failed_login_alert();
	}
	?>
		<p><?= _("Here you can review all unreviewed items suggested by users") ?></p>
	<?php
	$actionUrl = '?action=review_initiatives&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Review initiatives").'</a>';
	show_login($actionUrl, $btn);
}

function do_review_initiatives($objectId=null) {
	global $config;
	$category = null;
	if (!$objectId && isset($_REQUEST['category']) && $_REQUEST['category']) {
	    $category = $_REQUEST['category'];
	}
	$items = get_unreviewed_initiatives(false, $objectId, null, true, $category);
	if (isset($items->error)) {
		?>
		<div class="alert alert-danger">
  		Erro! <i><?= $items->error ?></i>
		</div>
		<?php
		return $items;
	}
	?>
	<!--
	<div class="modal fade modal-wide" id="get-latlng-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title text-primary" id="feature-title"><?= _('Get latitude/longitude') ?></h4>
        </div>
        <div class="modal-body">
        	<?php include('get_latlng_modal.php'); ?>
        </div>
        <div class="modal-footer">
        	<button class="btn btn-sm btn-success"><?= _("Close") ?></button>
        </div>
      </div>
    </div>
  </div>
  -->
    <form class="form-horizontal" id="filter_review_initiatives" action="?action=review_initiatives&do_it=1" method="post">
        <?= make_review_initiative_select(null, 'category', $category, _("Filter category"), 'firstHalf', true); ?>
    		<button type="submit" class="btn btn-default"><?= _('apply filter') ?></button>
    	</div>
    </form>
    <hr>
	<?= show_table($items, "review_initiatives") ?>
	<?php
}
?>
