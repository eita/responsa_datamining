<?php

function assign_categories() {
	global $config;
	?>
	<h1><?= _("Assign categories") ?></h1>
	<?php
	$items = get_all_initiatives_organized_by_territory('city');
	if (isset($_REQUEST['do_it'])) {
		if ($config->isAdmin || $config->valid_actions['assign_categories']!='admin') {
			if (isset($_POST['state_id']) && $_POST['state_id']) {
				do_assign_categories($items);
				return;
			}
		} else {
			show_failed_login_alert();
		}
	}
	$territory_chooser_html = do_choose_territory($items)
	?>
	<p><?= _("Here is your opportunity to assign categories to all data coming from external sources to have them better prepared for usage in Responsa and in Encontre.") ?></p>
	<?php
		$actionUrl = '?action=assign_categories&do_it=1';
		$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Assign categories").'</button>';
		show_login($actionUrl, $btn, $territory_chooser_html);
	?>
	<!--
	<h2><?= _("Status") ?></h2>
	<table class="table">
		<?php foreach ($config->data_sources as $f=>$v) { ?>
		<tr>
		<?php
			$ninit = count_initiatives($f);
			if ($ncat = count($config->categories_assigned[$f])) {
				$txt = sprintf(_("Database <label>%s</label>:You have %s initiatives categorized out of %s"),$v,$ncat,$ninit);
			} else {
				$txt = sprintf(_("Database <label>%s</label>:You have no initiatives categorized!"),$v);
			}
		?>
			<td><?= $txt ?></td>
			<td><a class="btn btn-sm btn-success" href="?action=assign_categories&do_it=1&data_source=<?= $f ?>"><?= sprintf(_("Assign categories from %s"), $v) ?></a></td>
		</tr>
		<?php } ?>
	</table>
	-->
	<?php
}
	
function do_assign_categories($items) {
	global $config;
	$loc = get_territories();
	$states = array();
	if (count($_POST['state_id'])==1) {
		$states[$_POST['state_id'][0]] = $loc->territories[$_POST['state_id'][0]]->name;
	} else {
		foreach ($_POST['state_id'] as $state_id) {
			$states[$state_id] = $loc->territories[$state_id]->alias;
		}
	}
	
	?>
	<h3><?= (count($states)==1) ? _('Categorizing the state') : _('Categorizing states') ?> <?= implode(', ',$states) ?></h3>
	<?php
	$table = array();
	foreach ($items as $territory_id=>$local_items) {
		if (!in_array(substr($territory_id,0,2), array_keys($states))) {
			continue;
		}
		foreach ($local_items as $item) {
			$t = new StdClass();
			$t->category = show_category_radiobox($item->source[0]->source,$item->source[0]->source."-".$item->local_id, $item->category,"responsa-category");
			$t->title = ($item->url)
				? "<a href='".$item->url."'>".$item->title."</a>"
				: $item->title;
			$t->sources = get_labeled_sources($item->source);
			$t->territory = $item->territory_label;
			$t->details = make_html_cell($item);
			$table[] = $t;
		}
	}
	show_table($table, "assign_categories");
}

function do_choose_territory($items, $loc=false) {
	global $config;
	$loc = get_territories();
	$html = "<br /><div class='clearfix'>";
	$state = "";
	$ordered_items = array();
	foreach ($items as $territory_id=>$local_items) {
		$current_state = substr($territory_id,0,2);
		if ($state != $current_state) {
			$ordered_items[$loc->territories[$current_state]->slug] = "<div class='checkbox-inline'><label><input type='checkbox' name='state_id[]' value='$current_state'> ".$loc->territories[$current_state]->name."</label></div>";
		}
	}
	ksort($ordered_items);
	foreach ($ordered_items as $o) {
		$html .= $o;
	}
	$html.="</div><br /><br />";
	return $html;
}

?>
