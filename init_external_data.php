<?php 
$config->duplicates = get_all_duplicates();
$config->data = get_all_initiatives("raw");
//$config->number_initiatives=count_initiatives();
$config->assigned_categories = get_all_categories();
$config->duplicates_hierarchy = array(
	"agrorede"=>25,
	"cirandas"=>30,
	"muda"=>15,
	"experiencias_ean"=>12,
	"feiras_organicas"=>10,
	"gcrs"=>27,
	"comsol"=>6
);

?>
