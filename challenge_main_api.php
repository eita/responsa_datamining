<?php

include("functions.php");
include("config.php");

$config->debug = false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}

$con = connect();

if (!isset($_GET['territoryId']) || !$_GET['territoryId'] ) {
	$ret = new StdClass();
	$ret->error = "You didn't send all obligatory fields or the correct model. Bye!";
} else {
	if (isset($_GET['lastFetchedAt']) && $_GET['lastFetchedAt']) {
		$lastFetchedAt = $_GET['lastFetchedAt'];
	}
	$sql = "SELECT MAX(relevance) AS relevance FROM Challenge";
	$res = new StdClass();
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$success = false;
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$max = $tmp->fetch_assoc();
		$max = $max['relevance'];
		if ($max==0) {
			$max = 1;
		}
	}

	$loc = get_territories();	
	$territory_id = $_GET['territoryId'];
	$territory_city_id = substr($territory_id,0,7);
	$territory_state_id = substr($territory_id,0,2);
	$territory_region_id = $loc->territories[$territory_city_id]->region_id;
	/*$sql = "
	SELECT 
		a.objectId,
		a.title, 
		a.createdAt, 
		a.description, 
		a.territoryId, 
		b.name authorName, 
		b.objectId authorId,
		a.relevance, 
		a.answerCount, 
		a.upVotes, 
		IF(a.correctAnswerId!=\"\",c.description,\"\") as correctAnswerDescription, 
		IF(a.territoryId='$territory_id',1,IF(SUBSTR(a.territoryId,1,7)='$territory_city_id',0.7,IF(a.territoryRegionId='".$territory_region_id."',0.4,IF(SUBSTR(a.territoryId,1,2)='$territory_state_id',0.2,0)))) rank_territory,
		DATEDIFF( CURDATE( ) , a.createdAt ) AS rank_days
	FROM 
		Challenge a LEFT JOIN 
			ChallengeAnswer c ON c.challengeId = a.objectId AND c.objectId = a.correctAnswerId, 
		_User b
	WHERE
		a.authorId = b.objectId AND {correctAnswer}
	ORDER BY
		((rank_territory*80) + ((a.relevance/".$max.")*100) + (70-(rank_days*3))) DESC
	LIMIT 20
	";*/
	
	$sql = "
	SELECT 
		a.objectId,
		a.title,
		IF(a.territoryId='$territory_id',1,IF(SUBSTR(a.territoryId,1,7)='$territory_city_id',0.7,IF(a.territoryRegionId='".$territory_region_id."',0.4,IF(SUBSTR(a.territoryId,1,2)='$territory_state_id',0.2,IF(a.territoryId='',1,0))))) rank_territory,
		DATEDIFF( CURDATE( ) , a.createdAt ) AS rank_days
	FROM 
		Challenge a
	WHERE a.published=1 {correctAnswer} {lastFetchedAt}
	ORDER BY
		((rank_territory*60) + ((a.relevance/".$max.")*60) + (100-(rank_days*3))) DESC
	LIMIT 20
	";
	
	if (isset($lastFetchedAt)) {
		$sql = str_replace('{lastFetchedAt}', " AND a.updatedAt>'$lastFetchedAt'",$sql);
	} else {
		$sql = str_replace('{lastFetchedAt}', "",$sql);
	}
	
	$challengeSQL = str_replace('{correctAnswer}',' AND a.correctAnswerId=""',$sql);
	//pR($challengeSQL);exit;
	
	
	$res = new StdClass();
	if (!$tmp = $con->query($challengeSQL)) {
		$error = sprintf("Error in query '$challengeSQL': %s\n", $con->error);
		$success = false;
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$res->challenges = array();
		$rank = 0;
		while ($row = $tmp->fetch_assoc()) {
			//$res->challenges[] = treat_fields_from_challenge($row);
			$t = new StdClass();
			$t->objectId = $row['objectId'];
			$t->rank = $rank;
			if (isset($_GET['show_titles'])) {
				$t->title = $row['title'];
			}
			// $res->challenges[] = $row['objectId'];
			$res->challenges[] = $t;
			$rank++;
		}
		
		$discoverySQL = str_replace('{correctAnswer}',' AND a.correctAnswerId!=""',$sql);
		
		if (!$tmp = $con->query($discoverySQL)) {
			$error = sprintf("Error in query '$discoverySQL': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$res->discoveries = array();
			while ($row = $tmp->fetch_assoc()) {
				//$res->discoveries[] = treat_fields_from_challenge($row);
				$t = new StdClass();
				$t->objectId = $row['objectId'];
				$t->rank = $rank;
				if (isset($_GET['show_titles'])) {
					$t->title = $row['title'];
				}
				//$res->discoveries[] = $row['objectId'];
				$res->discoveries[] = $t;
				$rank++;
			}
		}
	}
}

function treat_fields_from_challenge($r) {
	$t = $r;
	if (isset($t['createdAt']) && $t['createdAt']) {
		$createdAt = new \DateTime($r['createdAt']);
		$t['createdAt'] = getParseDateFormat($createdAt);
	}
	if (isset($t['updatedAt']) && $t['updatedAt']) {
		$updatedAt = new \DateTime($r['updatedAt']);
		$t['updatedAt'] = getParseDateFormat($updatedAt);
	}
	if (isset($t['localCreatedAt']) && $t['localCreatedAt']) {
		$localCreatedAt = new \DateTime($r['localCreatedAt']);
		$t['localCreatedAt'] = getParseDateFormat($localCreatedAt);
	}
	if (isset($t['correctAnswerDescription']) && !$t['correctAnswerDescription']) {
		unset($t['correctAnswerDescription']);
	}
	unset($t['rank_territory'],$t['rank_days']);
	return $t;
}

@header("Cache-Control: no-store, no-cache, must-revalidate; Content-type: application/json; charset=utf-8");
echo json_encode($res);
?>
