<?php

require("misc/parse-php-sdk/autoload.php");
require("parse_config.php");
date_default_timezone_set('UTC');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseGeoPoint;
use Parse\ParseClient;

function update_remote($item, $saved_item=false, $respect_reviewed=true) {
	global $config;
	require("parse_objects.php");
	ParseClient::initialize( $config->parse->app_id, $config->parse->rest_key, $config->parse->master_key );
	ParseClient::setServerURL($config->parse->server_url, '1');
	if (!isset($item) || (!isset($saved_item['objectId']) && (!$item->local_id || !$item->source))) {
		return false;
	}
	$willBecomeReviewed = (isset($item->reviewed) && $item->reviewed);
	$willBecomePublished = (isset($item->published) && $item->published);
	$new = true;
	if($saved_item) {
		$query = new ParseQuery("ResponsaObject");
		try {
  		$query->get($saved_item['objectId']);
  		$remote = $query->first();
  		$new = false;
  		
  		$isReviewed = ($remote->get('reviewed')==true);
  		
  		if ($respect_reviewed && $isReviewed && !$willBecomeReviewed) {
				$res = new StdClass();
				$res->success = true;
				$res->remote_id = $remote->getObjectId();
				$res->msg = sprintf(_('Didn\'t save object "%s" because it\'s reviewed in parseDB and not reviewed in source'), $res->remote_id);
				return $res;
			}
  		
		} catch (ParseException $ex) {  
			$new = true;
		}
	}
	if ($new) {
		$remote = new ParseObject("ResponsaObject");
	}
	
	// Prepare data:
	$data = new StdClass();
	foreach ($parseObj->ResponsaObject as $fieldDB) {
		$field = (isset($config->mapDB[$fieldDB]))
			? $config->mapDB[$fieldDB]
			: $fieldDB;
		switch ($field) {
			case "objectId":
			case "lng":
			case "ACL":
			case "countComments":
			case "countFavorites":
			case "countWillAttendGathering":
			case "ownerId":
			case "published":
			case "reviewed":
				// ignore these fields.
			break;
			case "source":
				if (isset($item->source) && isset($item->source[0])) {
					$data->source = strval($item->source[0]->source);
					$data->sources = $item->source;
				}
			break;
			case "created_at":
				if (isset($item->created_at)) {
					$data->sourceCreatedAt = new \DateTime($item->created_at);
				}
			break;
			case "updated_at":
				if (isset($item->updated_at)) {
					$data->sourceUpdatedAt = new \DateTime($item->updated_at);
				}
			break;
			case "geolocation":
				if (isset($item->lat) && $item->lat && isset($item->lng) && $item->lng) {
					$data->geoLocation = new ParseGeoPoint(floatval(str_replace(",",".",$item->lat)), floatval(str_replace(",",".",$item->lng)));
				}
			break;
			case "avatar":
				if (isset($item->avatar) && $item->avatar) {
					$path_to_avatar = download_avatar($item->avatar,unique_id($item->source[0]->source,$item->local_id));
					$avatar = ParseFile::createFromFile($path_to_avatar, basename($path_to_avatar));
				}
				if (isset($avatar)) {
					$data->avatarImage = $avatar;
				}
			break;
			default:
				if (isset($item->{$field}) && $item->{$field}) {
					$data->{$fieldDB} = strval($item->{$field});
				}
			break;
		}
	}
	
	$data->published = $willBecomePublished;
	$data->reviewed = $willBecomeReviewed;
	
	// Now we prepare to save remotely via Parse:
	foreach ($data as $field=>$value) {
		if (is_array($value)) {
			$remote->setArray($field, $value);
		} else {
			$remote->set($field, $value);
		}
	}
	$res = new StdClass();
	try {
		
		$remote->save();
		
		$res->success = true;
		$res->remote_id = $remote->getObjectId();
		$res->msg = ($new)
			? sprintf(_("Object with ID <i>%s</i> created!"),$res->remote_id)
			: _("Object successfully updated!");
		// When an unreviewed initiative becomes reviewed, the local data in responsa_datamining must be updated
		if (!$new && !$isReviewed && $willBecomeReviewed && $remote->get('source')=='responsa') {
			include('update_sources.php');
			do_update_sources('responsa', true);
		}
	} catch (ParseException $ex) {  
		$res->error = true;
		if (!$new) {
			$res->remote_id = $remote->getObjectId();
		}
		$res->msg = ($new)
			? sprintf(_("Failed to create new object, with error message: <i>%s</i>"),$ex->getMessage())
			: sprintf(_("Failed to update object <i>%s</i>, with error message: <i>%s</i>"),$remote->getObjectId(), $ex->getMessage());
	}
	return $res;
}

function unpublish_remote($items) {
	global $config;
	ParseClient::initialize( $config->parse->app_id, $config->parse->rest_key, $config->parse->master_key );
	ParseClient::setServerURL($config->parse->server_url, '1');
	if (!isset($items) || !is_array($items)) {
		return false;
	}
	$destroyed_items = array();
	foreach ($items as $objectId=>$item) {
		$query = new ParseQuery("ResponsaObject");
		$objectInfo = "Source=".$item->source."; Title='".$item->title."'; localId=".$item->localId;
		try {
  		$query->get($objectId);
  		$obj = $query->first();
  		$obj->set("published", false);
  		$obj->set("reviewed", true);
  		$obj->save();
  		save_log("log.csv",sprintf(_("Object %s succesfully unpublished in Parse"), $objectId).". $objectInfo");
  		$destroyed_items[] = $objectId;
		} catch (ParseException $ex) {
			$res = new StdClass();
			$res->error = 1;
			$res->msg = $ex;
			//show_alert($res);
			save_log("log.csv",sprintf(_("Failed retrieving or saving object %s."),$objectId)." $objectInfo");
		}
	}
	return $destroyed_items;
}

function update_remote_feedback($item, $objectId) {
	global $config, $parseObj;
	ParseClient::initialize( $config->parse->app_id, $config->parse->rest_key, $config->parse->master_key );
	ParseClient::setServerURL($config->parse->server_url, '1');
	if (!isset($objectId)) {
		return false;
	}
	$query = new ParseQuery("Feedback");
	try {
		$query->get($objectId);
		$remote = $query->first();
	} catch (ParseException $ex) {  
			$res = new StdClass();
			$res->error = true;
			$res->msg = sprintf(_("Failed to find feedback with id <i>%s</i>. Error message: <i>%s</i>"),$objectId, $ex->getMessage());
			return $res;
	}
	
	$item->reviewed = (isset($item->reviewed) && $item->reviewed)
		? true
		: false;
	
	// Now we prepare to save remotely via Parse:
	foreach ($item as $field=>$value) {
		if (is_array($value)) {
			$remote->setArray($field, $value);
		} else {
			$remote->set($field, $value);
		}
	}
	
	$res = new StdClass();
	try {
		$remote->save();
		$res->success = true;
		$res->msg = sprintf(_("Feedback with ID <i>%s</i> was successfully updated!"),$objectId);
	} catch (ParseException $ex) {
		$res->error = true;
		$res->msg = sprintf(_("Failed to update feedback with id <i>%s</i>. Error message: <i>%s</i>"),$objectId, $ex->getMessage());
	}
	return $res;
}


function getParseDateFormat($value="") {
	if (!$value) {
		return false;
	}
	$dateFormatString = 'Y-m-d\TH:i:s.u';
	$date = date_format($value, $dateFormatString);
	if (!$date) {
		return false;
	}
	$date = substr($date, 0, -3) . 'Z';
	return $date;
}

?>
