<?php

function review_comments() {
	global $config;
	?>
		<h1><?= _("Review comments") ?></h1>
	<?php
	if ($config->isAdmin || $config->valid_actions['review_comments']!='admin') {
		do_review_comments();
		return;
	} elseif (isset($_REQUEST['do_it'])) {
		show_failed_login_alert();
	}
	?>
		<p><?= _("Here you can see the latest comments sent by users and make an 'Official Responsa Comment' when needed") ?></p>
	<?php
	$actionUrl = '?action=review_comments&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Review comments").'</a>';
	show_login($actionUrl, $btn);
}

function do_review_comments() {
	global $config;
	$items = show_comments_per_initiative();
	if (isset($items->error)) {
		?>
		<div class="alert alert-danger">
  		Erro! <i><?= $items->error ?></i>
		</div>
		<?php
		return $items;
	}
}
?>
