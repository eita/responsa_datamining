<?php

function review_feedbacks() {
	global $config;
	?>
		<h1><?= _("Review feedbacks") ?></h1>
	<?php
	if ($config->isAdmin || $config->valid_actions['review_feedbacks']!='admin') {
		do_review_feedbacks();
		return;
	} elseif (isset($_REQUEST['do_it'])) {
		show_failed_login_alert();
	}
	?>
		<p><?= _("Here you can review the feedbacks sent by users") ?></p>
	<?php
	$actionUrl = '?action=review_feedbacks&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Review feedbacks").'</a>';
	show_login($actionUrl, $btn);
}

function do_review_feedbacks() {
	global $config;
	$items = get_all_unreviewed_feedbacks();
	if (isset($items->error)) {
		?>
		<div class="alert alert-danger">
  		Erro! <i><?= $items->error ?></i>
		</div>
		<?php
		return $items;
	}
	?>
	<?= show_table($items, "review_feedbacks") ?>
	<?php
}
?>
