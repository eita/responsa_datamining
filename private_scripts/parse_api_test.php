<?php

	include("../functions.php");
	include("../config.php");
	include("../parse_config.php");
	
	if (!isset($_GET['cuidado']) || $_GET['cuidado']!=$config->api_salt) {
		return "AI!";
	}

	$url = "http://dtygel.vps2.eita.org.br/responsa_datamining/parse_api.php";
	/* ResponsaObject:
	$params = array(
		"model" => "ResponsaObject",
		"data" => '{"address":"Praça da Rua Dr. Eurico V. de M. Carvalho, n º 221 - Paralela à ave","avatarImage":null,"category":"compre-e-troque","city":"Campinas","country":"BR","description":"Empreendimento de Economia Solidária","facebook":"","geoLocation":{"__type":"GeoPoint","latitude":-22.9099384,"longitude":-47.0626332},"localId":"46241","territoryId":"3509502","phone1":"","phone2":"","source":"cirandas","sourceCreatedAt":"2015-04-17T00:07:06.000Z","sourceUpdatedAt":"2015-07-29T04:22:23.000Z","sources":[{"local_id":46241,"source":"cirandas"},{"local_id":"1903","source":"feiras_organicas"},{"local_id":"b16","source":"gcrs"}],"state":"SP","title":"Trocas Verdes","titleSlug":"trocas-verdes","twitter":"","url":"http://cirandas.net/trocas-verdes/","website":"","zipCode":""}',
		"action" => "save",
		"object_id" => "umDe00EOet",
		"salt" => $config->api_salt,
		"debug" => 1
	);
	*/
	// COMMENT:
	//{"comment":" ffffvhgg","localCreatedAt":"2015-07-27T14:48:32.934Z","responsaObject":{"objectId":"5mw7uRLnyZ"},"user":{"objectId":"EbXyI6pHnk"},"objectId":"7TDIREKTJo","createdAt":"2015-07-27T14:48:33.286Z","updatedAt":"2015-07-27T14:48:33.286Z"}
	// RESPONSAOBJECT:
	//{"address":"Praça da Rua Dr. Eurico V. de M. Carvalho, n º 221 - Paralela à ave","avatarImage":null,"category":"compre-e-troque","city":"Campinas","country":"BR","description":"Empreendimento de Economia Solidária","facebook":"","geoLocation":{"__type":"GeoPoint","latitude":-22.9099384,"longitude":-47.0626332},"localId":"46241","territoryId":"3509502","phone1":"","phone2":"","source":"cirandas","sourceCreatedAt":"2015-04-17T00:07:06.000Z","sourceUpdatedAt":"2015-07-29T04:22:23.000Z","sources":[{"local_id":46241,"source":"cirandas"},{"local_id":"1903","source":"feiras_organicas"},{"local_id":"b16","source":"gcrs"}],"state":"SP","title":"Trocas Verdes","titleSlug":"trocas-verdes","twitter":"","url":"http://cirandas.net/trocas-verdes/","website":"","zipCode":""}
	//CHALLENGEUserJoin:
	$params = array(
		"model" => "ChallengeUserJoin",
		"data" => '{"objectId":"MRvDfcVslz","createdAt":"2015-07-29T18:06:24.962Z", "updatedAt":"2015-07-29T18:06:36.424Z","challengeId":"SIL4AMSAq6","userId":"xhBuktIQAg"}',
		"action" => "update",
		"object_id" => "MRvDfcVslz",
		"salt" => $config->api_salt,
		"debug" => 1
	);


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($params));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$dataRaw = curl_exec($ch);
	curl_close($ch);
	$data = json_decode($dataRaw);
	pR($data);
?>
