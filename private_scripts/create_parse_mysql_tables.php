<?php

include("../functions.php");
include("../parse_config.php");

if (!isset($_GET['cuidado']) || $_GET['cuidado']!=$config->api_salt) {
	return "AI!";
}

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}


$con = connect();

//////////////////////////
$sql = "
	DROP TABLE IF EXISTS `_Installation`;
	CREATE TABLE IF NOT EXISTS `_Installation` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`userId` varchar(255) NOT NULL,
		`GCMSenderId` varchar(255) NOT NULL,
		`appIdentifier` varchar(255) NOT NULL,
		`appName` varchar(30) NOT NULL,
		`appVersion` varchar(30) NOT NULL,
		`badge` varchar(10) NOT NULL,
		`channels` text NOT NULL,
		`deviceToken` varchar(255) NOT NULL,
		`deviceToken` int(11) NOT NULL,
		`deviceTokenLastModified` int(11) NOT NULL,
		`deviceType` varchar(20) NOT NULL,
		`androidVersion` int(11) NOT NULL,
		`osVersion` int(11) NOT NULL,
		`installationId` varchar(255) NOT NULL,
		`parseVersion` varchar(20) NOT NULL,
		`pushType` varchar(20) NOT NULL,
		`timeZone` varchar(255) NOT NULL,
		`user_id` text NOT NULL,
		`user_uid` text NOT NULL,
		`localeIdentifier` varchar(255) NOT NULL,
		PRIMARY KEY (`objectId`),
		KEY `userId` (`userId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////
$sql = "
	DROP TABLE IF EXISTS `Challenge`;
	CREATE TABLE IF NOT EXISTS `Challenge` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`title` varchar(255) NOT NULL,
		`description` text NOT NULL,
		`authorId` varchar(255) NOT NULL,
		`territoryId` varchar(100) NOT NULL,
		`territoryRegionId` varchar(14) NOT NULL,
		`territoryLabel` varchar(255) NOT NULL,
		`relevance` int(11) NOT NULL,
		`upvotes` int(11) NOT NULL,
		`answerCount` text NOT NULL,
		`correctAnswerId` varchar(255) NOT NULL,
		`published` tinyint(1) NOT NULL,
		PRIMARY KEY (`objectId`),
		KEY `territoryId` (`territoryId`),
		KEY `territoryRegionId` (`territoryRegionId`),
  	KEY `published` (`published`),
  	KEY `authorId` (`authorId`),
  	KEY `correctAnswerId` (`correctAnswerId`),
  	FULLTEXT KEY `search` (`title`,`description`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////
$sql = "
	DROP TABLE IF EXISTS `_User`;
	CREATE TABLE IF NOT EXISTS `_User` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`username` varchar(255) NOT NULL,
		`password` varchar(255) NOT NULL,
		`name` varchar(255) NOT NULL,
		`avatar` text NOT NULL,
		`territoryId` varchar(100) NOT NULL,
		`territoryRegionId` varchar(14) NOT NULL,
		`territoryLabel` varchar(14) NOT NULL,
		`authData` text NOT NULL,
		`emailVerified` varchar(6) NOT NULL,
		`email` varchar(255) NOT NULL,
		`phone` varchar(255) NOT NULL,
		`steps` int(11) NOT NULL,
		`loggedInOnce` tinyint(1) NOT NULL
		PRIMARY KEY (`objectId`),
		KEY `territoryId` (`territoryId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `ChallengeAnswer`;
	CREATE TABLE IF NOT EXISTS `ChallengeAnswer` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`localCreatedAt` datetime NOT NULL,
		`authorId` varchar(255) NOT NULL,
		`challengeId` varchar(255) NOT NULL,
		`description` text NOT NULL,
		`relevance` int(11) NOT NULL,
		`upvotes` int(11) NOT NULL,
		`chosenAnswer` tinyint(1) NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `authorId` (`authorId`),
  	KEY `challengeId` (`challengeId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `ChallengeUserJoin`;
	CREATE TABLE IF NOT EXISTS `ChallengeUserJoin` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`challengeId` varchar(255) NOT NULL,
		`challengeAnswerId` varchar(255) NOT NULL,
		`userId` varchar(255) NOT NULL,
		`favorite` tinyint(1) NOT NULL,
		`vote` int(11) NOT NULL,
		`cachedOrderedChallengesList` text NOT NULL,
		`favoriteChangedAt` datetime NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `challengeId` (`challengeId`),
  	KEY `challengeAnswerId` (`challengeAnswerId`),
  	KEY `userId` (`userId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `Comment`;
	CREATE TABLE IF NOT EXISTS `Comment` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`localCreatedAt` datetime NOT NULL,
		`responsaObjectId` varchar(255) NOT NULL,
		`userId` varchar(255) NOT NULL,
		`comment` text NOT NULL,
		`image` text NOT NULL,
		`thumbnail` text NOT NULL,
		`likes` int(11) NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `responsaObjectId` (`responsaObjectId`),
  	KEY `userId` (`userId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `Feedback`;
	CREATE TABLE IF NOT EXISTS `Feedback` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`cachedData` text NOT NULL,
		`lastFetchedAt` text NOT NULL,
		`userId` varchar(255) NOT NULL,
		`userText` text NOT NULL,
		`view` varchar(255) NOT NULL,
		`viewClass` varchar(255) NOT NULL,
		`viewObjectId` varchar(255) NOT NULL,
		`reviewed` tinyint(1) NOT NULL,
		`reviwerNotes` text NOT NULL,
		`reviewerInternalComments` text NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `view` (`view`),
  	KEY `userId` (`userId`),
  	KEY `viewClass` (`viewClass`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `Notification`;
	CREATE TABLE IF NOT EXISTS `Notification` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`targetUserId` varchar(255) NOT NULL,
		`channels` varchar(255) NOT NULL,
		`target` varchar(255) NOT NULL,
		`targetParams` varchar(255) NOT NULL,
		`targetView` varchar(255) NOT NULL,
		`title` varchar(255) NOT NULL,
		`shortMessage` varchar(255) NOT NULL,
		`titleChannel` varchar(255) NOT NULL,
		`shortMessageChannel` varchar(255) NOT NULL,
		`stepsChannel` int(11) NOT NULL,
		`stepsOwner` int(11) NOT NULL,
		`read` tinyint(1) NOT NULL,
		`channelIntent` varchar(255) NOT NULL,
		`channelIntentParams` text NOT NULL,
		`actionTrigger` varchar(255) NOT NULL,
		`userId` varchar(255) NOT NULL,
		`silent` tinyint(1) NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `userId` (`userId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `ResponsaObject`;
	CREATE TABLE IF NOT EXISTS `ResponsaObject` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`type` varchar(50) NOT NULL,
		`title` varchar(255) NOT NULL,
		`titleSlug` varchar(255) NOT NULL,
		`description` text NOT NULL,
		`category` varchar(60) NOT NULL,
		`avatarImage` text NOT NULL,
		`territoryId` varchar(20) NOT NULL,
		`territoryRegionId` varchar(14) NOT NULL,
		`territoryLabel` varchar(255) NOT NULL,
		`address` text NOT NULL,
		`zipCode` varchar(255) NOT NULL,
		`geoLocation` varchar(255) NOT NULL,
		`dateTimeStart` datetime NOT NULL,
		`dateTimeFinish` datetime NOT NULL,
		`placeInitiativeId` varchar(255) NOT NULL,
		`localId` varchar(255) NOT NULL,
		`source` varchar(100) NOT NULL,
		`sourceCreatedAt` datetime NOT NULL,
		`sourceUpdatedAt` datetime NOT NULL,
		`sources` text NOT NULL,
		`url` varchar(255) NOT NULL,
		`phone1` varchar(255) NOT NULL,
		`phone2` varchar(255) NOT NULL,
		`email` varchar(255) NOT NULL,
		`website` varchar(255) NOT NULL,
		`twitter` varchar(255) NOT NULL,
		`facebook` varchar(255) NOT NULL,
		`countComments` int(11) NOT NULL,
		`countFavorites` inf(11) NOT NULL,
		`countWillAttendGathering` int(11) NOT NULL,
		`ownerId` varchar(100) NOT NULL,
		`published` tinyint(1) NOT NULL,
		`reviewed` tinyint(1) NOT NULL,
		`reviwerNotes` text NOT NULL,
		`reviewerInternalComments` text NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `category` (`category`),
  	KEY `territoryId` (`territoryId`),
  	KEY `ownerId` (`ownerId`),
  	KEY `published` (`published`),
  	KEY `reviewed` (`reviewed`),
  	KEY `placeInitiativeId` (`placeInitiativeId`),
  	KEY `placeInitiativeId` (`localId`),
  	KEY `placeInitiativeId` (`source`),
  	FULLTEXT KEY `search` (`title`,`description`,`territoryLabel`),
  	FULLTEXT KEY `search_title` (`title`),
  	FULLTEXT KEY `search_description` (`description`),
  	FULLTEXT KEY `search_territory` (`territoryLabel`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
//////////////////////////////
$sql = "
	DROP TABLE IF EXISTS `UserResponsaObject`;
	CREATE TABLE IF NOT EXISTS `UserResponsaObject` (
		`objectId` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`responsaObjectId` varchar(255) NOT NULL,
		`userId` varchar(255) NOT NULL,
		`favoritou` tinyint(1) NOT NULL,
		`favoritouChangedAt` datetime NOT NULL,
		`favoritou` datetime NOT NULL,
		`marcouPresencaChangedAt` tinyint(1) NOT NULL,
		`lastAlertChegada` datetime NOT NULL,
		PRIMARY KEY (`objectId`),
  	KEY `responsaObjectId` (`responsaObjectId`),
  	KEY `userId` (`userId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}

//////////////////////////
$sql = "
	DROP TABLE IF EXISTS `_Role`;
	CREATE TABLE IF NOT EXISTS `_Role` (
		`objectId` varchar(255) NOT NULL,
		`name` varchar(255) NOT NULL,
		`roles` varchar(255) NOT NULL,
		`users` varchar(255) NOT NULL,
		`createdAt` datetime NOT NULL,
		`updatedAt` datetime NOT NULL,
		`ACL` varchar(255) NOT NULL,
		PRIMARY KEY (`objectId`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
";
$sqlAr = explode(';',$sql);
foreach ($sqlAr as $sql) {
	if ($con->query($sql)) {
		echo "<hr>done";
	}
}
?>
