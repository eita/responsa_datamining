<?php

putenv('LC_ALL=pt_BR');
setlocale(LC_ALL, 'pt_BR');
bindtextdomain("responsa", "locale");
textdomain("responsa");

include("../functions.php");
include("../config.php");
include("../parse_config.php");
include("../parse_functions.php");

$config->debug = true;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}

$con = connect();
$h = fopen('dummy_phrases_for_translation_parse_key_labels.php','w');
fwrite($h,"<?php\n");

$sql = "
	SELECT * 
	FROM
		ResponsaObject a
	LIMIT 1
";
$tmp = $con->query($sql);
$row = $tmp->fetch_assoc();

foreach ($row as $k=>$v) {
	fwrite($h,"_('$k');\n");
}

$sql = "
	SELECT * 
	FROM
		_User a
	LIMIT 1
";
$tmp = $con->query($sql);
$row = $tmp->fetch_assoc();

foreach ($row as $k=>$v) {
	fwrite($h,"_('$k');\n");
}

fwrite("?>");
fclose($h);

?>
