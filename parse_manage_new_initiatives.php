<?php

putenv('LC_ALL=pt_BR');
setlocale(LC_ALL, 'pt_BR');
bindtextdomain("responsa", "locale");
textdomain("responsa");

include("functions.php");
include("config.php");
include("parse_config.php");
include("parse_functions.php");

/*$config->debug = true;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}*/

require_once "Mail.php";
require_once "Mail/mime.php";
include("config_mail.php");



// If an unpublished and unreviewed initiative is there for 7 days, it will be published automagically
$timeToValidate = 3600 * 24 * 7;

$con = connect();


// Set all unreviewed initiatives older than 7 days to publish=TRUE!
$sql = "
	UPDATE 
		ResponsaObject a,
		__internal_MailsSent b
	SET
		a.published = 1
	WHERE
		a.objectId = b.ResponsaObjectId AND
		b.dateSent < NOW() - INTERVAL '".$timeToValidate."' SECOND
";
if (!$tmp = $con->query($sql)) {
	$error = sprintf("Error in query '$sql': %s\n", $con->error);
	$success = false;
	$res = new StdClass();
	$res->error = 1;
	$res->msg = (isset($error)) ? $error : 42;
	show_alert($res);
}


// Clean __internal_MailsSent from initiatives that had messages sent more than 7 days ago:
$sql = "DELETE FROM __internal_MailsSent WHERE dateSent < NOW() - INTERVAL '".$timeToValidate."' SECOND";
if (!$tmp = $con->query($sql)) {
	$error = sprintf("Error in query '$sql': %s\n", $con->error);
	$success = false;
	$res = new StdClass();
	$res->error = 1;
	$res->msg = (isset($error)) ? $error : 42;
	show_alert($res);
}


// Are there new unreviewed initiatives suggested by users?
$sql = "
	SELECT * 
	FROM
		ResponsaObject a
	WHERE 
		a.objectId NOT IN (SELECT ResponsaObjectId FROM __internal_MailsSent) AND
		a.published = 0 AND
		a.reviewed = 0 AND
		a.type = 'initiative'
";
$tmp = $con->query($sql);
$initiativeDataList = array();
$userDataList = array();
while ($row = $tmp->fetch_assoc()) {
	$initiativeDataList[$row['objectId']] = $row;
	$sql = "SELECT * FROM _User WHERE objectId='".$row['ownerId']."'";
	$tmp2 = $con->query($sql);
	$userDataList[$row['objectId']] = $tmp2->fetch_assoc();
}
if (count($initiativeDataList)>0) {
	foreach ($initiativeDataList as $objectId=>$initiativeData) {
		$userData = $userDataList[$objectId];
		// Send message:
		send_message_to_admins($initiativeData, $userData);
		exit;
		// Add log to __internal_MailsSent:
		//$sql = "INSERT INTO __internal_MailsSent (ResponsaObjectId) VALUES ('$objectId')";
		//$tmp = $con->query($sql);
	}
}
exit;


function send_message_to_admins($initiativeDataRaw, $userDataRaw) {
	global $config, $con;
	
	// Get the administrators
	$sql = "SELECT users FROM _Role WHERE name='Administrator'";
	$tmp = $con->query($sql);
	$row = $tmp->fetch_assoc();
	$adminObjectIds = json_decode($row['users']);
	if (count($adminObjectIds)>0) {
		$sql = "SELECT * FROM _USER WHERE objectId IN ('".implode($adminObjectIds, "','")."')";
		$tmp = $con->query($sql);
		$admins = array();
		while ($row = $tmp->fetch_assoc()) {
			$admins[$row['objectId']] = $row;
		}
	}
	
	// For debug purposes only:
	$admins = array('1'=>array('objectId'=>'1', 'email'=>'dtygel@eita.org.br', 'name'=>'Daniel Tygel'));
	
	// Prepare initiative data:
	$initiativeData = "<table border='0'>\n";
	foreach ($initiativeDataRaw as $key=>$value) {
		if ($value && $value != '0000-00-00 00:00:00') {
			$initiativeData .= "<tr>\n";
			$initiativeData .= "<td>"._($key).":</td>";
			$initiativeData .= "<td>&nbsp;</td>";
			$initiativeData .= "<td>".$value."</td>";
			$initiativeData .= "</tr>\n";
		}
	}
	$initiativeData .= "</table>";
	
	// Prepare user data:
	$userData = "<table border='0'>\n";
	foreach ($userDataRaw as $key=>$value) {
		if ($value && $value != '0000-00-00 00:00:00') {
			$userData .= "<tr>\n";
			$userData .= "<td>"._($key).":</td>";
			$userData .= "<td>&nbsp;</td>";
			$userData .= "<td>".$value."</td>";
			$userData .= "</tr>\n";
		}
	}
	$userData .= "</table>";
	
	// Prepare basic message params:
	$from = "dtygel@eita.org.br";
	$subject = sprintf("[RESPONSA] Sugerida nova iniciativa '%s'", $initiativeDataRaw['title']);
	$text = sprintf("
		<p>Olá, <b>%s</b>,</p>
		<p>Uma nova iniciativa foi sugerida para compor a base de dados do Responsa. Confira os dados abaixo:</p>
		<h3>Dados da iniciativa</h3>
		%s
		<br />
		<h3>Dados da/do usuária/o que sugeriu esta iniciativa</h3>
		%s
		<br />
		<p>Se nenhum/a moderador/a do GT do Responsa reagir em uma semana, a iniciativa estará aprovada automaticamente mesmo sem revisão detalhada.</p>
		<p>Um abraço, </p>
		<p><i>equipes da EITA e do Instituto Kairós</i></p>
	", $userDataRaw['name'], $initiativeData, $userData);
	$crlf = "\n";
	
	$mime = new Mail_mime($crlf);
	$mimeparams=array();
	$mimeparams['text_encoding']="7bit";
	$mimeparams['head_charset']="UTF-8";
	$mimeparams['text_charset']="UTF-8";
	$mimeparams['html_charset']="UTF-8";
	$mime->setHTMLBody($text);
	
	/*
	$file = "a3.jpg";
	$mimetype = "image/jpeg";
	$mime->addAttachment($file, $mimetype); 
	*/

	
	// Send messages:	
	foreach ($admins as $adminObjectId=>$admin) {
		$to = $admin['email'];
		$headers = array (
			'From' => $from,
			'To' => $to,
			'Subject' => $subject
		);
		$smtp = Mail::factory(
			'smtp',
			array (
				'host' => $config->smtp_host,
				'auth' => true,
				'username' => $config->smtp_username,
				'password' => $config->smtp_password
			)
		);
		
		$body = $mime->get($mimeparams);
		$headers = $mime->headers($headers); 

		// Action!
		$mail = $smtp->send($to, $headers, $body);
		
		if (PEAR::isError($mail)) {
		 echo("" . $mail->getMessage() . "");
		} else {
			echo("Message successfully sent!");
		}

	}
	 
	
}

exit;

@header("Content-type: application/json; charset=utf-8");

?>
