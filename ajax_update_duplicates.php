<?php


if (!isset($_GET['ref_source']) || !isset($_GET['ref_local_id']) || !isset($_GET['dup_source']) || !isset($_GET['dup_local_id'])) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("There was nothing to update!");
} else {

	include("functions.php");
	include("config.php");
	include("init_external_data.php");

	$refs = $config->data_sources;
	foreach ($refs as $f=>$r) {
		$refs[$f] = array();
	}
	$hidden = $refs;

	$ref_source = $_GET['ref_source'];
	$ref_local_id = $_GET['ref_local_id'];

	$dup_source = $_GET['dup_source'];
	$dup_local_id = $_GET['dup_local_id'];

	foreach ($config->data[$dup_source]->data as $item) {
		if ($item->local_id == $dup_local_id) {
			$dup_source = $item->source;
			break;
		}
	}
	if (!is_array($dup_source)) {
		$t = new StdClass();
		$t->local_id = $dup_local_id;
		$t->source = $dup_source;
		$dup_source = array($t);
	}

	/*
	Before, I only assigned the source for different sources. Now I think even duplicates in same source must be in the 'sources' array...
	if ($ref_source != $dup_source) {
		$refs[$ref_source][$ref_local_id] = array($t);
	}*/
	$refs[$ref_source][$ref_local_id] = $dup_source;
	$hidden[$dup_source[0]->source][$dup_local_id] = true;
	$ret = new StdClass();
	$updated = update_data($refs, $hidden, null, false);
	if ($updated) {
		$ret->success = true;
		$ret->msg = _("Updates saved!");
	} else {
		$ret->error = true;
		$ret->msg = _("There was an error doing the update!");
	}
}

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
