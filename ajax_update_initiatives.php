<?php

	/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);*/


require("functions.php");
require("config.php");
include("parse_config.php");
include("parse_functions.php");

$valid_actions = array('reject', 'approve', 'save');
$valid_fields = $parseObj->ResponsaObject;

if (!isset($_POST['objectId']) || !isset($_POST['action']) || !in_array($_POST['action'], $valid_actions)) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("Not enough parameters to procede");
} else if (!check_login()) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("Login failed");
}

if (!isset($ret)) {
	$objectId = $_POST['objectId'];
	$action = $_POST['action'];
	$data = $_POST['data'];
	$item = new StdClass();
	foreach ($data as $field=>$value) {
		if (isset($config->mapDB[$field])) {
			$item->{$config->mapDB[$field]} = $value;
		} else {
			$item->{$field} = $value;
		}
	}
	$updated = false;
	switch($action) {
		case 'approve':
			$item->published = true;
			$item->reviewed = true;
		break;
		case 'reject':
			$item->published = false;
			$item->reviewed = true;
		break;
	}
	$ret = update_remote($item, array('objectId'=>$objectId));
}

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
