<?php

include("functions.php");
include("config.php");
$res = get_all_initiatives("geoJSON");
if (isset($_GET['callback']) && $_GET['callback']) {
	echo $_GET['callback']."(".json_encode($res).");";
} else if (isset($_GET['download']) && $_GET['download']) {
	$date = new DateTime();
	$fDate = $date->format('Y-m-d');
	header("Content-type: application/text; charset=utf-8");
  header("Content-Disposition: attachment; filename=\"dados_responsa_$fDate.js\"");
  echo json_encode($res);
} else {
	@header("Content-type: application/json; charset=utf-8");
	echo json_encode($res);
}
?>
