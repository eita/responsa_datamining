<?php

function create_encontre_import_file() {
	global $config;
	
//download_avatar("",unique_id("cirandas","20986"));

	$config->encontre_import_fields = array(
		"post_name",
		"post_title",
		"post_status",
		"post_content",
		"post_excerpt",
		"post_author",
		"post_parent",
		"post_date",
		"post_image",
		"comment_status",
		"ping_status",
		"featured",
		"address",
		"gpsLatitude",
		"gpsLongitude",
		"telephone",
		"email",
		"emailContactOwner",
		"web",
		"hoursMonday",
		"hoursTuesday",
		"hoursWednesday",
		"hoursThursday",
		"hoursFriday",
		"hoursSaturday",
		"hoursSunday",
		"alternativeContent",
		"tax-ait-dir-item-category",
		"tax-ait-dir-item-territory"
	);
	if ($arq = fopen("data/encontre_import.csv","w")) {
		fputcsv($arq,$config->encontre_import_fields);
		$qtde = 0;
		foreach ($config->data as $f=>$data) {
			foreach ($data->data as $item) {
				$t = array();
				$image = (isset($item->avatar) && $item->avatar)
					? unique_id($f,$item->local_id)
					: "";
				$description = get_labeled_sources($item->source)."<br />".str_replace(array("\n","\t"),"<br />",$item->description);
				$t["post_name"] = $item->title_slug;
				$t["post_title"] = $item->title;
				$t["post_status"] = "publish";
				$t["post_content"] = $description;
				$t["post_excerpt"] = "";
				$t["post_author"] = "";
				$t["post_parent"] = "";
				$t["post_date"] = $item->created_at;
				$t["post_image"] = $image;
				$t["comment_status"] = "open";
				$t["ping_status"] = "open";
				$t["featured"] = "";
				$t["address"] = $item->address;
				$t["gpsLatitude"] = str_replace(",",".",$item->lat);
				$t["gpsLongitude"] = str_replace(",",".",$item->lng);
				$t["telephone"] = (isset($item->phone1) && $item->phone1) ? implode(" / ", array($item->phone1,$item->phone2)) : "";
				$t["email"] = $item->email;
				$t["emailContactOwner"] = "";
				$t["web"] = $item->website;
				$t["hoursMonday"] = "";
				$t["hoursTuesday"] = "";
				$t["hoursWednesday"] = "";
				$t["hoursThursday"] = "";
				$t["hoursFriday"] = "";
				$t["hoursSaturday"] = "";
				$t["hoursSunday"] = "";
				$t["alternativeContent"] = "";
				$t["tax-ait-dir-item-category"] = $item->category;
				$t["tax-ait-dir-item-territory"] = $item->territory_id;
				fputcsv($arq,$t);
				$qtde++;
				?>
				<p><?= $qtde ?> - <?= $item->title ?> (<?= get_labeled_sources($item->source) ?>)</p>
				<?php
			}
		}
		fclose($arq);
		$r = new StdClass();
		$r->success = 1;
		$r->msg = sprintf(_("File <b>%s</b> successfully saved!"),"<a href='data/encontre_import.csv'>encontre_import.csv</a>");
		show_alert($r);
	} else {
		$r = new StdClass();
		$r->error = 1;
		$r->msg = _("I've failed to save the file! Aborted.");
		show_alert($r);
	}
}

?>
