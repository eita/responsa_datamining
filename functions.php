<?php

error_reporting(E_ERROR | E_PARSE);

//////////// GENERAL FUNCTIONS //////////////////

function save_log($file_name,$data) {
	if (!$h = fopen("data/$file_name",'a')) {
		pR('It was not possible to save the file log.csv');
		return false;
	} else {
		$ret = true;
		if (!fwrite($h,$data."\n")) {
			fwrite($h,"I was not able to save a line of file\n");
			$ret = false;
		}
		fclose($h);
		return $ret;
	}
}
function save_data($file_name,$data,$show_message=true) {
	global $config;
	if ($file_name == 'data.json' && (!isset($data) || !$data || !isset($data['cirandas']) || !$data['cirandas']->data)) {
		if (!$h = fopen('data/log.csv','a')) {
			pR('It was not possible to save the file log.csv');
		} else {
			fwrite($h,json_encode(getallheaders())."\n");
			fwrite($h,json_encode($_REQUEST)."\n");
			fwrite($h,"-----------------\n");
			fclose($h);
		}
	}
	$dataJSON = json_encode($data);
	$success = file_put_contents("data/$file_name",$dataJSON);
	if ($config->debug && (!$success || !$dataJSON)) {
		echo _("Trying to save the file")." data/$file_name";
		pR($success===false);
		pR(json_last_error_msg());
		pR($dataJSON);
		pR($data);
	}
	if ($show_message) {
		$r = new StdClass();
		if ($success) {
			$r->success = 1;
			$r->msg = _("Done! I've successfully saved the file")." <a href='data/$file_name'><i>$file_name</i></a>.";
		} else {
			$r->error = 1;
			$r->msg = _("There was an error that prevented me to save the file")." :-(";
		}
		show_alert($r);
		return false;
	} else {
		return $success;
	}
}

function save_data_line($file_name,$dir="data/", $object_id, $data) {
	global $config;
	$arq = $dir.$file_name;
	$h = fopen($arq,"a");
	if ($h!==false) {
		fwrite($h,$object_id."|,|".$data."\n");
		return true;
	} else {
		return false;
	}
}

function busca_recursiva_array($ar, $s, $perfect_match=true) {
	foreach ($ar as $a) {
		if (is_array($a)) {
			if ($ret = busca_recursiva_array($a, $s, $perfect_match))
				return $ret;
		} else if (sanitize($a)==$s) {
			return $ar;
		}
	}
	return false;
}


function pR($t) {
	echo "<pre>";
	print_r($t);
	echo "</pre>";
}

function get_country_iso3166_2($country) {
	$paises = array(
		"Afeganistão"=>"AF",
		"Ilhas Aland"=>"AX",
		"Albânia"=>"AL",
		"Alderney"=>"",
		"Argélia"=>"DZ",
		"Samoa Americana"=>"AS",
		"Andorra"=>"AD",
		"Angola"=>"AO",
		"Anguilla"=>"AI",
		"Antártida"=>"AQ",
		"Antígua e Barbuda"=>"AG",
		"Argentina"=>"AR",
		"Armênia"=>"AM",
		"Aruba"=>"AW",
		"Ilha de Ascensão"=>"",
		"Austrália"=>"AU",
		"Áustria"=>"AT",
		"Azerbaijão"=>"AZ",
		"Bahamas"=>"BS",
		"Bahrein"=>"BH",
		"Bangladesh"=>"BD",
		"Barbados"=>"BB",
		"Belarus"=>"BY",
		"Bélgica"=>"BE",
		"Belize"=>"BZ",
		"Benin"=>"BJ",
		"Bermudas"=>"BM",
		"Butão"=>"BT",
		"Bolívia"=>"BO",
		"Bósnia-Herzegóvina"=>"BA",
		"Botsuana"=>"BW",
		"Ilha Bouvet (Território da Noruega)"=>"BV",
		"Brasil"=>"BR",
		"Território Britânico do Oceano índico"=>"IO",
		"Brunei"=>"BN",
		"Bulgária"=>"BG",
		"Burkina Fasso"=>"BF",
		"Burundi"=>"BI",
		"Camboja"=>"KH",
		"Camarões"=>"CM",
		"Canadá"=>"CA",
		"Cabo Verde"=>"CV",
		"Ilhas Cayman"=>"KY",
		"República Centro-Africana"=>"CF",
		"Chade"=>"TD",
		"Ilhas do Canal"=>"",
		"Chile"=>"CL",
		"China"=>"CN",
		"Ilha Natal"=>"CX",
		"Ilhas Cocos"=>"CC",
		"Colômbia"=>"CO",
		"Ilhas Comores"=>"KM",
		"Congo"=>"CG",
		"República Democrática do Congo (ex-Zaire)"=>"CD",
		"Ilhas Cook"=>"CK",
		"Costa Rica"=>"CR",
		"Costa do Marfim"=>"CI",
		"Croácia (Hrvatska)"=>"HR",
		"Cuba"=>"CU",
		"Chipre"=>"CY",
		"República Tcheca"=>"CZ",
		"Dinamarca"=>"DK",
		"Djibuti"=>"DJ",
		"Dominica"=>"DM",
		"República Dominicana"=>"DO",
		"Equador"=>"EC",
		"Egito"=>"EG",
		"El Salvador"=>"SV",
		"Guiné Equatorial"=>"GQ",
		"Eritréia"=>"ER",
		"Estônia"=>"EE",
		"Etiópia"=>"ET",
		"União Europeia"=>"",
		"Ilhas Faroes"=>"FO",
		"Ilhas Falkland (Malvinas)"=>"FK",
		"Fiji"=>"FJ",
		"Finlândia"=>"FI",
		"França"=>"FR",
		"Guiana Francesa"=>"GF",
		"Polinésia Francesa"=>"PF",
		"Territórios do Sul da França"=>"TF",
		"Gabão"=>"GA",
		"Gâmbia"=>"GM",
		"Geórgia"=>"GE",
		"Alemanha"=>"DE",
		"Gana"=>"GH",
		"Gibraltar"=>"GI",
		"Grécia"=>"GR",
		"Groelândia"=>"GL",
		"Granada"=>"GD",
		"Guadalupe"=>"GP",
		"Guam (Território dos Estados Unidos)"=>"GU",
		"Guatemala"=>"GT",
		"Guernsey"=>"GG",
		"Guiné"=>"GN",
		"Guiné-Bissau"=>"GW",
		"Guiana"=>"GY",
		"Haiti"=>"HT",
		"Ilhas Heard e McDonald (Território da Austrália)"=>"HM",
		"Honduras"=>"HN",
		"Hong Kong"=>"HK",
		"Hungria"=>"HU",
		"Islândia"=>"IS",
		"Índia"=>"IN",
		"Indonésia"=>"ID",
		"ORGANIZAÇÕES INTERNACIONAIS"=>"",
		"Irã"=>"IR",
		"Iraque"=>"IQ",
		"Irlanda"=>"IE",
		"Ilha do Homem"=>"IM",
		"Israel"=>"IL",
		"Itália"=>"IT",
		"Jamaica"=>"JM",
		"Japão"=>"JP",
		"Jersey"=>"JE",
		"Jordânia"=>"JO",
		"Cazaquistão"=>"KZ",
		"Kênia"=>"KE",
		"Kiribati"=>"KI",
		"Coréia do Norte"=>"KP",
		"Coréia do Sul"=>"KR",
		"Kuait"=>"KW",
		"Kyrgyzstan"=>"KG",
		"Laos"=>"LA",
		"Látvia"=>"LV",
		"Líbano"=>"LB",
		"Lesoto"=>"LS",
		"Libéria"=>"LR",
		"Líbia"=>"LY",
		"Liechtenstein"=>"LI",
		"Lituânia"=>"LT",
		"Luxemburgo"=>"LU",
		"Macau"=>"MO",
		"Macedônia (República Yugoslava)"=>"MK",
		"Madagascar"=>"MG",
		"Malaui"=>"MW",
		"Malásia"=>"MY",
		"Maldivas"=>"MV",
		"Mali"=>"ML",
		"Malta"=>"MT",
		"Ilhas Marshall"=>"MH",
		"Martinica"=>"MQ",
		"Mauritânia"=>"MR",
		"Maurício"=>"MU",
		"Mayotte"=>"YT",
		"Mexico"=>"MX",
		"Micronésia"=>"FM",
		"Moldova"=>"MD",
		"Mônaco"=>"MC",
		"Mongólia"=>"MN",
		"Montenegro"=>"ME",
		"Montserrat"=>"MS",
		"Marrocos"=>"MA",
		"Moçambique"=>"MZ",
		"Myanma (Ex-Burma)"=>"MM",
		"Namíbia"=>"NA",
		"Nauru"=>"NR",
		"Nepal"=>"NP",
		"Holanda"=>"NL",
		"Antilhas Holandesas"=>"AN",
		"Nova Caledônia"=>"NC",
		"Nova Zelândia"=>"NZ",
		"Nicarágua"=>"NI",
		"Níger"=>"NE",
		"Nigéria"=>"NG",
		"Niue"=>"NU",
		"Ilhas Norfolk"=>"NF",
		"Ilhas Marianas do Norte"=>"MP",
		"Noruega"=>"NO",
		"Omã"=>"OM",
		"Paquistão"=>"PK",
		"Palau"=>"PW",
		"Territórios Palestinos Ocupados"=>"PS",
		"Panamá"=>"PA",
		"Papua-Nova Guiné"=>"PG",
		"Paraguai"=>"PY",
		"Peru"=>"PE",
		"Filipinas"=>"PH",
		"Ilha Pitcairn"=>"PN",
		"Polônia"=>"PL",
		"Portugal"=>"PT",
		"Porto Rico"=>"PR",
		"Qatar"=>"QA",
		"Ilha Reunião"=>"RE",
		"Romênia"=>"RO",
		"Federação Russa"=>"RU",
		"Ruanda"=>"RW",
		"São Bartolomeu"=>"BL",
		"Santa Helena"=>"SH",
		"São Cristóvão e Névis"=>"KN",
		"Santa Lúcia"=>"LC",
		"São Martim"=>"MF",
		"St. Pierre and Miquelon"=>"PM",
		"Saint Vincente e Granadinas"=>"VC",
		"Samoa Ocidental"=>"WS",
		"San Marino"=>"SM",
		"São Tomé e Príncipe"=>"ST",
		"Arábia Saudita"=>"SA",
		"Senegal"=>"SN",
		"Sérvia"=>"RS",
		"Ilhas Seychelles"=>"SC",
		"Serra Leoa"=>"SL",
		"Cingapura"=>"SG",
		"Eslováquia"=>"SK",
		"Eslovênia"=>"SI",
		"Ilhas Solomão"=>"SB",
		"Somália"=>"SO",
		"África do Sul"=>"ZA",
		"Ilhas Geórgia do Sul e Sandwich do Sul"=>"GS",
		"União Soviética"=>"",
		"Espanha"=>"ES",
		"Sri Lanka"=>"LK",
		"Sudão"=>"SD",
		"Suriname"=>"SR",
		"Ilhas Svalbard e Jan Mayen"=>"SJ",
		"Suazilândia"=>"SZ",
		"Suécia"=>"SE",
		"Suíça"=>"CH",
		"Síria"=>"SY",
		"Taiwan"=>"TW",
		"Tadjiquistão"=>"TJ",
		"Tanganica"=>"",
		"Tanzânia"=>"TZ",
		"Tailândia"=>"TH",
		"Timor Leste (Ex-East Timor)"=>"TL",
		"Togo"=>"TG",
		"Ilhas Tokelau"=>"TK",
		"Tonga"=>"TO",
		"Trinidad and Tobago"=>"TT",
		"Tunísia"=>"TN",
		"Turquia"=>"TR",
		"Turcomenistão"=>"TM",
		"Ilhas Turks e Caicos"=>"TC",
		"Tuvalu"=>"TV",
		"Uganda"=>"UG",
		"Ucrânia"=>"UA",
		"Emirados Árabes Unidos"=>"AE",
		"Grã-Bretanha (Reino Unido, UK)"=>"GB",
		"Estados Unidos"=>"US",
		"Ilhas Menores dos Estados Unidos"=>"UM",
		"Uruguai"=>"UY",
		"Uzbequistão"=>"UZ",
		"Vanuatu"=>"VU",
		"Vaticano"=>"VA",
		"Venezuela"=>"VE",
		"Vietnam"=>"VN",
		"Ilhas Virgens (Inglaterra)"=>"VG",
		"Ilhas Virgens (Estados Unidos)"=>"VI",
		"Reino da Jugoslávia"=>"",
		"Ilhas Wallis e Futuna"=>"WF",
		"Saara Ocidental (Ex-Spanish Sahara)"=>"EH",
		"Iêmen"=>"YE",
		"Zâmbia"=>"ZM",
		"Zanzibar"=>"",
		"Zimbábue"=>"ZW"
	);
	return $paises[$country];
}

function sanitize($t) {
        $t = strip_tags(remove_accents($t));

        // Preserve escaped octets.
        $t = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $t);
        // Remove percent signs that are not part of an octet.
        $t = str_replace('%', '', $t);
        // Restore octets.
        $t = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $t);

        $t = strtolower($t);
        $t = preg_replace('/&.+?;/', '', $t); // kill entities
        $t = str_replace('.', '-', $t);

        // Convert nbsp, ndash and mdash to hyphens
        $t = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $t );

        // Strip these characters entirely
        $t = str_replace( array(
                // iexcl and iquest
                '%c2%a1', '%c2%bf',
                // angle quotes
                '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
                // curly quotes
                '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
                '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
                // copy, reg, deg, hellip and trade
                '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
                // acute accents
                '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
                // grave accent, macron, caron
                '%cc%80', '%cc%84', '%cc%8c',
        ), '', $t );

        // Convert times to x
        $t = str_replace( '%c3%97', 'x', $t );

        $t = preg_replace('/[^%a-z0-9 _-]/', '', $t);
        $t = preg_replace('/\s+/', '-', $t);
        $t = preg_replace('|-+|', '-', $t);
        $t = trim($t, '-');

        return $t;
	}

   /**
	 * Converts all accent characters to ASCII characters.
	 *
	 * If there are no accent characters, then the string given is just returned.
	 *
	 * @since 1.2.1
	 *
	 * @param string $string Text that might have accent characters
	 * @return string Filtered string with replaced "nice" characters.
	 */
	function remove_accents($string) {
		if ( !preg_match('/[\x80-\xff]/', $string) )
			return $string;

			$chars = array(
			// Decompositions for Latin-1 Supplement
			chr(194).chr(170) => 'a', chr(194).chr(186) => 'o',
			chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
			chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
			chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
			chr(195).chr(134) => 'AE',chr(195).chr(135) => 'C',
			chr(195).chr(136) => 'E', chr(195).chr(137) => 'E',
			chr(195).chr(138) => 'E', chr(195).chr(139) => 'E',
			chr(195).chr(140) => 'I', chr(195).chr(141) => 'I',
			chr(195).chr(142) => 'I', chr(195).chr(143) => 'I',
			chr(195).chr(144) => 'D', chr(195).chr(145) => 'N',
			chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
			chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
			chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
			chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
			chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
			chr(195).chr(158) => 'TH',chr(195).chr(159) => 's',
			chr(195).chr(160) => 'a', chr(195).chr(161) => 'a',
			chr(195).chr(162) => 'a', chr(195).chr(163) => 'a',
			chr(195).chr(164) => 'a', chr(195).chr(165) => 'a',
			chr(195).chr(166) => 'ae',chr(195).chr(167) => 'c',
			chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
			chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
			chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
			chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
			chr(195).chr(176) => 'd', chr(195).chr(177) => 'n',
			chr(195).chr(178) => 'o', chr(195).chr(179) => 'o',
			chr(195).chr(180) => 'o', chr(195).chr(181) => 'o',
			chr(195).chr(182) => 'o', chr(195).chr(184) => 'o',
			chr(195).chr(185) => 'u', chr(195).chr(186) => 'u',
			chr(195).chr(187) => 'u', chr(195).chr(188) => 'u',
			chr(195).chr(189) => 'y', chr(195).chr(190) => 'th',
			chr(195).chr(191) => 'y', chr(195).chr(152) => 'O',
			// Decompositions for Latin Extended-A
			chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
			chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
			chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
			chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
			chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
			chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
			chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
			chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
			chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
			chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
			chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
			chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
			chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
			chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
			chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
			chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
			chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
			chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
			chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
			chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
			chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
			chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
			chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
			chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
			chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
			chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
			chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
			chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
			chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
			chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
			chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
			chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
			chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
			chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
			chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
			chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
			chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
			chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
			chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
			chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
			chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
			chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
			chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
			chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
			chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
			chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
			chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
			chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
			chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
			chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
			chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
			chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
			chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
			chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
			chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
			chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
			chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
			chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
			chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
			chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
			chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
			chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
			chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
			chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
			// Decompositions for Latin Extended-B
			chr(200).chr(152) => 'S', chr(200).chr(153) => 's',
			chr(200).chr(154) => 'T', chr(200).chr(155) => 't',
			// Euro Sign
			chr(226).chr(130).chr(172) => 'E',
			// GBP (Pound) Sign
			chr(194).chr(163) => '',
			// Vowels with diacritic (Vietnamese)
			// unmarked
			chr(198).chr(160) => 'O', chr(198).chr(161) => 'o',
			chr(198).chr(175) => 'U', chr(198).chr(176) => 'u',
			// grave accent
			chr(225).chr(186).chr(166) => 'A', chr(225).chr(186).chr(167) => 'a',
			chr(225).chr(186).chr(176) => 'A', chr(225).chr(186).chr(177) => 'a',
			chr(225).chr(187).chr(128) => 'E', chr(225).chr(187).chr(129) => 'e',
			chr(225).chr(187).chr(146) => 'O', chr(225).chr(187).chr(147) => 'o',
			chr(225).chr(187).chr(156) => 'O', chr(225).chr(187).chr(157) => 'o',
			chr(225).chr(187).chr(170) => 'U', chr(225).chr(187).chr(171) => 'u',
			chr(225).chr(187).chr(178) => 'Y', chr(225).chr(187).chr(179) => 'y',
			// hook
			chr(225).chr(186).chr(162) => 'A', chr(225).chr(186).chr(163) => 'a',
			chr(225).chr(186).chr(168) => 'A', chr(225).chr(186).chr(169) => 'a',
			chr(225).chr(186).chr(178) => 'A', chr(225).chr(186).chr(179) => 'a',
			chr(225).chr(186).chr(186) => 'E', chr(225).chr(186).chr(187) => 'e',
			chr(225).chr(187).chr(130) => 'E', chr(225).chr(187).chr(131) => 'e',
			chr(225).chr(187).chr(136) => 'I', chr(225).chr(187).chr(137) => 'i',
			chr(225).chr(187).chr(142) => 'O', chr(225).chr(187).chr(143) => 'o',
			chr(225).chr(187).chr(148) => 'O', chr(225).chr(187).chr(149) => 'o',
			chr(225).chr(187).chr(158) => 'O', chr(225).chr(187).chr(159) => 'o',
			chr(225).chr(187).chr(166) => 'U', chr(225).chr(187).chr(167) => 'u',
			chr(225).chr(187).chr(172) => 'U', chr(225).chr(187).chr(173) => 'u',
			chr(225).chr(187).chr(182) => 'Y', chr(225).chr(187).chr(183) => 'y',
			// tilde
			chr(225).chr(186).chr(170) => 'A', chr(225).chr(186).chr(171) => 'a',
			chr(225).chr(186).chr(180) => 'A', chr(225).chr(186).chr(181) => 'a',
			chr(225).chr(186).chr(188) => 'E', chr(225).chr(186).chr(189) => 'e',
			chr(225).chr(187).chr(132) => 'E', chr(225).chr(187).chr(133) => 'e',
			chr(225).chr(187).chr(150) => 'O', chr(225).chr(187).chr(151) => 'o',
			chr(225).chr(187).chr(160) => 'O', chr(225).chr(187).chr(161) => 'o',
			chr(225).chr(187).chr(174) => 'U', chr(225).chr(187).chr(175) => 'u',
			chr(225).chr(187).chr(184) => 'Y', chr(225).chr(187).chr(185) => 'y',
			// acute accent
			chr(225).chr(186).chr(164) => 'A', chr(225).chr(186).chr(165) => 'a',
			chr(225).chr(186).chr(174) => 'A', chr(225).chr(186).chr(175) => 'a',
			chr(225).chr(186).chr(190) => 'E', chr(225).chr(186).chr(191) => 'e',
			chr(225).chr(187).chr(144) => 'O', chr(225).chr(187).chr(145) => 'o',
			chr(225).chr(187).chr(154) => 'O', chr(225).chr(187).chr(155) => 'o',
			chr(225).chr(187).chr(168) => 'U', chr(225).chr(187).chr(169) => 'u',
			// dot below
			chr(225).chr(186).chr(160) => 'A', chr(225).chr(186).chr(161) => 'a',
			chr(225).chr(186).chr(172) => 'A', chr(225).chr(186).chr(173) => 'a',
			chr(225).chr(186).chr(182) => 'A', chr(225).chr(186).chr(183) => 'a',
			chr(225).chr(186).chr(184) => 'E', chr(225).chr(186).chr(185) => 'e',
			chr(225).chr(187).chr(134) => 'E', chr(225).chr(187).chr(135) => 'e',
			chr(225).chr(187).chr(138) => 'I', chr(225).chr(187).chr(139) => 'i',
			chr(225).chr(187).chr(140) => 'O', chr(225).chr(187).chr(141) => 'o',
			chr(225).chr(187).chr(152) => 'O', chr(225).chr(187).chr(153) => 'o',
			chr(225).chr(187).chr(162) => 'O', chr(225).chr(187).chr(163) => 'o',
			chr(225).chr(187).chr(164) => 'U', chr(225).chr(187).chr(165) => 'u',
			chr(225).chr(187).chr(176) => 'U', chr(225).chr(187).chr(177) => 'u',
			chr(225).chr(187).chr(180) => 'Y', chr(225).chr(187).chr(181) => 'y',
			// Vowels with diacritic (Chinese, Hanyu Pinyin)
			chr(201).chr(145) => 'a',
			// macron
			chr(199).chr(149) => 'U', chr(199).chr(150) => 'u',
			// acute accent
			chr(199).chr(151) => 'U', chr(199).chr(152) => 'u',
			// caron
			chr(199).chr(141) => 'A', chr(199).chr(142) => 'a',
			chr(199).chr(143) => 'I', chr(199).chr(144) => 'i',
			chr(199).chr(145) => 'O', chr(199).chr(146) => 'o',
			chr(199).chr(147) => 'U', chr(199).chr(148) => 'u',
			chr(199).chr(153) => 'U', chr(199).chr(154) => 'u',
			// grave accent
			chr(199).chr(155) => 'U', chr(199).chr(156) => 'u',
			);

			// Used for locale-specific rules
			$locale = 'pt_BR';

			if ( 'de_DE' == $locale ) {
				$chars[ chr(195).chr(132) ] = 'Ae';
				$chars[ chr(195).chr(164) ] = 'ae';
				$chars[ chr(195).chr(150) ] = 'Oe';
				$chars[ chr(195).chr(182) ] = 'oe';
				$chars[ chr(195).chr(156) ] = 'Ue';
				$chars[ chr(195).chr(188) ] = 'ue';
				$chars[ chr(195).chr(159) ] = 'ss';
			} elseif ( 'da_DK' === $locale ) {
				$chars[ chr(195).chr(134) ] = 'Ae';
	 			$chars[ chr(195).chr(166) ] = 'ae';
				$chars[ chr(195).chr(152) ] = 'Oe';
				$chars[ chr(195).chr(184) ] = 'oe';
				$chars[ chr(195).chr(133) ] = 'Aa';
				$chars[ chr(195).chr(165) ] = 'aa';
			}

			$string = strtr($string, $chars);

		return $string;
	}

function my_json_decode($s, $singlequoted = true) {
	global $config;
	$s = mb_convert_encoding($s, 'UTF-8', 'UTF-8');
	// change all escaped double quotes to double quotes:
	$s = str_replace("\\\"","\"",$s);
	$s = str_replace("\\\\\"","\\\"",$s);

	if ($singlequoted) {
		// transform all escaped single quotes in ~|~|~|~|~|~|~|~|~|~|:
		$s = str_replace("\\'","~|~|~|~|~|~|~|~|~|~|",$s);
		// transform all double quotes outside single quotes in ||||||||||
		$s = preg_replace('/"(?=(?:(?:[^\']*+\'){2})*+[^\']*+\z)/','||||||||||',$s);
		// now replace all double quotes inside single quotes by |z|z|z|z|z|z|z|z|z|z
		$s = str_replace("\"","|z|z|z|z|z|z|z|z|z|z",$s);
		// now replace all single quotes by double quotes:
		$s = str_replace("'","\"",$s);
		// now replace all |||||||||| back to double quotes:
		$s = str_replace("||||||||||","\"",$s);
	}
	// Now prepare some things more:
	//$s = str_replace(array("[,",",]","\\\"","\\\\n", "\\n","\\\\u"),array("[\"\",",",\"\"]","\"","\\n","","\\u"),$s);
	$s = preg_replace('/\r\n?/', "\n", $s);
	$s = str_replace(array(
			"[,",
			",]",
			", ]",
			",}",
			", }",
			" : {",
			"\\\\n",
			"\\n",
			"\n",
			"\t",
			"[]"
		),array(
			"[\"\",",
			",\"\"]",
			",\"\"]",
			"}",
			"}",
			":{",
			"\\n",
			"",
			"",
			" ",
			"\"\""
		),$s);

	while (strpos($s,',true,')>0 || strpos($s,',false,')>0) {
		$s = str_replace(array(",true,", ",false,"), array(",\"true\",", ",\"false\","), $s);
	}
	// and now replace the ~|~|~|~|~|~|~|~|~|~| back to single quotes, but now not escaped anymore:
	$s = str_replace("~|~|~|~|~|~|~|~|~|~|","'",$s);

	while (strpos($s,',,')>0) {
		$s = str_replace(",,",",\"\",", $s);
	}
	// transform all ':' outside quotes in ||||||||||
	$s = preg_replace('/:(?=(?:(?:[^"]*+"){2})*+[^"]*+\z)/','||||||||||',$s);
	// now replace all ':' inside quotes by ~|~|~|~|~|~|~|~|~|~|
	$s = str_replace(':','~|~|~|~|~|~|~|~|~|~|',$s);
	// make all |||||||||| back to ':':
	$s = str_replace('||||||||||',':',$s);
	// now give quotes to all fields. They are defined as the word before ':':
  $s = preg_replace('/(\w+):/i', '"\1":', $s);
  // we're done, so we can put back the ':' inside quotes and the double quotes escaped:
  $s = str_replace('~|~|~|~|~|~|~|~|~|~|',':',$s);
  $s = str_replace('|z|z|z|z|z|z|z|z|z|z',"\\\"",$s);
  //pR($s);exit;
  $ret = json_decode($s, false, 1024, JSON_BIGINT_AS_STRING);
  if ($config->debug) {
  	if (!$ret) {
  		echo _("I had an error processing json_decode. The error is").": <i>".json_last_error_msg()."</i>";
  	}
  	echo "<p>"._("Processed string").":</p>\n";
  	pR($s);
  }
  return $ret;
}

function json_clean($json) {
	$json = trim($json);
	while (strpos($json,"  ")!==false || strpos($json,"\t")!==false || strpos($json,"\n")!==false)
		$json = str_replace(array("  ","\t","\n")," ",$json);
	while (strpos($json,'] ')!==false || strpos($json,'} ')!==false || strpos($json,'], ')!==false || strpos($json,'}, ')!==false || strpos($json,' [')!==false || strpos($json,' {')!==false || strpos($json,'[ ')!==false || strpos($json,'{ ')!==false || strpos($json," :")!==false) {
		$json = str_replace(
			array("] ","} ","], ","}, "," ["," {","[ ","{ ","}{", "]["," :"),
			array("]", "}", "]",  "}",  "[", "{", "[", "{", "},{","],[",":"),$json);
	}
	$json = preg_replace('/\](\w+)/i', '],\1', $json);
	return $json;
}

function show_table($data,$id='table-responsa') {
	global $config;
	$titles = array_keys(get_object_vars($data[0]));
	if (count($data)==1) {
		?>
	<table id="<?= $id ?>" class="table table-striped table-condensed table-bordered">
		<?php
	} else {
		?>
	<table id="<?= $id ?>" class="table table-striped table-condensed table-bordered bootstrap-datatable datatable responsive">
		<?php
	}
	?>
    <thead>
    <tr>
    		<?php foreach ($titles as $title) {
    			if ($title!='trId') {
		  		?>
		  			<th><?= (isset($config->fields[$title])) ? $config->fields[$title] : $title ?></th>
		  		<?php } ?>
		  	<?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $d) {
    	if (isset($d->trId)) {
    		$trId = " id='".$d->trId."'";
    	} else {
    		$trId = "";
    	}
    ?>
    	<tr<?= $trId ?>>
    	<?php foreach ($titles as $title) { ?>
    		<?php if ($title!='trId') { ?>
        	<td><?= $d->{$title} ?></td>
        <?php } ?>
      <?php } ?>
    	</tr>
    <?php } ?>
    </tbody>
    </table>
	<?php
}

function convert_uf_state($s,$reverse=false) {
	$states = array(
		"acre"=>"AC",
		"alagoas"=>"AL",
		"amapa"=>"AP",
		"amazonas"=>"AM",
		"bahia"=>"BA",
		"ceara"=>"CE",
		"distrito-federal"=>"DF",
		"espirito-santo"=>"ES",
		"goias"=>"GO",
		"maranhao"=>"MA",
		"mato-grosso"=>"MT",
		"mato-grosso-do-sul"=>"MS",
		"minas-gerais"=>"MG",
		"para"=>"PA",
		"paraiba"=>"PB",
		"parana"=>"PR",
		"pernambuco"=>"PE",
		"piaui"=>"PI",
		"rio-de-janeiro"=>"RJ",
		"rio-grande-do-norte"=>"RN",
		"rio-grande-do-sul"=>"RS",
		"rondonia"=>"RO",
		"roraima"=>"RR",
		"santa-catarina"=>"SC",
		"sao-paulo"=>"SP",
		"sergipe"=>"SE",
		"tocantins"=>"TO"
	);
	if ($reverse) {
		$states = array_flip($states);
	}
	return $states[sanitize($s)];
}

function get_territories($typeFilter=array()) {
	$handle = fopen ("data/Brasil/territories_for_sql_with_subdistritos.csv","r");
	$loc = new StdClass();
	//$loc->states = $loc->cities = $loc->subcities = $loc->states_cities = $loc->states_subcities = $loc->territories = array();
	$line = fgetcsv($handle, 1000, ",");
	$loc->territories = array();
	while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$len = strlen($line[0]);
		if (in_array($len,array(2,7,11))) {
			$id = sanitize($line[0]);
			$name = $line[1];
			$slug = sanitize($name);
			$parent_id = $line[3];
			$state_id = ($len>2) ? substr($id,0,2) : "";
			//$loc->territories[$id]=$name;
			$t = new StdClass();
			switch($len) {
				case 1:
					$t->type='region';
				break;
				case 2:
					$t->type='state';
				break;
				case 4:
					$t->type='substate_region';
				break;
				case 7:
					$t->type='city';
				break;
				case 11:
					$t->type='subcity_region';
				break;
			}
			if (count($typeFilter)==0 || in_array($t->type, $typeFilter)) {
				$t->name = $name;
				$t->slug = $slug;
				if ($line[2]) {
					$t->alias = strtoupper(sanitize($line[2]));
				}
				if ($line[4] && $line[5]) {
					$t->lat = $line[4];
					$t->lng = $line[5];
				}
				$t->parent_id = $parent_id;
				if ($len>2) {
					$t->state = $loc->territories[$state_id]->alias;
				}
				if ($len==11) {
					$city_id = substr($id,0,7);
					// Just in case the city is not in the database:
					if (!isset($loc->territories[$city_id])) {
						$tt = $t;
						$tt->type = 'city';
						$loc->territories[$city_id] = $tt;
					}

					$city_name = $loc->territories[$city_id]->name;
					$t->label = $city_name." / ".$loc->territories[$state_id]->alias;
					if ($t->name != $city_name) {
						$t->label .= " - ".$t->name;
					}
					$t->region_id = $line[6];
				}
				$loc->territories[$id] = $t;

				/*
				if($len==2) {
					$loc->states[$id]=$slug;
				} elseif($len==7) {
					$loc->cities[$id]=$slug;
					$loc->states_cities[$state_id][$slug]=$id;
				} elseif($len==11) {
					$loc->cities[$id]=$slug;
					if (!isset($loc->states_subcities[$state_id][$parent_id])) {
						$loc->states_subcities[$state_id][$parent_id] = array();
					}
					$loc->states_subcities[$state_id][$parent_id][$slug]=$id;
				}
				*/

			}
		}
	}
	foreach ($loc->territories as $id=>$territory) {
		if (strlen($id)==11) {
			$loc->territories[substr($id,0,7)]->region_id = $territory->region_id;
		}
	}
	return $loc;
}

function make_edit_initiative_form($item) {
	$objectId = $item->remote_id;

	$html = '
		<div class="card">
			<div class="card-block">
				<h4 class="card-title">'.$item->title.'</h4>
				<div class="card-text">
					<p>'.$item->territory_label.'</p>
				</div>
				<form class="form-horizontal">
	';
	$field = 'title';
	$label = _("Title");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label);

	$html .= '
					<div class="form-group">
						<label for="description__'.$objectId.'" class="col-sm-2 control-label">'._('description').'</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="description__'.$objectId.'" rows="3">'.$item->description.'</textarea>
						</div>
					</div>
	';

	$field = 'address';
	$label = _("Address");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label);

	$field = 'zip_code';
	$label = _("Zip Code");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label, 'firstHalf');

	$field = 'category';
	$label = _("Category");
	$html .= make_review_initiative_select($objectId, $field, $item->{$field}, $label, 'secondHalf');

	if (isset($item->geolocation) && $item->geolocation) {
		$geolocation = json_decode($item->geolocation);
		$lat = $geolocation->latitude;
		$lng = $geolocation->longitude;
	} elseif (isset($item->lat) && isset($item->lng)) {
		$lat = $item->lat;
		$lng = $item->lng;
	} else {
		$lat = $lng = "";
	}
	$field = "lat";
	$label = _("Lat");
	$valueL = $lat;
	$html .= make_review_initiative_input($objectId, $field, $valueL, $label, 'firstHalf');
	$field = "lng";
	$label = _("Lng");
	$valueL = $lng;
	$html .= make_review_initiative_input($objectId, $field, $valueL, $label, 'secondHalf');
	//$html .= '<div class="col-md-12"><a class="btn btn-md btn-default get-lat-lng-modal" id="get_latlng_modal__'.$objectId.'"><span class="glyphicon glyphicon-globe"></span> '._("Get coordinates from map").'</a></div>';
	$html .= '<div class="col-md-12"><a href="http://www.mapcoordinates.net/pt" target="_blank" class="btn btn-md btn-default"><span class="glyphicon glyphicon-globe"></span> '._("Get coordinates from map").'</a></div>';

	$field = 'phone1';
	$label = _("Phone").' 1';
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label, 'firstHalf');

	$field = 'phone2';
	$label = _("Phone").' 2';
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label, 'secondHalf');

	$field = 'email';
	$label = _("Email");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label);

	$field = 'website';
	$label = _("Website");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label, 'firstHalf');

	$field = 'facebook';
	$label = _("Facebook");
	$html .= make_review_initiative_input($objectId, $field, $item->{$field}, $label, 'secondHalf');

	$html .= '
					<div class="form-group">
						<label for="reviewerNotes__'.$objectId.'" class="col-sm-2 control-label">'._('Reviewer Notes').'</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="reviewerNotes__'.$objectId.'" rows="3">'.$item->reviewer_notes.'</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="reviewerInternalComments__'.$objectId.'" class="col-sm-2 control-label">'._('Reviewer internal comments').'</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="reviewerInternalComments__'.$objectId.'" rows="3">'.$item->reviewer_internal_comments.'</textarea>
						</div>
					</div>
			</form>
		</div>
	</div>
	';

	return $html;
}

function make_review_initiative_input($objectId, $field, $value, $label, $half = null) {
	if (!$half) {
		$colLabel = '2';
		$colInput = '10';
	} else {
		$colLabel = '2';
		$colInput = '4';
	}
	$html = '';
	if (!$half || $half=='firstHalf') {
		$html .= '
			<div class="form-group">
		';
	}
	$html .= '
				<label for="'.$field.'__'.$objectId.'" class="col-sm-'.$colLabel.' control-label">'.$label.'</label>
				<div class="col-sm-'.$colInput.'">
					<input type="text" class="form-control" id="'.$field.'__'.$objectId.'" value="'.htmlspecialchars($value).'">
				</div>
	';
	if (!$half || $half=='secondHalf') {
		$html .= '
			</div>
		';
	}
		return $html;
}

function make_review_initiative_select($objectId, $field, $value, $label, $half = null, $addAllCatsOption = false) {
    if ($field!='category') {
        return false;
    }
    global $config;
	if (!$half) {
		$colLabel = '2';
		$colInput = '10';
	} else {
		$colLabel = '2';
		$colInput = '4';
	}
	$options = ($addAllCatsOption) ? '<option value="">'._("All categories").'</option>' : '';
	foreach ($config->categories as $catSlug=>$cat) {
	    $selTxt = ($catSlug==$value) ? " selected" : "";
	    $options .= "<option value='$catSlug'$selTxt>".$cat[0]."</option>";
	}
	$html = '';
	if (!$half || $half=='firstHalf') {
		$html .= '
			<div class="form-group">
		';
	}
	$objIdTxt = ($objectId) ? '__'.$objectId : '';
	$html .= '
				<label for="'.$field.$objIdTxt.'" class="col-sm-'.$colLabel.' control-label">'.$label.'</label>
				<div class="col-sm-'.$colInput.'">
					<select class="form-control" name="'.$field.$objIdTxt.'" id="'.$field.$objIdTxt.'">
					    '.$options.'
					</select>
				</div>
	';
	if (!$half || $half=='secondHalf') {
		$html .= '
			</div>
		';
	}
		return $html;
}

function make_html_cell($item) {
	global $config;
	ob_start();
	?>
	<div class="row">
	<?php if (isset($item->avatar) && $item->avatar) { ?>
		<div class="col-lg-3 col-md-6 col-sm-12">
			<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
		</div>
		<div class="col-lg-9 col-md-6 col-sm-12">
	<?php } else { ?>
		<div class="col-lg-12">
	<?php } ?>
			<?php if (isset($item->description)) { ?>
				<p class='text-info'><?= get_excerpt($item->description,300) ?></p>
			<?php } ?>
			<p class='text-info'><label><?= _("Local ID") ?>:</label> <?= $item->local_id ?></p>
			<p class='text-info'><?= get_labeled_sources($item->source) ?></p>
			<?php if (isset($item->updated_at)) { ?>
				<p class='text-info'><label><?= _("Last updated at") ?>:</label> <?= make_date($item->updated_at) ?></p>
			<?php } ?>
			<?php if (isset($item->created_at)) { ?>
				<p class='text-info'><label><?= _("Created at") ?>:</label> <?= make_date($item->created_at) ?></p>
			<?php } ?>
		</div>
	</div>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function make_html_card($item,$buttons=array(),$size_lg=6,$size_md=12,$size_sm=12) {
	global $config;
	if (!$item->title) {
		return false;
	}
	$titleRaw = get_excerpt($item->title,40);
	$title = (isset($item->url) && $item->url) ? "<a title='".$item->title."' href='".$item->url."'>$titleRaw</a>" : $titleRaw;
	$title_full = (isset($item->url) && $item->url) ? "<a title='".$item->title."' href='".$item->url."'>".$item->title."</a>" : $item->title;
	ob_start();
	?>
	<div id="card__<?= $item->source[0]->source ?>__<?= $item->source[0]->local_id ?>" class="box col-lg-<?= $size_lg ?> col-md-<?= $size_md ?> col-sm-<?= $size_sm ?>">
		<div class='box-inner'>
			<div class="box-header well" data-original-title="<?= $titleRaw ?>">
				<h2 class="title"><?= $title ?> <?= get_labeled_sources($item->source) ?></h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-down"></i></a>
					<?php foreach ($buttons as $button) { ?>
						<?= $button ?>
					<?php } ?>
				</div>
			</div>
		<div class="box-content" style="display:none">
			<div class="row">
				<?php if (isset($item->avatar) && $item->avatar) { ?>
					<div class="col-lg-3 col-md-6 col-sm-12">
						<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
					</div>
					<div class="col-lg-9 col-md-6 col-sm-12">
				<?php } else { ?>
					<div class="col-lg-12">
				<?php } ?>
							<p class="text-info">
							<label><?= _("Title") ?>:</label> <?= $title_full ?><br />
							<?php if (isset($item->description)) { ?>
								<label><?= _("Description") ?>:</label> <i><?= get_excerpt($item->description) ?></i><br />
							<?php } ?>
							<?php if ($item->source) { ?>
								<label><?= _("Source") ?>:</label> <?= get_labeled_sources($item->source) ?><br />
							<?php } ?>
								<label><?= _("Local ID") ?>:</label> <?= $item->local_id ?><br />
							<?php if (isset($item->updated_at)) { ?>
								<label><?= _("Last updated at") ?>:</label> <?= make_date($item->updated_at) ?><br />
							<?php } ?>
							<?php if (isset($item->created_at)) { ?>
								<label><?= _("Created at") ?>:</label> <?= make_date($item->created_at) ?><br />
							<?php } ?>
							</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function make_html_popup($item) {
	global $config;
	if (!$item->title) {
		return false;
	}
	$titleRaw = $item->title;
	$title = (isset($item->url) && $item->url) ? "<a title='".$item->title."' href='".$item->url."'>$titleRaw</a>" : $titleRaw;
	$title_full = (isset($item->url) && $item->url) ? "<a href='".$item->url."'>".$item->title."</a>" : $item->title;
	if (isset($item->category) && $cat = $config->categories[$item->category]) {
		$category = _($cat[0]);
		$title = "<span title='".$category."' class='responsa-category-icon fa fa-".$cat[1]."'></span> ".$title;
	}
	$territory_label = $item->territory_label;
	ob_start();
	?>
		<div id="responsa_popup">
			<div class="row">
				<h2><?= $title ?></h2>
			</div>
			<div class="row">
				<?php if (isset($item->avatar) && $item->avatar) { ?>
					<div class="col-lg-6">
						<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
					</div>
					<div class="col-lg-6">
				<?php } else { ?>
					<div class="col-lg-12">
				<?php } ?>
							<p class="text-info">
							<?php if (isset($category)) { ?>
								<label><?= _("Category") ?>:</label> <i><?= $category ?></i><br />
							<?php } ?>
								<label><?=("territory")?>:</label> <?= $territory_label ?><br />
							<?php if (isset($item->description)) { ?>
								<label><?= _("Description") ?>:</label> <i><?= get_excerpt($item->description, 40) ?></i><br />
							<?php } ?>
							<?php if ($item->source) { ?>
								<label><?= _("Source") ?>:</label> <?= get_labeled_sources($item->source) ?><br />
							<?php } ?>
								<label><?= _("Local ID") ?>:</label> <?= $item->local_id ?><br />
							<?php if (isset($item->updated_at)) { ?>
								<label><?= _("Last updated at") ?>:</label> <?= make_date($item->updated_at) ?><br />
							<?php } ?>
							<?php if (isset($item->created_at)) { ?>
								<label><?= _("Created at") ?>:</label> <?= make_date($item->created_at) ?><br />
							<?php } ?>
							<?php if ($item->url) { ?>
								<a class="btn btn-default btn-sm" style="float:right" target="_blank" href="<?= $item->url ?>"><?= _("Details") ?></a>
							<?php } ?>
							</p>
					</div>
				</div>
			</div>
		</div>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return str_replace(array("\n","\t"),"",$html);
}

function make_user_html_popup($item) {
	global $config;
	if (!$item->name) {
		return false;
	}
	$title = $item->name;
	$territory_label = $item->territoryLabel;
	ob_start();
	?>
		<div id="responsa_popup">
			<div class="row">
				<h2><?= $title ?></h2>
			</div>
			<div class="row">
				<?php if (isset($item->avatar) && $item->avatar) { ?>
					<div class="col-lg-6">
						<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
					</div>
					<div class="col-lg-6">
				<?php } else { ?>
					<div class="col-lg-12">
				<?php } ?>
							<p class="text-info">
								<label><?=("territory")?>:</label> <?= $territory_label ?><br />
								<label><?= _("Local ID") ?>:</label> <?= $item->objectId ?><br />
							<?php if (isset($item->phone)) { ?>
								<label><?= _("Phone") ?>:</label> <?= $item->phone ?><br />
							<?php } ?>
							<?php if (isset($item->updatedAt)) { ?>
								<label><?= _("Last updated at") ?>:</label> <?= make_date($item->updatedAt) ?><br />
							<?php } ?>
							<?php if (isset($item->createdAt)) { ?>
								<label><?= _("Created at") ?>:</label> <?= make_date($item->createdAt) ?><br />
							<?php } ?>
							</p>
					</div>
				</div>
			</div>
		</div>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return str_replace(array("\n","\t"),"",$html);
}

function make_html_duplicate_merger_modal($ref, $dup) {
	global $config;

	$rows = array(
							array(
								'field' => 'title',
								'label' => _("Title")),
							array(
								'field' => 'description',
								'label' => _("Description")),
							array(
								'field' => 'address',
								'label' => _("Address")),
							array(
								'field' => 'zipCode',
								'label' => _("Zip Code")),
							array(
								'field' => 'category',
								'label' => _("Category")),
							array(
								'field' => "lat",
								'label' => _("Lat")),
							array(
								'field' => "lng",
								'label' => _("Lng")),
							array(
								'field' => 'phone1',
								'label' => _("Phone").' 1'),
							array(
								'field' => 'phone2',
								'label' => _("Phone").' 2'),
							array(
								'field' => 'email',
								'label' => _("Email")),
							array(
								'field' => 'website',
								'label' => _("Website")),
							array(
								'field' => 'facebook',
								'label' => _("Facebook"))
						);

	ob_start();
	?>
	<form class="form-horizontal" id="merge_duplicates_form">
	<?php
	foreach ($rows as $row) {
		if ((!isset($ref->{$row['field']}) || !$ref->{$row['field']}) &&
			(!isset($dup->{$row['field']}) || !$dup->{$row['field']})) {
			continue;
		}
		if ($row['field']=='description') {
			if (isset($ref->description)) {
				$ref->description = get_excerpt($ref->description, 300);
			}
			if (isset($dup->description)) {
				$dup->description = get_excerpt($dup->description, 300);
			}
		}
		if (isset($ref->{$row['field']}) && $ref->{$row['field']}) {
			$refChecked = " checked='checked'";
			$dupChecked = "";
		} else {
			$refChecked = "";
			$dupChecked = " checked='checked'";
		}

		?>
		<div class="row row-bordered">
			<div class="text-info col-sm-2"><label><?= $row['label'] ?></label></div>
			<div class="col-sm-4<?= ($refChecked) ? ' highlighted' : '' ?>" id="ref_<?= $row['field'] ?>"><?= $ref->{$row['field']} ?></div>
			<div class="col-sm-1 text-center"><input type="radio" name="<?= $row['field'] ?>" value="ref"<?= $refChecked ?>></div>
			<div class="col-sm-1 text-center"><input type="radio" name="<?= $row['field'] ?>" value="dup"<?= $dupChecked ?>></div>
			<div class="col-sm-4<?= ($dupChecked) ? ' highlighted' : '' ?>" id="dup_<?= $row['field'] ?>"><?= $dup->{$row['field']} ?></div>
		</div>
		<?php
	}
	?>
	</form>
	<div class="row row-bordered">
		<div class="text-info col-sm-2"><label><?= _('Local ID') ?></label></div>
		<div class="col-sm-4"><?= $ref->local_id ?></div>
		<div class="col-sm-2">&nbsp;</div>
		<div class="col-sm-4"><?= $dup->local_id ?></div>
	</div>
	<div class="row row-bordered">
		<div class="text-info col-sm-2"><label><?= _('Sources') ?></label></div>
		<div class="col-sm-4"><?= get_labeled_sources($ref->source) ?></div>
		<div class="col-sm-2">&nbsp;</div>
		<div class="col-sm-4"><?= get_labeled_sources($dup->source) ?></div>
	</div>
	<?php
	$ret->html = str_replace(array("\n","\t"),"",ob_get_contents());
	ob_end_clean();
	/*











	ob_start();
	?>
			<div class="row">
				<?php if (isset($item->avatar) && $item->avatar) { ?>
					<div class="col-lg-6">
						<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
					</div>
					<div class="col-lg-6">
				<?php } else { ?>
					<div class="col-lg-12">
				<?php } ?>
							<p class="text-info">
							<?php if (isset($category)) { ?>
								<label><?= _("Category") ?>:</label> <i><?= $category ?></i><br />
							<?php } ?>
							<?php if (isset($item->description)) { ?>
								<label><?= _("Description") ?>:</label> <i><?= get_excerpt($item->description, 600) ?></i><br />
							<?php } ?>
							<?php if ($item->source) { ?>
								<label><?= _("Source") ?>:</label> <?= get_labeled_sources($item->source) ?><br />
							<?php } ?>
								<label><?= _("Local ID") ?>:</label> <?= $item->local_id ?><br />
							<?php if (isset($item->updated_at)) { ?>
								<label><?= _("Last updated at") ?>:</label> <?= make_date($item->updated_at) ?><br />
							<?php } ?>
							<?php if (isset($item->created_at)) { ?>
								<label><?= _("Created at") ?>:</label> <?= make_date($item->created_at) ?><br />
							<?php } ?>
							<?php if ($item->url) { ?>
								<a class="btn btn-default btn-sm" style="float:right" target="_blank" href="<?= $item->url ?>"><?= _("Details") ?></a>
							<?php } ?>
							</p>
					</div>
				</div>
			</div>
	<?php
	$ret->html = str_replace(array("\n","\t"),"",ob_get_contents());
	ob_end_clean();
	*/



	return $ret;
}

function make_html_modal($item) {
	global $config;
	if (!$item->title) {
		return false;
	}
	$ret = new StdClass();

	$title = (isset($item->url) && $item->url) ? "<a title='".$item->title."' href='".$item->url."'>".$item->title."</a>" : $item->title;
	$title_full = (isset($item->url) && $item->url) ? "<a href='".$item->url."'>".$item->title."</a>" : $item->title;
	if (isset($item->category) && $cat = $config->categories[$item->category]) {
		$category = _($cat[0]);
		//$title = "<span title='".$category."' class='responsa-category-icon fa fa-".$cat[1]."'></span> ".$title;
	}
	$ret->title = $title;
	$ret->territory_label = $item->territory_label;
	$ret->category = $item->category;
	$ret->lat = $item->lat;
	$ret->lng = $item->lng;

	ob_start();
	?>
			<div class="row">
				<?php if (isset($item->avatar) && $item->avatar) { ?>
					<div class="col-lg-6">
						<p><img style="width:100%" src="<?= $item->avatar ?>" /></p>
					</div>
					<div class="col-lg-6">
				<?php } else { ?>
					<div class="col-lg-12">
				<?php } ?>
							<p class="text-info">
							<?php if (isset($category)) { ?>
								<label><?= _("Category") ?>:</label> <i><?= $category ?></i><br />
							<?php } ?>
							<?php if (isset($item->description)) { ?>
								<label><?= _("Description") ?>:</label> <i><?= get_excerpt($item->description, 600) ?></i><br />
							<?php } ?>
							<?php if (isset($item->address) && $item->address) { ?>
								<label><?= _("Address") ?>:</label> <?= $item->address ?>
								<?php if (isset($item->zip_code) && $item->zip_code) { ?>
									, <label><?= _("Zip code") ?>:</label> <?= $item->zip_code ?>
								<?php } ?>
								<br />
							<?php } ?>
							<?php if (isset($item->phone1) && $item->phone1) { ?>
								<label><?= _("Phone") ?>:</label> <?= $item->phone1 ?>
								<?php if (isset($item->phone2) && $item->phone2) { ?>
									, <?= $item->phone2 ?>
								<?php } ?>
								<br />
							<?php } ?>
								<!--<label><?= _("Local ID") ?>:</label> <?= $item->local_id ?><br />-->
							<?php if (isset($item->facebook) && $item->facebook) { ?>
								<label><?= _("Facebook") ?>:</label> <a target="_blank" href="<?= $item->facebook ?>"><?= $item->facebook ?></a><br />
							<?php } ?>
							<?php if (isset($item->created_at)) { ?>
								<label><?= _("Created at") ?>:</label> <?= make_date($item->created_at) ?><br />
							<?php } ?>
							<?php if (isset($item->updated_at)) { ?>
								<label><?= _("Last updated at") ?>:</label> <?= make_date($item->updated_at) ?><br />
							<?php } ?>
							<?php if (isset($item->source)) { ?>
								<label><?= _("Source") ?>:</label> <?= get_labeled_sources($item->source) ?><br />
							<?php } ?>
							<?php if (isset($item->url)) { ?>
								<a class="btn btn-default btn-sm" style="float:right" target="_blank" href="<?= $item->url ?>"><?= _("Details") ?></a>
							<?php } ?>
							</p>
					</div>
				</div>
			</div>
	<?php
	$ret->html = str_replace(array("\n","\t"),"",ob_get_contents());
	ob_end_clean();
	return $ret;
}

function generate_metadata($item) {
	global $config;
	if (!$item->title) {
		return false;
	}
	$ret = new StdClass();


	if (isset($item->category) && $cat = $config->categories[$item->category]) {
		$category = _($cat[0]);
	}
	$territory_label = $item->territory_label;
	$ret->title = $item->title;
	if (isset($item->avatar) && $item->avatar) {
		$ret->image = $item->avatar;
	}
	$ret->description = $item->description;
	$ret->lat = $item->lat;
	$ret->lng = $item->lng;

	// OpenGraph:
	$ret->og = new StdClass();
	$ret->og->title = $item->title;
	$ret->og->type = 'article';
	$ret->og->image = (isset($item->avatar) && $item->avatar)
		? $item->avatar
		: 'http://encontre.consumoresponsavel.org.br/assets/responsa/logo_consumo_responsavel.png';
	$ret->og->description = $item->description;
	$ret->og->article = new StdClass();
	$ret->og->article->published_time = $item->created_at;
	$ret->og->article->modified_time = $item->updated_at;
	$ret->og->article->tag = $category;
	$ret->og->place = new StdClass();
	$ret->og->place->location = new StdClass();
	$ret->og->place->location->latitude	= $item->lat;
	$ret->og->place->location->latitude	= $item->lng;
	$ret->og->locale = new StdClass();
	$ret->og->locale->locale = 'pt_BR';

	//Twitter:
	$ret->twitter = new StdClass();
	$ret->twitter->card = 'summary';
	$ret->twitter->title = $item->title;
	$ret->twitter->description = $item->description;

	//Business:
	$ret->business = new StdClass();
	$ret->business->contact_data = new StdClass();
	$ret->business->contact_data->email = $item->email;
	$ret->business->contact_data->phone_number = $item->phone1;
	$ret->business->contact_data->street_address = $item->address;
	$ret->business->contact_data->locality = $item->city;
	$ret->business->contact_data->region = $item->state;
	$ret->business->contact_data->postal_code = $item->zip_code;
	$ret->business->contact_data->country_name = $item->country;


	return $ret;
}

function get_single_initiative($source, $local_id) {
	global $config;
	if (!isset($config->data[$source]) || !$config->data[$source]) {
		return false;
	}
	$ret = false;
	foreach ($config->data[$source]->data as $item) {
		if ($item->local_id == $local_id) {
			$ret = $item;
			break;
		}
	}
	return $ret;
}

function generate_data_file_from_sources() {
	global $config;
	// Get data from sources:
	foreach ($config->data_sources as $f=>$v) {
		if (file_exists("data/data$v.json") || file_exists("data/rawData$v.json")) {
			$data = (file_exists("data/data$v.json"))
				? json_decode(file_get_contents("data/data$v.json"))
				: json_decode(file_get_contents("data/rawData$v.json"));
			$items = array();
			foreach ($data->data as $i=>$d) {
				$t = new StdClass();
				$t->source = $f;
				$t->local_id = $d->local_id;
				$data->data[$i]->source = array($t);
				$data->data[$i]->title_slug = sanitize($d->title);
			}
			$config->data[$f] = $data;
		}
	}

	// Generates the file data.json:
	update_data(null,null,null,false);
}

function get_dashboard_data($firstTime) {
	global $con, $config;

	$dashboardData = new StdClass();

	// USERS:
	$dashboardData->users = new StdClass();
	$users = get_users_from_DB();
	if (!$users->error) {
		$dashboardData->users->count = count($users);
		$dashboardData->users->citiesCount = number_format_i18(get_user_cities_from_DB(true));
		$dashboardData->users->status = $users[0]->name.", ".$users[0]->territoryLabel;
		$dashboardData->users->time = show_time_span($users[0]->createdAt);
	}


	// INITIATIVES
	if ($firstTime=='true') {
		$dashboardData->initiatives = new StdClass();
		$params = new StdClass();
		$params->limit = 1;
		$params->published = 1;
		$params->types = array('initiative');
		$initiatives = get_initiatives_from_DB($params, 'api');
		if (!$initiatives->error) {
			$dashboardData->initiatives->count = number_format_i18(count_initiatives());
			$dashboardData->initiatives->citiesCount = number_format_i18(get_cities_from_DB(true));
			$dashboardData->initiatives->sourcesCount = count($config->data_sources);
			$dashboardData->initiatives->status = $initiatives[0]->title.", ".$initiatives[0]->territory_label;
		}
	}

	// CHALLENGES AND CHALLENGE ANSWERS:
	$dashboardData->challenges = new StdClass();
	$dashboardData->challengeAnswers = new StdClass();
	$dashboardData->challenges->count = number_format_i18(get_quantity_of_challenges_from_DB());
	$dashboardData->challengeAnswers->count = number_format_i18(show_challenge_answers_grouped_by_challenge(true));
	$latestChallengeAnswer = show_challenge_answers_grouped_by_challenge(false, 1, false);
	if (!$latestChallengeAnswer->error) {
		$latestChallengeAnswer = array_shift(array_values($latestChallengeAnswer));
		$dashboardData->challengeAnswers->status = '"'.$latestChallengeAnswer[0]->description.'"';
		$dashboardData->challengeAnswers->time = show_time_span($latestChallengeAnswer[0]->createdAt);
	}

	// COMMENTS:
	$dashboardData->comments = new StdClass();
	$dashboardData->comments->count = number_format_i18(show_comments_per_initiative(true));
	$latestComment = show_comments_per_initiative(false, 1, false);
	if (!$latestComment->error) {
		$latestComment = array_shift(array_values($latestComment));
		$dashboardData->comments->status = '"'.$latestComment[0]->comment.'"';
		$dashboardData->comments->time = show_time_span($latestComment[0]->createdAt);
	}

	// UNREVIEWED INITIATIVES:
	$dashboardData->unreviewedInitiatives = new StdClass();
	$dashboardData->unreviewedInitiatives->count = number_format_i18(get_unreviewed_initiatives(true));
	$latestUnreviewedInitiative = get_unreviewed_initiatives(false, null, 1, false);
	if (!$latestUnreviewedInitiative->error) {
		$dashboardData->unreviewedInitiatives->status = $latestUnreviewedInitiative[0]['title'].", ".$latestUnreviewedInitiative[0]['territoryLabel'];
		$dashboardData->unreviewedInitiatives->time = show_time_span($latestUnreviewedInitiative[0]['createdAt']);
	}

	// PENDING FEEDBACKS:
	$dashboardData->pendingFeedbacks = new StdClass();
	$dashboardData->pendingFeedbacks->count = number_format_i18(get_all_unreviewed_feedbacks(true));
	$latestPendingFeedback = get_all_unreviewed_feedbacks(false, 1, false);
	if (!$latestPendingFeedback->error) {
		$dashboardData->pendingFeedbacks->status = '"'.$latestPendingFeedback[0]->userText.'"';
		$dashboardData->pendingFeedbacks->time = show_time_span($latestPendingFeedback[0]->createdAt);
	}


	return $dashboardData;
}

function get_initiatives_from_DB($params=null, $layout='raw', $territory_id=null, $show_hidden=false) {
	global $con, $config;
	$sql = "SELECT * FROM ResponsaObject";
	if ((isset($params->objectId) && $params->objectId)) {
		$sql .= " WHERE objectId='".$params->objectId."'";
	} else {
		$where = array();
		if (isset($params->sources) && count($params->sources)>0) {
			$where[] = (count($params->sources)>1)
				? "source IN ['".implode("','",$params->sources)."']"
				: "source='".$params->sources[0]."'";
		}
		if (isset($params->territoryId) && $params->territoryId) {
			$l = strlen($params->territoryId);
			if ($l==11) {
				$where[] = "territoryId='".$params->territoryId."'";
			} else {
				$where[] = "SUBSTRING(territoryId,0,$l)='".$params->territoryId."'";
			}
		}
		if (isset($params->categories) && count($params->categories)>0) {
			$where[] = (count($params->categories)>1)
				? "category IN ('".implode("','",$params->categories)."')"
				: "category='".$params->categories[0]."'";
		}
		if (isset($params->types) && count($params->types)>0) {
			$where[] = (count($params->types)>1)
				? "type IN ('".implode("','",$params->types)."')"
				: "type='".$params->types[0]."'";
		}
		if (isset($params->published) && $params->published) {
			$where[] = "published=1";
		}
		if (isset($params->reviewed) && $params->reviewed) {
			$where[] = "reviewed=1";
		}
		if (isset($params->localId)) {
			$where[] = "localId='".$params->localId."'";
		}
		if (isset($params->where)) {
			$where[] = "(".$params->where.")";
		}

		$sql .= " WHERE ".implode(" AND ", $where);
		$sql .= " ORDER BY createdAt DESC";
		if (isset($params->limit) && $params->limit>0) {
			$sql .= " LIMIT ".$params->limit;
		}
	}
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$loc = get_territories();
		$items = array();
		while ($r = $tmp->fetch_object()) {
			if (!isset($items[$r->source])) {
				$items[$r->source] = new StdClass();
				$items[$r->source]->data = array();
			}
			$t = new StdClass();
			foreach ($r as $field=>$value) {
				switch ($field) {
					case 'avatarImage':
						$ar = json_decode($value);
						$t->{$config->mapDB[$field]} = $ar->{'_url'};
					break;
					case 'geoLocation':
						$ar = json_decode($value);
						$t->lat = $ar->latitude;
						$t->lng = $ar->longitude;
					break;
					case 'sources':
						$t->source = json_decode($value);
					break;
					case 'created_at':
						$t->created_at = (isset($r->sourceCreatedAt) && $r->sourceCreatedAt)
							? $r->sourceCreatedAt
							: $r->createdAt;
					break;
					case 'updated_at':
						$t->updated_at = (isset($r->sourceUpdatedAt) && $r->sourceUpdatedAt)
							? $r->sourceUpdatedAt
							: $r->updatedAt;
					break;
					case 'territoryId':
						$t->state = $loc->territories[$value]->state;
						$t->city = $loc->territories[$loc->territories[$value]->parent_id]->name;
						if ($loc->territories[$value]->name != $t->city) {
							$t->subcity = $loc->territories[$value]->name;
						}
						$t->territory_id = $value;
					break;
					case 'source':
					case 'source_created_at':
					case 'source_updated_at':
					case 'territoryRegionId':
					break;
					default:
						if (isset($config->mapDB[$field])) {
							$t->{$config->mapDB[$field]} = $value;
						}
					break;
				}
			}
			$items[$r->source]->data[] = $t;
		}
		$res = export_to_layout($items, $layout, $territory_id, $show_hidden);
	}
	return $res;
}

function get_users_from_DB($params=null, $layout='raw') {
	global $con, $config;
	$sql = "SELECT a.* FROM _User a";
	if ((isset($params->objectId) && $params->objectId)) {
		$sql .= " WHERE a.objectId='".$params->objectId."'";
	} else {
		$where = array();
		$where[] = "a.name!='' AND a.objectId IN (SELECT userId FROM _Installation WHERE userId!='')";
		if (isset($params->territoryId) && $params->territoryId) {
			$where[] = "a.territoryId='".$params->territoryId."'";
		}
		if (isset($params->cityId) && $params->cityId) {
			$where[] = "SUBSTRING(a.territoryId,1,7) = '".$params->cityId."'";
		}
		if (isset($params->stateId) && $params->stateId) {
			$where[] = "SUBSTRING(a.territoryId,1,2) = '".$params->stateId."'";
		}
		if (isset($params->where)) {
			$where[] = "(".$params->where.")";
		}

		$sql .= " WHERE ".implode(" AND ", $where);
		$sql .= " ORDER BY a.createdAt DESC";
	}

	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$loc = get_territories();
		$items = array();
		while ($r = $tmp->fetch_object()) {
			$t = new StdClass();
			foreach ($r as $field=>$value) {
				switch ($field) {
					case 'avatar':
						$ar = json_decode($value);
						$t->{$field} = $ar->{'_url'};
					break;
					case 'createdAt':
						$t->createdAt = (isset($r->sourceCreatedAt) && $r->sourceCreatedAt)
							? $r->sourceCreatedAt
							: $r->createdAt;
					break;
					case 'updatedAt':
						$t->updatedAt = (isset($r->sourceUpdatedAt) && $r->sourceUpdatedAt)
							? $r->sourceUpdatedAt
							: $r->updatedAt;
					break;
					case 'territoryId':
						$t->state = $loc->territories[$value]->state;
						$t->city = $loc->territories[$loc->territories[$value]->parent_id]->name;
						if ($loc->territories[$value]->name != $t->city) {
							$t->subCity = $loc->territories[$value]->name;
						}
						$t->territoryId = $value;
						$t->lat = $loc->territories[$loc->territories[$value]->parent_id]->lat;
						$t->lng = $loc->territories[$loc->territories[$value]->parent_id]->lng;
					break;
					default:
							$t->{$field} = $value;
					break;
				}
			}
			$items[] = $t;
		}
		$res = ($layout=='raw')
			? $items
			: export_users_to_layout($items, $layout);
	}
	return $res;
}

function get_quantity_of_users_from_DB() {
	global $con, $config;
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	//$sql = "SELECT count(*) qtde FROM _User WHERE name!=''";
	$sql = "SELECT a.userId FROM _Installation a, _User b where a.userId=b.objectId and b.name!='' group by a.userId";

	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$res = $tmp->num_rows;
	}
	return $res;
}

function get_quantity_of_challenges_from_DB() {
	global $con, $config;
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	$sql = "SELECT count(*) qtde FROM Challenge";

	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$row = $tmp->fetch_assoc();
		$res = $row['qtde'];
	}
	return $res;
}

function get_cities_from_DB($countOnly = false) {
	global $con, $config;
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	$sql = "SELECT SUBSTRING( territoryId, 1, 7 ) city_id , ANY_VALUE(territoryLabel), count( * ) qtde
					FROM ResponsaObject
					WHERE type = 'initiative'
						AND published =1
						AND territoryId != ''
					GROUP BY SUBSTRING( territoryId, 1, 7 )
					ORDER BY qtde DESC
	";

	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res->cities = array();
		$res->count = $tmp->num_rows;
		while ($row = $tmp->fetch_assoc()) {
			$res->cities[$row['city_id']] = new StdClass();
			$res->cities[$row['city_id']]->qtde = $row['qtde'];
			$res->cities[$row['city_id']]->name = $row['territoryLabel'];
		}
	}
	return $res;
}

function get_user_cities_from_DB($countOnly=false) {
	global $con, $config;
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	$sql = "SELECT SUBSTRING( territoryId, 1, 7 ) city_id , ANY_VALUE(territoryLabel), count( * ) qtde
					FROM _User
					WHERE territoryId != ''
						AND name!=''
					GROUP BY SUBSTRING( territoryId, 1, 7 )
					ORDER BY qtde DESC
	";

	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res->cities = array();
		$res->count = $tmp->num_rows;
		while ($row = $tmp->fetch_assoc()) {
			$res->cities[$row['city_id']] = new StdClass();
			$res->cities[$row['city_id']]->qtde = $row['qtde'];
			$res->cities[$row['city_id']]->name = $row['territoryLabel'];
		}
	}
	return $res;
}

function get_all_initiatives($layout="table", $territory_id=false, $show_hidden=false) {
	global $config;
	if (!isset($config->data) || !is_array($config->data)) {
		if (file_exists("data/data.json")) {
			$config->data = json_decode(file_get_contents("data/data.json"));
		} else {
			generate_data_file_from_sources();
		}
		if (!isset($config->data) || !$config->data) {
			save_log('errors.log',date('j/m/Y, H\hi\ms\s'));
			save_log('errors.log', 'Error retrieving data from data.json or from sources.');
			return false;
		}
	}
	$data_from_sources = $config->data;
	return export_to_layout($data_from_sources, $layout, $territory_id, $show_hidden);
}

function export_to_layout($data, $layout="table", $territory_id=false, $show_hidden=false) {
	global $config;
	foreach ($data as $f=>$dataItems) {
		$items[$f] = array();
		foreach ($dataItems->data as $item) {
			if (
				(!$territory_id || substr($item->territory_id,0,strlen($territory_id))==$territory_id) &&
				($show_hidden || !isset($item->hide)) &&
				($item->lat && $item->lng)
				) {
				$t = new StdClass();
				switch($layout) {
					case "map":
						$t->local_id = $item->local_id;
						$t->lat = $item->lat;
						$t->lng = $item->lng;
						$t->title = $item->title;
						$t->popup = make_html_popup($item);
						$t->category = $item->category;
					break;
					case "table":
						$t->id = unique_id($item->source[0]->source, $item->source[0]->local_id);
						$t->title = (isset($item->url)) ? "<a href='".$item->url."' target='_blank'>".$item->title."</a>" : $item->title;
						$t->territory = $item->territory_label;
						$t->details = make_html_cell($item);
						$t->url = (isset($item->url)) ? "<a href='".$item->url."' target='_blank'>".$item->url."</a>" : "";
					break;
					case "csv":
					    $t = array();
                        $t['id'] = unique_id($item->source[0]->source, $item->source[0]->local_id);
                        $t['url_no_responsa'] = "http://mapa.consumoresponsavel.org.br/?id=".$t['id'];
						$t['title'] = $item->title;
						$t['title'] = $item->title;
						$t['state'] = $item->state;
						$t['city'] = $item->city;
						$t['subcity'] = $item->subcity;
						$t['address'] = $item->address;
						$t['url'] = $item->url;
						$t['lat'] = $item->lat;
						$t['lng'] = $item->lng;
						$t['avatar'] = $item->avatar;
						$t['description'] = $item->description;
						$t['sources'] = implode('|', get_sources_for_csv($item->source));
						$t['updated_at'] = $item->updated_at;
						$t['created_at'] = $item->created_at;
						$t['category'] = $item->category;
					case "geoJSON":
					case "geoJSONRaw":
						if (isset($item->category) && $item->category) {
							$t->type = "Feature";
							//$t->id = unique_id($item->source[0]->source,$item->local_id);
							$point = new StdClass();
							$point->type = "Point";
							$point->coordinates = array($item->lng, $item->lat);
							$t->geometry = $point;
							$props = new StdClass();
							$props->id = unique_id($item->source[0]->source,$item->local_id);
							$props->title = $item->title;
							$props->state = $item->state;
							$props->city = $item->city;
							$props->subcity = $item->subcity;
							$props->category = $item->category;
							/*foreach ($config->fields as $field=>$name) {
								if (isset($item->{$field}) && $item->{$field}) {
									$props->{$field} = $item->{$field};
								}
							}*/
							$t->properties = $props;
						}
					break;
					case "api":
					case "raw":
					default:
						$t = $item;
					break;
				}
				$items[$f][] = $t;
			}
		}
	}

	if ($layout=="raw") {
		$res = array();
		foreach ($data as $f=>$dataItems) {
			$res[$f] = $dataItems;
			$res[$f]->data = $items[$f];
		}
	} else {
		$res = array();
		foreach ($items as $f=>$data) {
			$res = array_merge($res, $data);
		}
		$final_res = array();
		if ($layout=="geoJSON") {
			foreach (array_keys($config->categories) as $category) {
				$t = new StdClass();
				$t->type = "FeatureCollection";
				$t->features = array();
				$final_res[$category] = $t;
			}
			foreach ($res as $r) {
				$final_res[$r->properties->category]->features[] = $r;
			}
			$res = $final_res;
		} elseif ($layout=='geoJSONRaw') {
			$t = new StdClass();
			$t->type = "FeatureCollection";
			$t->features = $res;
			$res = $t;
		} elseif ($layout=='singleRaw') {
			$res = $res[0];
		}
	}
	return $res;
}

function export_users_to_layout($items, $layout="table") {
	global $config;
	$res = array();
	foreach ($items as $item) {
		$t = new StdClass();
		switch($layout) {
			case "map":
				$t->objectId = $item->objectId;
				$t->lat = floatval($item->lat);
				$t->lng = floatval($item->lng);
				$t->title = $item->name;
				$t->popup = make_user_html_popup($item);
			break;
			case "table":
				$t->userId = $item->objectId;
				$t->name = $item->name;
				$t->territory = $item->territoryLabel;
				$t->created_at = make_date($item->createdAt);
				$t->updated_at = make_date($item->updatedAt);
				$t->phone = $item->phone;
			break;
		}
		$res[] = $t;
	}
	return $res;
}

function get_all_initiatives_organized_by_territory($territory_level = 'city') {
	global $config;
	switch ($territory_level) {
		case 'city':
			$length = 7;
		break;
		case 'subcity_region':
		default:
			$length = 11;
		break;
	}
	$data_from_sources = $config->data;
	$items_with_keys = array();
	foreach ($data_from_sources as $source=>$data) {
		$items = $data->data;
		foreach ($items as $item) {
			if (isset($item->territory_id) && $item->territory_id) {
				$territory_id = substr($item->territory_id,0,$length);
				if (!isset($items_with_keys[$territory_id])) {
					$items_with_keys[$territory_id] = array();
				}
				if (!isset($item->title_slug) || !$item->title_slug) {
					$item->title_slug = sanitize($item->title);
				}
				$items_with_keys[$territory_id][$item->title_slug."|".unique_id($item->source[0]->source,$item->local_id)] = $item;
			}
		}
	}
	ksort($items_with_keys);
	$items = array();
	foreach ($items_with_keys as $territory_id => $local_items) {
		$t = $local_items;
		ksort($t);
		$items[$territory_id] = array_values($t);
	}
	return $items;
}

function get_unreviewed_initiatives($countOnly=false, $objectId=null, $limit=null, $html=true, $category=null) {
	global $con, $config;
	if (!isset($con) || $con->error) {
		$res = new StdClass();
		$res->error = (isset($con->error)) ? $con->error : 42;
		return $res;
	}
	$sql = "SELECT a.*, b.name AS userName, b.territoryLabel AS userTerritoryLabel, b.phone AS userPhone FROM ResponsaObject a LEFT JOIN _User b ON a.ownerId=b.objectId WHERE a.type='initiative' AND a.reviewed=0 AND source='responsa'";
	if ($objectId) {
		$sql .= " AND a.objectId = '$objectId'";
	} else if ($category) {
		$sql .= " AND a.category = '$category'";
	}
	$sql .= " ORDER BY a.createdAt DESC";

	if ($limit && $limit>0) {
		$sql .= " LIMIT $limit";
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res = array();
		while ($row = $tmp->fetch_assoc()) {
			if ($html) {
				$item = map_item_from_DB($row);

				$objectId = $item->remote_id;
				$t = new StdClass();
				$t->trId = "tr-".$objectId;

				$t->actions = '
					<span class="text-nowrap">
						<a href="#" title="'._("Save").'" class="btn btn-xs btn-info review_initiatives" action="save" objectId="'.$objectId.'"><span class="glyphicon glyphicon-save"></span></a>
						<a href="#" title="'._("Approve").'" class="btn btn-xs btn-success review_initiatives" action="approve" objectId="'.$objectId.'"><span class="glyphicon glyphicon-ok"></span></a>
						<a href="#" title="'._("Reject").'" class="btn btn-xs btn-danger review_initiatives" action="reject" objectId="'.$objectId.'"><span class="glyphicon glyphicon-remove"></span></a>
					</span>
				';

				$t->owner = '<strong>'.$row['userName'].'</strong>';
				$t->owner .= '<br />'.$row['userTerritoryLabel'];
				$t->owner .= '<br />Tel: '.$row['userPhone'];
				$t->owner .= '<br />'._('Created at').': '.make_date($row['createdAt']);

				$t->initiative = make_edit_initiative_form($item);

				$res[] = $t;
			} else {
				$res[] = $row;
			}
		}
	}
	return $res;
}

function get_all_unreviewed_feedbacks($countOnly=false, $limit=null, $html=true) {
	global $con, $config;
	if (!isset($con) || $con->error) {
		$res = new StdClass();
		$res->error = (isset($con->error)) ? $con->error : 42;
		return $res;
	}
	$sql = "SELECT a.*, b.name AS userName, b.territoryLabel AS userTerritoryLabel, b.phone AS userPhone FROM Feedback a LEFT JOIN _User b ON a.userId=b.objectId WHERE a.reviewed=0 ORDER BY a.createdAt DESC";
	if ($limit && $limit>0) {
		$sql .= " LIMIT $limit";
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res = array();
		while ($item = $tmp->fetch_object()) {
			if (!$html) {
				$res[] = $item;
			} else {
				$objectId = $item->objectId;
				$t = new StdClass();
				$t->trId = "tr-".$objectId;

				$t->actions = '
					<span class="text-nowrap">
						<a href="#" title="'._("Save").'" class="btn btn-xs btn-info review_feedbacks" action="save" objectId="'.$objectId.'"><i class="glyphicon glyphicon-save"></i></a>
						<a href="#" title="'._("Approve").'" class="btn btn-xs btn-success review_feedbacks" action="approve" objectId="'.$objectId.'"><i class="glyphicon glyphicon-ok"></i></a>
					</span>
				';

				$t->info = '<label>'._('From').':</label> '.$item->userName.' ('.$item->userTerritoryLabel.')';
				$t->info .= '<br />Tel: '.$item->userPhone;
				$t->info .= '<br />'.make_date($item->createdAt);
				if ($item->view) {
					$t->info .= '<br/><br /><label>View:</label> '.$item->view;
				}
				if ($item->viewClass) {
					$t->info .= '<br /><label>ViewClass:</label> '.$item->viewClass;
				}
				if ($item->viewObjectId) {
					$t->info .= '<br /><label>ViewObjectId:</label> '.$item->viewObjectId.' <a href="?action=edit_published_initiatives&do_it=1&objectId='.$item->viewObjectId.'" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-edit"></span> '._("edit").'</a>';
				}


				$t->feedback = '<p class="alert alert-success">'.$item->userText.'</p>';
				$t->feedback .= '
				<form class="form-horizontal">
					<div class="form-group">
							<label for="reviewerNotes__'.$objectId.'" class="col-sm-2 control-label">'._('Reviewer Notes').'</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="reviewerNotes__'.$objectId.'" rows="3">'.$item->reviewerNotes.'</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="reviewerInternalComments__'.$objectId.'" class="col-sm-2 control-label">'._('Reviewer internal comments').'</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="reviewerInternalComments__'.$objectId.'" rows="3">'.$item->reviewerInternalComments.'</textarea>
							</div>
						</div>
				</form>
				';

				$res[] = $t;
			}
		}
	}
	return $res;
}

function show_comments_per_initiative($countOnly=false, $limit=false, $html=true) {
	global $con, $config;
	if (!isset($con) || $con->error) {
		$res = new StdClass();
		$res->error = (isset($con->error)) ? $con->error : 42;
		return $res;
	}
	if ($countOnly) {
		$sql = "SELECT a.objectId FROM Comment a, ResponsaObject c WHERE a.responsaObjectId = c.objectId AND c.published=1";
	} else {
		$sql = "SELECT a.*, b.name AS userName, b.territoryLabel AS userTerritoryLabel, b.phone AS userPhone, c.title as initiativeTitle, c.territoryLabel AS initiativeTerritoryLabel, c.type as initiativeType FROM Comment a LEFT JOIN _User b ON a.userId=b.objectId LEFT JOIN ResponsaObject c ON a.responsaObjectId=c.objectId WHERE c.published=1 ORDER BY a.createdAt DESC";
		if ($limit && $limit>0) {
			$sql .= " LIMIT $limit";
		}
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
		return $res;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res = array();
		$items = array();
		while ($item = $tmp->fetch_object()) {
			if (!isset($items[$item->responsaObjectId])) {
				$items[$item->responsaObjectId] = array();
			}
			$items[$item->responsaObjectId][] = $item;
		}

		if (!$html) {
			return $items;
		}

		$html = "";
		foreach ($items as $responsaObjectId=>$comments) {
			$titleLabel = ($comments[0]->initiativeType == 'initiative')
				? _('Initiative')
				: _('Gathering');

			$html .= '
				<div class="col-md-12">
					<h2>'.$comments[0]->initiativeTitle.' <a href="?action=edit_published_initiatives&do_it=1&objectId='.$responsaObjectId.'" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-edit"></span> '._("edit").'</a></h2>
					'.$comments[0]->initiativeTerritoryLabel.' - <label>'._("Type").':</label> '.$titleLabel.' - <label>objectId:</label> <a href="?action=edit_published_initiatives&do_it=1&objectId='.$responsaObjectId.'">'.$responsaObjectId.'</a>';

			/*$html .= '
					<div class="col-sm-4">
						<form class="form">
							<div class="form-group">
								<textarea class="form-control" id="reviewerComments__'.$responsaObjectId.'" rows="2"></textarea>
							</div>
						</form>
					</div>
					<div clas="col-sm-8">&nbsp;</div>
			';*/
			foreach ($comments as $comment) {
				if (!$comment->comment && !$comment->image) {
					continue;
				}
				$html .= '
						<div class="commentBlock col-sm-12">
				';
				if ($comment->comment) {
					$html .= '
							<div class="bubble col-sm-8">
								<p>'.$comment->comment.'</p>
							</div>
					';
				} elseif ($comment->image) {
					$image = json_decode($comment->image);
					$html .= '
							<div class="col-sm-8">
								<img src="'.$image->_url.'" style="max-width:800px">
							</div>
					';
				}
				$html .= '
							<div class="commentInfo col-sm-4">
								'.$comment->userName.'<br />'.$comment->userTerritoryLabel.'<br />'._("Phone").': '.$comment->userPhone.'<br />'.make_date($comment->createdAt).'
							</div>
						</div>
				';
			}
			$html .= '
				</div>
			';
		}
		echo $html;
		return null;
	}
}

function show_challenge_answers_grouped_by_challenge($countOnly=false, $limit=false, $html=true) {
	global $con, $config;
	if (!isset($con) || $con->error) {
		$res = new StdClass();
		$res->error = (isset($con->error)) ? $con->error : 42;
		return $res;
	}
	if ($countOnly) {
		$sql = "SELECT a.objectId FROM ChallengeAnswer a, Challenge b WHERE a.challengeId = b.objectId AND b.published=1";
	} else {
		$sql = "SELECT
							a.*,
							b.title as challengeTitle,
							b.description as challengeDescription,
							b.createdAt as challengeCreatedAt,
							b.correctAnswerId,
							b.relevance as challengeRelevance,
							c.name AS challengeAuthorName,
							c.territoryLabel AS challengeAuthorTerritoryLabel,
							c.phone AS challengeAuthorPhone,
							d.name AS challengeAnswerAuthorName,
							d.territoryLabel AS challengeAnswerAuthorTerritoryLabel,
							d.phone AS challengeAnswerAuthorPhone
						FROM
							ChallengeAnswer a,
							Challenge b,
							_User c,
							_User d
						WHERE
							c.objectId=b.authorId AND
							d.objectId=a.authorId AND
							a.challengeId = b.objectId AND
							b.published=1
						ORDER BY a.createdAt DESC";
	}
	if ($limit) {
		$sql .= " LIMIT $limit";
	}
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
		return $res;
	} else {
		if ($countOnly) {
			return $tmp->num_rows;
		}
		$res = array();
		$items = array();
		while ($item = $tmp->fetch_object()) {
			if (!isset($items[$item->challengeId])) {
				$items[$item->challengeId] = array();
			}
			$items[$item->challengeId][] = $item;
		}
		if (!$html) {
			return $items;
		}

		$html = "";
		foreach ($items as $challengeId=>$challengeAnswers) {
			$titleLabel = ($challengeAnswers[0]->correctAnswerId != '')
				? _('Discovery')
				: _('Challenge');

			$html .= '
				<div class="col-md-12">
					<h2>'.$challengeAnswers[0]->challengeTitle.'</h2>
					<h3>'.$challengeAnswers[0]->challengeDescription.'</h3>
					<label>'._("Type").':</label> '.$titleLabel.' - <label>'._("Relevance").': </label> '.$challengeAnswers[0]->challengeRelevance.' - <label>objectId:</label> '.$challengeId.'
					<br /><label>'._("Created at").'</label> '.make_date($challengeAnswers[0]->challengeCreatedAt).'
					<br /><label>'._("Created by").'</label> '.$challengeAnswers[0]->challengeAuthorName.' - '.$challengeAnswers[0]->challengeAuthorTerritoryLabel.' - <label>'._("Phone").':</label> '.$challengeAnswers[0]->challengeAuthorPhone;

			/*$html .= '
					<div class="col-sm-4">
						<form class="form">
							<div class="form-group">
								<textarea class="form-control" id="reviewerChallengeAnswers__'.$challengeId.'" rows="2"></textarea>
							</div>
						</form>
					</div>
					<div clas="col-sm-8">&nbsp;</div>
			';*/
			foreach ($challengeAnswers as $challengeAnswer) {
				if (!$challengeAnswer->description) {
					continue;
				}
				$html .= '
						<div class="commentBlock col-sm-12">
						<div class="bubble col-sm-8">
							<p>'.$challengeAnswer->description.'</p>
						</div>
							<div class="commentInfo col-sm-4">
								'.$challengeAnswer->challengeAnswerAuthorName.'<br />'.$challengeAnswer->challengeAnswerAuthorTerritoryLabel.'<br />'._("Phone").': '.$challengeAnswer->challengeAnswerAuthorPhone.'<br />'.make_date($challengeAnswer->createdAt).'<br />objectId: '.$challengeAnswer->objectId.'
							</div>
						</div>
				';
			}
			$html .= '
				</div>
			';
		}
		echo $html;
		return null;
	}
}

function map_item_from_DB($itemRaw) {
	if (!isset($itemRaw) || (!is_array($itemRaw) && !is_object($itemRaw))) {
		return false;
	}
	global $config;
	$item = new StdClass();
	foreach ($itemRaw as $f=>$v) {
		if (isset($config->mapDB[$f])) {
			$item->{$config->mapDB[$f]} = $v;
		}
	}
	return $item;
}

function count_initiatives($f="") {
	global $con;
	if (!isset($con)) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	if ($f) {
		//$c = count($config->data[$f]->data);
	} else {
		$sql = "SELECT count(*) AS initiatives FROM ResponsaObject WHERE type='initiative' and published=1";
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$res = $tmp->fetch_assoc();
			$res = $res['initiatives'];
		}
	}
	return $res;
}

function clean_all_duplicates() {
	global $config;

	if (!isset($config->duplicates) || !$config->duplicates) {
		$config->duplicates = get_all_duplicates();
		if (!$config->duplicates) {
			return false;
		}
	}
	$need_update = false;
	$new_dups = $config->duplicates;
	foreach ($config->duplicates as $f=>$d) {
		if (isset($d->refs)) {
			foreach ($d->refs as $local_id=>$ref) {
				$exists = check_if_exists_locally($f,$local_id);
				if ($exists && !$exists->error) {
					$changed = false;
					foreach ($ref as $i=>$r) {
						$exists = check_if_exists_locally($r->source,$r->local_id);
						if ($exists==false) {
							unset($new_dups[$f]->refs[$local_id][$i]);
							$need_update = true;
							$changed = true;
						} elseif (isset($exists->error)) {
							show_alert($exists);
						}
					}
					if ($changed) {
						$new_dups[$f]->refs[$local_id] = array_values($new_dups[$f]->refs[$local_id]);
					}
				} elseif (isset($exists->error)) {
					show_alert($exists);
				} else {
					unset($new_dups[$f]->refs[$local_id]);
					$need_update = true;
				}
			}
		}

		if (isset($d->hidden)) {
			foreach ($d->hidden as $local_id=>$hidden) {
				$exists = check_if_exists_locally($f,$local_id);
				if (!$exists) {
					unset($new_dups[$f]->hidden[$local_id]);
					$need_update = true;
				} elseif (isset($exists->error)) {
					show_alert($exists);
				}
			}
		}
	}

	$res = new StdClass();
	if ($need_update) {
		$config->duplicates = $new_dups;
		if (save_data("duplicates.json", $config->duplicates, false)) {
			$res->success = 1;
			$res->msg = _('The duplicates file had to be updated and the operation was successfull.');
		} else {
			$res->error = 1;
			$res->msg = _('The duplicates file had to be updated but I couldn\'t save it! Please check your permissions to duplicates.json file');
		}
	} else {
		$res->success = 1;
		$res->msg = _('The duplicates file was checked against the database, and there is nothing to update');
	}
	return $res;
}

function get_all_duplicates() {
	global $config;

	if (file_exists("data/duplicates.json")) {
		$duplicates = json_decode(file_get_contents("data/duplicates.json"));
	} else {
		return false;
	}
	$res = array();
	foreach ($duplicates as $f=>$d) {
		$res[$f] = new StdClass();
		$res[$f]->refs = $res[$f]->hidden = array();
		if (isset($d->refs)) {
			foreach ($d->refs as $local_id=>$ref) {
				$res[$f]->refs[$local_id] = $ref;
			}
		}
		if (isset($d->hidden)) {
			foreach ($d->hidden as $local_id=>$hidden) {
				$res[$f]->hidden[$local_id] = $hidden;
			}
		}
	}
	return $res;
}

function get_all_categories() {
	global $config;

	if (file_exists("data/categories.json")) {
		$categoriesRaw = json_decode(file_get_contents("data/categories.json"));
		$categories = array();
		foreach ($categoriesRaw as $f=>$cat) {
			$categories[$f] = $cat;
		}
		update_categories($categories);
	} else {
		$categories = update_categories();
	}
	return $categories;
}


function update_categories($cats=false) {
	global $config;

	if (!isset($config->data_sources) || !$config->data_sources) {
		$error = true;
	} else {
		$error = false;
		foreach ($config->data_sources as $f=>$v) {
			if (!isset($config->data[$f]->data) || !$config->data[$f]->data) {
				$error = true;
				break;
			}
		}
	}
	if ($error) {
		save_log('errors.log',date('j/m/Y, H\hi\ms\s'));
		save_log('errors.log','Error trying to retrieve categories: no data found!');
		return $cats;
	}

	$categories = array();
	foreach ($config->data_sources as $f=>$v) {
		$data = $config->data[$f]->data;
		$categories[$f] = array();
		foreach ($data as $i=>$item) {
			$t = new StdClass();
			$t->local_id = $item->local_id;
			$t->category = null;
			if (is_array($cats) && is_array($cats[$f])) {
				foreach ($cats[$f] as $cat) {
					if ($cat->local_id==$item->local_id) {
						$t->category = $cat->category;
						break;
					}
				}
			}
			if ($cats===false && !$t->category) {
				if (!isset($item->category) || !$item->category) {
					$t->category = $config->default_categories[$f];
				} else {
					$t->category = $item->category;
				}
			}

			$categories[$f][] = $t;
			$u = $item;
			$u->category = $t->category;
			$data[$i] = $u;
		}
		$config->data[$f]->data = $data;
	}
	$cat_ret = save_data("categories.json", $categories, false);
	$data_ret = save_data("data.json", $config->data, false);

	return ($cat_ret && $data_ret) ? $categories : false;
}


function get_labeled_sources($sources) {
	if (!is_array($sources) || !$sources) {
		return "";
	}
	$ret = "";
	$control = array();
	foreach ($sources as $source) {
		if (!in_array($source->source, $control)) {
			$ret .= "<label class=\"label label-warning\">".$source->source."</label>&nbsp;";
			$control[] = $source->source;
		}
	}
	return $ret;
}

function get_sources_for_csv($sources) {
	if (!is_array($sources) || !$sources) {
		return "";
	}
	$ret = array();
	$control = array();
	foreach ($sources as $source) {
		if (!in_array($source->source, $ret)) {
			$ret[] = $source->source;
		}
	}
	return $ret;
}

function show_grid($grid, $make_card=false, $buttons=array()) {
	global $config;
	if (!is_array($grid)) {
		return false;
	}
	foreach ($grid as $i=>$row) {
		?>
		<div class="row">
		<?php
		foreach ($row as $card) {
			echo ($make_card) ? make_html_card($card, $buttons) : $card;
		}
		?>
		</div>
		<?php if (isset($grid[$i+1])) { ?>
			<hr>
		<?php
		}
	}
}

/**
 * Get excerpt from string
 *
 * @param String $str String to get an excerpt from
 * @param Integer $maxLength Maximum length the excerpt may be
 * @return String excerpt
 */
function get_excerpt($str, $maxLength=100) {
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, 0, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt = $str;
	}

	return $excerpt;
}

function number_format_i18($number,$decimals=0) {
  $locale = localeconv();
  return number_format($number,$decimals,
             $locale['decimal_point'],
             $locale['thousands_sep']);
}

function make_date($d) {
	return date("d/m/Y",mktime(0,0,0,substr($d,5,2),substr($d,8,2),substr($d,0,4)));
}
function get_data_from_duplicates($d) {
	global $config;
	$dsource = $d->source[0]->source;
	$dlocal_id = $d->local_id;
	$dsources = array();
	foreach ($d->source as $s) {
		$dsources[$s->source] = array($s->local_id => true);
	}
	foreach ($config->data as $f=>$items) {
		foreach ($items->data as $i=>$item) {
			if ($f!=$dsource && $item->local_id!=$dlocal_id && isset($dsources[$f][$item->local_id])) {
				if (!$d->avatar && $item->avatar) {
					$d->avatar = $item->avatar;
				}
				if (!$d->description && $item->description) {
					$d->description = $item->description;
				}
				if (!$d->address && $item->address) {
					$d->address = $item->address;
				}
				if (!$d->lat && $item->lat) {
					$d->lat = $item->lat;
				}
				if (!$d->lng && $item->lng) {
					$d->lng = $item->lng;
				}
				if (!$d->territory_id && $item->territory_id) {
					$d->territory_id = $item->territory_id;
				}
			}
		}
	}
	return $d;
}

function update_data($refs, $hidden, $mergedData, $show_message=true) {
	global $config;

	$do_it = false;
	if ($refs==null && $hidden==null && $mergedData==null) {
		// Build refs and hidden from $config:
		$refs = $hidden = array();
		$update_duplicates = false;
		foreach ($config->data_sources as $f=>$v) {
			if ($config->data[$f] && ($config->duplicates[$f]->refs || $config->duplicates[$f]->hidden)) {
				$refs[$f] = $config->duplicates[$f]->refs;
				$hidden[$f] = $config->duplicates[$f]->hidden;
				$do_it = true;
			}
		}
	} else {
		$update_duplicates = true;
		foreach ($config->data_sources as $f=>$ds) {
			if ($config->data[$f] && (count($refs[$f]) || count($hidden[$f]))) {
				$do_it = true;
				break;
			}
		}
	}
	if (!$do_it) {
		return true;
	}
	if ($update_duplicates) {
		if ($config->duplicates) {
			// First I make a crazy nested loop to add the present refs to the config duplicates variable:
			foreach ($config->data_sources as $f=>$ds) {
				// refs:
				if (isset($refs[$f]) && is_array($refs[$f])) {
					foreach ($refs[$f] as $local_id=>$sources) {
							if (isset($config->duplicates[$f]->refs[$local_id])) {
								foreach ($sources as $source) {
									$exists = false;
									foreach ($config->duplicates[$f]->refs[$local_id] as $csource) {
										if ($source==$csource) {
											$exists = true;
											break;
										}
									}
									if (!$exists) {
										$config->duplicates[$f]->refs[$local_id][] = $source;
									}
								}
							} else {
								$config->duplicates[$f]->refs[$local_id] = $sources;
							}
					}
				}
				// hidden:
				if (isset($hidden[$f]) && is_array($hidden[$f])) {
					foreach ($hidden[$f] as $local_id=>$bool) {
						if (!in_array($local_id, array_keys($config->duplicates[$f]->hidden))) {
							$config->duplicates[$f]->hidden[$local_id] = true;
						}
					}
				}
				// mergedData:
				if (isset($mergedData[$f]) && is_array($mergedData[$f])) {
					foreach ($mergedData[$f] as $local_id=>$newMergedDataItem) {
						$config->duplicates[$f]->mergedData[$local_id] = $newMergedDataItem;
					}
				}
			}
		} else {
			$config->duplicates = array();
			foreach ($config->data_sources as $f=>$ds) {
				if (count($refs[$f]) || count($hidden[$f]) || count($mergedData)) {
					$t = new StdClass();
					$t->refs = $refs[$f];
					$t->hidden = $hidden[$f];
					$t->mergedData = $mergedData[$f];
					$config->duplicates[$f] = $t;
				}
			}
		}
	} // end of if $update_duplicates


	// Apply the refs, mergedData and hidden to the data:
	// refs and mergedData:
	foreach ($config->data_sources as $f=>$v) {
		$data = $config->data[$f]->data;
		foreach ($data as $i=>$item) {
			if (isset($refs[$f][$item->local_id])) {
				if (isset($mergedData[$f][$item->local_id])) {
					$data[$i] = $mergedData[$f][$item->local_id];
				} else {
					$data[$i] = get_data_from_duplicates($data[$i]);
				}
				$data[$i]->source = array_merge($item->source, $refs[$f][$item->local_id]);
			}
		}
		// hidden:
		if (isset($hidden[$f]) && is_array($hidden[$f])) {
			$local_ids = array_keys($hidden[$f]);
			foreach ($data as $i=>$item) {
				if (in_array($item->local_id,$local_ids)) {
					$data[$i]->hide = 1;
				}
			}
		}
		$config->data[$f]->data = $data;
	}

	$save1 = save_data("data.json", $config->data, $show_message);
	if ($update_duplicates) {
		$save2 = save_data("duplicates.json", $config->duplicates, $show_message);
	} else {
		$save2 = true;
	}
	if (!$show_message) {
		return ($save1 && $save2);
	}
}

function clean_duplicates($data_source) {
	global $config;
	if (!$data_source || !isset($config->duplicates) || !isset($config->duplicates[$data_source])) {
		return false;
	}
	$dups = $config->duplicates[$data_source];

	if (isset($dups->refs) && is_array($dups->refs)) {
		$refs = $dups->refs;
		foreach ($refs as $local_id=>$sources) {
			if (!check_if_exists_locally($data_source, $local_id)) {
				if (count($sources)>1) {
					$newSources = clean_sources($data_source, $sources);
					$leadingNewSource = $newSources[0];
					$trailingNewSources = array_shift($newSources);
					if (!isset($config->duplicates[$leadingNewSource->source])) {
						$config->duplicates[$leadingNewSource->source] = array();
					}
					if (!isset($config->duplicates[$leadingNewSource->source]->refs[$leadingNewSource->local_id])) {
						$config->duplicates[$leadingNewSource->source]->refs[$leadingNewSource->local_id] = trailingNewSources;
					} else {
						foreach ($trailingNewSources as $trailingNewSource) {
							$config->duplicates[$leadingNewSource->source]->refs[$leadingNewSource->local_id][] = trailingNewSource;
						}
					}
				}
				unset($config->duplicates[$data_source]->refs[$local_id]);
			}
		}
	}
	if (isset($dups->hidden) && is_array($dups->hidden)) {
		$hidden = $dups->hidden;
		$newHidden = array();
		foreach ($hidden as $local_id=>$h) {
			if (check_if_exists_locally($data_source, $local_id)) {
				$newHidden[$local_id] = true;
			}
		}
		$config->duplicates[$data_source]->hidden = $newHidden;
	}

	$res = new StdClass();
	if (save_data("duplicates.json", $config->duplicates, false)) {
		$res->success = 1;
		$res->msg = _('The duplicates file had to be updated and the operation was successfull.');
	} else {
		$res->error = 1;
		$res->msg = _('The duplicates file had to be updated but I couldn\'t save it! Please check your permissions to duplicates.json file');
	}
	return $res;
}

function clean_sources($data_source, $sources) {
	if (!$data_source || !$sources || !is_array($sources) || count($sources)==0) {
		return array();
	}
	$cleanedSources = array();
	foreach ($sources as $source) {
		if ($source != $data_source || ($source == $data_source && check_if_exists_locally($data_source, $sources[$i]->local_id))) {
			$cleanedSources[] = $source;
		}
	}
	return $cleanedSources;
}

function show_category_radiobox($f="", $name="radio", $selected="", $class="") {
	global $config;
	$class = ($class) ? "class='$class'" : "";
	ob_start();
	foreach ($config->categories as $value=>$label) {
		$sel = ($value == $selected) ? " checked='checked'" : "";
		?>
			<div class="radio"><label>
			<input type="radio" <?= $class ?> name="<?= $name ?>" value="<?= $value ?>" <?= $sel ?>> <span class="responsa-category-icon fa fa-<?= $label[1] ?>"></span> <?= _($label[0]) ?>
			</label></div>
		<?php
	}
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function reset_duplicates() {
	global $config;
	if (file_exists("data/duplicates.json")) {
		unlink("data/duplicates.json");
	}
	if (file_exists("data/data.json")) {
		unlink("data/data.json");
	}
	unset($config->duplicates, $config->data);
	$config->data = get_all_initiatives("raw");
}

function download_avatar($url,$name="") {
	global $config;
	$basename = basename($url);
	$ext = pathinfo($basename);
	$ext = $ext['extension'];
	$name = ($name)
		? $name.$ext
		: basename($url);
	$ch = curl_init($url);
	$fp = fopen("avatars/$name", 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_exec($ch);
		curl_close($ch);
	fclose($fp);
	return "avatars/$name";
}

function unique_id($source, $local_id) {
	$divider = "__";
	return $source.$divider.$local_id;
}

function check_if_exists_locally($source,$local_id) {
	global $config;
	$arq = "data/rawData".$config->data_sources[$source].".json";
	if (file_exists($arq)) {
		$dataRaw = json_decode(file_get_contents($arq));
		$r = false;
		foreach ($dataRaw->data as $item) {
			if ($local_id == $item->local_id) {
				$r = $item;
				break;
			}
		}
	} else {
		$r = new StdClass();
		$r->error = 1;
		$r->msg = sprintf(_("I couldn't check source %s because the file 'data/rawData$v.json' doesn't exist!"),$source);
	}
	return $r;
}

function check_if_responsaobject_exists_in_db($source,$local_id) {
	global $con;
	if (!$source || !$local_id) {
		return false;
	}
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = _("Error").": ".$con->error;
		return $res;
	}
	$sql = "SELECT * FROM ResponsaObject WHERE localId='$local_id' AND source='$source'";
	if (!$tmp = $con->query($sql)) {
		$res = new StdClass();
		$res->error = 1;
		$res->msg = sprintf("Error in query '$sql': %s\n", $con->error);
	} else {
		$res = $tmp->fetch_assoc();
	}
	return $res;
}


//*
//*
//* Faz a conexão no banco de dados
//*
//*
function connect() {
	include "config_mysql.php";
	@$con = new mysqli($config->db['server'], $config->db['user'], $config->db['password'], $config->db['db']);
	if (mysqli_connect_errno()) {
    $con = new StdClass();
    $con->error = sprintf("Connect failed: %s\n", mysqli_connect_error());
	} else {
		$con->set_charset("utf8");
	}
	return $con;
}

function show_alert($r) {
	if (!$r || !isset($r->msg)) {
		return false;
	}
	$class = (isset($r->success)) ? 'success' : 'danger';
	?>
	<div class="alert alert-<?= $class ?>">
		<?= $r->msg ?>
	</div>
	<?php
	return true;
}

function show_login($actionUrl, $btn=null, $hook_html="") {
	?>
	<form action="<?= $actionUrl ?>" method="post">
    <div class="form-login">
    	<?= $hook_html ?>
    	<?php if (!check_login()) { ?>
		    <input type="text" name="username" class="form-control" placeholder="<?= _('username') ?>" />
		    </br>
		    <input type="password" name="password" class="form-control" placeholder="<?= _('password') ?>" />
			  </br>
	    <?php } ?>
	    <div class="wrapper">
	    	<span class="group-btn">
	    		<?= ($btn) ? $btn : '<a href="#" class="btn btn-primary btn-md">'._('login').'</a>' ?>
	    	</span>
	    </div>
    </div>
	</form>
	<?php
}

function show_failed_login_alert() {
	?>
	<div class="alert alert-danger">
		<?= _('Login failed!') ?>
	</div>
	<?php
}

function check_login() {
	global $config;
	include('config_admin.php');
	if (
	    isset($_SESSION['loggedInResponsaDatamining']) &&
	    $_SESSION['loggedInResponsaDatamining']==$config->salt &&
	    isset($_SESSION['user']) &&
	    isset($_SESSION['userLevel']) &&
	    isset($config->passwd[$_SESSION['user']]) &&
	    $_SESSION['userLevel']==$config->passwd[$_SESSION['user']][0]
    ) {
		return array($_SESSION['user'], $_SESSION['userLevel']);
	}
	if (!isset($_POST['username']) || !isset($_POST['password'])) {
		unset($_SESSION['loggedInResponsaDatamining']);
		return false;
	}
	$hashedPasswd = hash('sha256', $_POST['username'].$_POST['password']);
    if (isset($config->passwd[$_POST['username']]) && $config->passwd[$_POST['username']][1]==$hashedPasswd) {
	    $_SESSION["loggedInResponsaDatamining"]=$config->salt;
	    $_SESSION["userLevel"]=$config->passwd[$_POST['username']][0];
	    $_SESSION['user']=$_POST['username'];
	    return array($_SESSION['user'], $_SESSION['userLevel']);
    }

	unset($_SESSION['loggedInResponsaDatamining'], $_SESSION['userLevel']);
	return false;
}

function remove_out_of_boundaries($data) {
	if (!isset($data) || !is_array($data) || count($data)==0) {
		return array();
	}
	$res = array();
	foreach ($data as $d) {
		if (validate_latlng($d->lat, $d->lng)) {
			$res[] = $d;
		}
	}
	return $res;
}

function validate_latlng($lat,$lng) {
	$NW = array(5.272222,-73.992222);
	$SE = array(-33.742222,-34.791667);
	return (isset($lat) && isset($lng)) && ($lat<=$NW[0] && $lng>=$NW[1]) && ($lat>=$SE[0] && $lng<=$SE[1]);
}

function key_for_encryption() {
	global $config;
	if (!$config->salt) {
		return;
	}
	//return pack('H*', password_hash($config->salt, PASSWORD_DEFAULT)."\n");
	return pack('H*', $config->salt);
}

function encrypt($s) {
  # the key should be random binary, use scrypt, bcrypt or PBKDF2 to
  # convert a string into a key
  # key is specified using hexadecimal
  //$key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");

  $key = key_for_encryption();

  # show key size use either 16, 24 or 32 byte keys for AES-128, 192
  # and 256 respectively
  $key_size =  strlen($key);
  echo "Key size: " . $key_size . "<br>";

  # create a random IV to use with CBC encoding
  $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
  $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

  # creates a cipher text compatible with AES (Rijndael block size = 128)
  # to keep the text confidential
  # only suitable for encoded input that never ends with value 00h
  # (because of default zero padding)
  $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $s, MCRYPT_MODE_CBC, $iv);

  # prepend the IV for it to be available for decryption
  $ciphertext = $iv . $ciphertext;

  # encode the resulting cipher text so it can be represented by a string
  $ciphertext_base64 = base64_encode($ciphertext);

	return $ciphertext_base64;
}

function decrypt($enc) {
	$key = key_for_encryption();
	//$key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
	$ciphertext_dec = base64_decode($enc);

	# generate iv_size
  $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

  # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
  $iv_dec = substr($ciphertext_dec, 0, $iv_size);

  # retrieves the cipher text (everything except the $iv_size in the front)
  $ciphertext_dec = substr($ciphertext_dec, $iv_size);

  # may remove 00h valued characters from end of plain text
  $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

echo "<br>".$plaintext_dec."<br>";

  return $plaintext_dec;
}

function show_time_span($dateTime) {
	$time = strtotime($dateTime.' UTC');
	$timeEllapsed = time() - $time;
	$timeEllapsed = ($timeEllapsed<1) ? 1 : $timeEllapsed;
	$tokens = array (
		  31536000 => array(_('year'),_('years')),
		  2592000 => array(_('month'),_('months')),
		  604800 => array(_('week'),_('weeks')),
		  86400 => array(_('day'),_('days')),
		  3600 => array(_('hour'),_('hours')),
		  60 => array(_('minute'),_('minutes')),
		  1 => array(_('second'),_('seconds'))
	);
	foreach ($tokens as $unit => $text) {
		if ($timeEllapsed < $unit) continue;
		$numberOfUnits = floor($timeEllapsed / $unit);
		return sprintf(_('%s ago'), $numberOfUnits.' '.(($numberOfUnits>1)?$text[1]:$text[0]));
	}
}

function is_parse_date($value) {
	// PARSE: 2018-01-05T15:11:31.673Z
	// MYSQL TIMESTAMP: 2018-01-05 15:11:31.67
	if (strlen($value) != 24) {
		return false;
	}
	return (
		$value[4]=='-' &&
		$value[7]=='-' &&
		$value[10]=='T' &&
		$value[13]==':' &&
		$value[16]==':' &&
		$value[19]=='.' &&
		$value[23]=='Z'
	);
}
function from_parse_date_to_mysql_timestamp($value) {
	if (!is_parse_date($value)) {
		return $value;
	}

	return substr($value,0,10)." ".substr($value,11,11);
}

?>
