<?php
include 'functions.php';
include 'config.php';
if (isset($_REQUEST['action']) && $config->isCron) {
	require('init.php');
	exit;
}
$config->login = check_login();
$config->isAdmin = ($config->login && $config->login[1]=='admin');
if (!$config->isAdmin && isset($config->action) ) {
    if(
        ($config->login[1]=='user' && $config->valid_actions[$config->action]!='user')
        || ($config->valid_actions[$config->action]=='admin')
    ) {
        unset($config->action);
    }
}
if (isset($config->action) && $config->action=='send_remote' && isset($_GET['do_it'])) {
	send_remote();
	exit;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title><?=_("Responsa external initiatives data management")?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Usei o tema 'Charisma'.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-slate.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
    
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>

		<?php 
    	switch($config->action) {
    		case "show_initiatives":
    		case "show_users":
    	?>
		  	<link rel="stylesheet" href="js/leaflet/leaflet.css" />
		  	<script src="js/leaflet/leaflet.js"></script>
		  	<link rel="stylesheet" href="js/leaflet-markercluster/MarkerCluster.css" />
				<link rel="stylesheet" href="js/leaflet-markercluster/MarkerCluster.Default.css" />
				<script src="js/leaflet-markercluster/leaflet.markercluster-src.js"></script>
			
				<link rel="stylesheet" href="js/leaflet-search/dist/leaflet-search.min.css" />
				<script src="js/leaflet-search/dist/leaflet-search.min.js"></script>
				<script src="js/leaflet-search/src/leaflet-search.js"></script>
			
				<script src="js/leaflet-locatecontrol/dist/L.Control.Locate.min.js"></script>
				
				<link rel="stylesheet" href="js/leaflet-awesome/leaflet.awesome-markers.css">
				<script src="js/leaflet-awesome/leaflet.awesome-markers.js"></script>
			<?php 
				break;
				case "find_duplicates":
			?>
				<!--<link rel="stylesheet" href="css/typeahead.css" />
				<script src="js/typeahead/dist/typeahead.bundle.min.js"></script>-->
				<script src="js/slautocomplete.js"></script>
			<?php
				break;
    	}
    	?>
    	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="js/leaflet-locatecontrol/dist/L.Control.Locate.mapbox.min.css" />

    <script src="js/responsa.js"></script>
   	<link href="css/responsa.css" rel="stylesheet">

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
<?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"> <img alt="Logo da EITA" src="img/Eita_roda_40x40.png" class="hidden-xs"/>
                <span>Responsa</span></a>

            <!-- user dropdown starts -->
            <!--<div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="login.php">Logout</a></li>
                </ul>
            </div>-->
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> <?=_("Choose the skin")?> </span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                		<li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->

            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="http://mapa.consumoresponsavel.org.br"><i class="glyphicon glyphicon-globe"></i> <?=_("Map of responsible consumption")?></a></li>
                <!--<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
                <li>
                    <form class="navbar-search pull-left">
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                </li>-->
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<?php } ?>
<div class="ch-container">
    <div class="row">
        <?php if (!isset($no_visible_elements) || !$no_visible_elements) { ?>
					<?php if ($config->login) { ?>
				    <!-- left menu starts -->
				    <div class="col-sm-2 col-lg-2">
				        <div class="sidebar-nav">
				            <div class="nav-canvas">
				                <div class="nav-sm nav nav-stacked">

				                </div>
				                <ul class="nav nav-pills nav-stacked main-menu">
				                    <li class="nav-header"><?=_("Menu")?></li>
				                    <?php
				                    foreach ($config->main_menu as $menu) {
				                        if (!$config->isAdmin && $menu[4]=='admin') {
				                            continue;
				                        }
				                    	switch($menu[3]) {
				                    		case "accordion":
				                    		?>
				                    			<li class="accordion">
				                        		<a href="#">
				                        			<i class="glyphicon glyphicon-<?= $menu[2] ?>"></i>
				                        			<span> <?= $menu[0] ?></span>
				                        		</a>
				                        		<ul class="nav nav-pills nav-stacked">
				                        			<?php foreach ($menu[5] as $submenu) { ?>
				                        			<li>
				                        				<a class="ajax-link" href="<?= $submenu[1] ?>"> <?= $submenu[0] ?></a>
				                        			</li>
				                        			<?php } ?>
				                        		</ul>
				                    		<?php
				                    		?>
				                    			</li>
				                    		<?php
				                    		break;
				                    		default:
				                    			?>
				                    			<li>
				                    				<a class="ajax-link" href="<?= $menu[1] ?>">
				                    					<i class="glyphicon glyphicon-<?= $menu[2] ?>"></i>
				                    					<span> <?= $menu[0] ?></span>
				                    				</a>
				                    			</li>
				                    			<?php
				                    		break;
				                    	}
				                    }
				                    ?>
				                </ul>
				                <label style="display:none" id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox" checked> Ajax on menu</label>
				            </div>
				        </div>
				    </div>
				    <!--/span-->
				    <!-- left menu ends -->
				  <?php } else { ?>
				  	<div class="col-md-2">
				  	</div>
				  <?php } ?>

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Atenção!</h4>

                <p>Este site só funciona com <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <?php } ?>
