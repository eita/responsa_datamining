<?php
if(!isset($_GET['q']) or empty($_GET['q']))
	die( json_encode(array('ok'=>0, 'errmsg'=>_("specify q parameter")) ) );

$struct = (isset($_GET['struct']) && $_GET['struct']=='autocomplete')
	? 'autocomplete'
	: 'api';

$territory_id = (isset($_GET['territory_id']) && $_GET['territory_id'])
	? $_GET['territory_id']
	: '';
	
$exclude_source = (isset($_GET['exclude_source']) && $_GET['exclude_source'])
	? $_GET['exclude_source']
	: '';
	
$exclude_local_id = (isset($_GET['exclude_local_id']) && $_GET['exclude_local_id'])
	? $_GET['exclude_local_id']
	: '';

include("functions.php");
include("config.php");
$res = get_all_initiatives("api", $territory_id, false);

function searchInit($item)	//search initial text in titles
{
	$reg = "/".sanitize($_GET['q'])."/i";	//initial case insensitive searching
	$by_title = (bool)@preg_match($reg, sanitize($item->title));
	$by_territory_label = (bool)@preg_match($reg, sanitize($item->territory_label));
	return $by_title || $by_territory_label;
}
$fdata = array_filter($res, 'searchInit');	//filter data
$fdata = array_values($fdata);	//reset $fdata indexs

$res = array();
foreach ($fdata as $f) {
	$t = new StdClass();
	if ($struct=='api') {
		$t->loc = array($f->lat,$f->lng);
		$t->title = $f->title." (".$f->territory_label.")";
	} else {
		if (!$exclude_source || !$exclude_local_id || $exclude_source!=$f->source[0]->source || $exclude_local_id!=$f->source[0]->local_id) {
			// The data:
			$t->source = $f->source[0]->source;
			$t->local_id = $f->source[0]->local_id;
			// The dropdown text:
			$t->value = $f->title;
			if (!$territory_id) {
				$t->value .= " (".$f->territory_label.")";
			}
		}
	}
	$res[] = $t;
}

$JSON = json_encode($res,true);

@header("Content-type: application/json; charset=utf-8");

if(isset($_GET['callback']) and !empty($_GET['callback']))	//support for JSONP request
	echo $_GET['callback']."($JSON)";
else
	echo $JSON;	//AJAX request
?>
