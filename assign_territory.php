<?php


function assign_territory() {
	global $config;
	?>
	<h1><?= _('Territory assignment') ?></h1>
	<?php
	if (isset($_REQUEST['do_it'])) {
		if ($config->isAdmin || $config->valid_actions['assign_territory']!='admin') {
			do_assign_territory();
			exit;
		} else {
			show_failed_login_alert();
		}
	}
	?>
	<p><?= sprintf(_("There are %s items in your database, from %s different sources. Here you can assign territories to the points. They have already the latitude and longitude, and in many cases they have free text of their city and state. But this script will try to assign a definite territory_id, which will be useful for filters by region, state and municipality in Responsa and Encontre."),number_format_i18($config->number_initiatives), count($config->data_sources)) ?></p>
	<ul>
	<?php
	foreach ($config->data_sources as $f=>$v) {
		$data = $config->data[$f];
		if (!isset($data->territory_assignment)) {
			$territory_assignment_text = _(" and <label class='label label-default'>hadn't ever being assigned to a territory_id</label>. So you must do this in this database");
		} else {
			$territory_assignment_text = sprintf(_(" and <label class='label label-default'>%s items had their territory assigned</label> at %s and <label class='label label-default'>%s items failed</label>."),$data->territory_assignment->qtty_items_assigned,$data->territory_assignment->date,$data->territory_assignment->qtty_items_failed);
		}
		?>
		<li><?= sprintf(_("The points from <i>%s</i> were last fetched at %s"),$v,$data->fetched_at." ".$territory_assignment_text) ?></li>
		<?php
	}
	?>
	</ul>
	<?php
	$actionUrl = '?action=assign_territory&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Assign territories now!").'</a>';
	show_login($actionUrl, $btn);
}

function do_assign_territory($singleSource="", $silent=false) {
	global $config;
	if (file_exists("data/Brasil/territories_for_sql_with_subdistritos.csv") && $config->data && $config->data_sources) {
		$loc = get_territories();
		$loc->states_territories = $loc->states_flip = $loc->territories_flip = array();
		foreach ($loc->territories as $id=>$territory) {
			$l = strlen($id);
			if($l==2) {
				$loc->states_flip[$territory->alias] = $id;
			} elseif ($l==11) {
				$state_id = substr($id,0,2);
				if (!isset($loc->states_territories[$state_id])) {
					$loc->states_territories[$state_id] = array();
				}
				$loc->states_territories[$state_id][$territory->slug] = $id;
				$loc->territories_flip[$territory->slug] = $id;
			}
		}
		foreach ($config->data_sources as $f=>$v) {
			if ($singleSource && $f!=$singleSource) {
				continue;
			}
			if (!$silent) {
				?>
				<h2><?= _("Processing") ?> <?= $v ?>...</h2>
				<?php
			}
			$similarities = $territories_id = array();
			foreach ($config->data[$f]->data as $i=>$d) {
				$state_id = $city_id = $subcity_id = 0;
				$similarities[$i] = array();
				// Just in case the source has the same territory ids:
				if ($d->territory_id && isset($loc->territories[$d->territory_id])) {
					$similarities[$i][$city_slug]=100;
					$territories_id[$i] = $d->territory_id;
				}
				
				if (!isset($territories_id[$i]) && $d->city) {
					$city_slug = sanitize($d->city);
					if ($d->state) {
						$state = sanitize($d->state);
						$state_id = (strlen($state)==2) ? $loc->states_flip[strtoupper($state)] : $loc->states_flip[convert_uf_state($state)];
					}
					if ($state_id) {
						if ($loc->states_territories[$state_id][$city_slug]) {
							$similarities[$i][$city_slug]=100;
							$territories_id[$i] = $loc->states_territories[$state_id][$city_slug];
						} else {
							//pR($state_id);pR($city_slug);pR($d);pR($loc->states_territories[$state_id]);exit;
							foreach ($loc->states_territories[$state_id] as $ref_territory_slug=>$ref_territory_id) {
								similar_text($city_slug,$ref_territory_slug,$similarities[$i][$ref_territory_slug]);
								if ($similarities[$i][$ref_territory_slug]==100) {
									$territories_id[$i] = $ref_territory_id;
								}
							}
						}
					} else {
						foreach($loc->territories as $ref_territory_id=>$ref_territory) {
							if ($ref_territory->type=='subcity_region') {
								similar_text($city_slug,$ref_territory->slug,$similarities[$i][$ref_territory->slug]);
								if ($similarities[$i][$ref_territory->slug]==100) {
									$territories_id[$i] = $ref_territory_id;
								}
							}
						}
					}
				}
				if (count($similarities[$i])>1) {
					arsort($similarities[$i]);
					$k=$first=0;
					foreach ($similarities[$i] as $candidate_slug=>$percentage) {
						if (!$first) {
							$first = $percentage;
						} elseif ($percentage<$first) {
							break;
						}
						$k++;
					}
					$similarities[$i] = array_slice($similarities[$i],0,$k);
				}
			}
			$table = array();
			$total_updated = 0;
			foreach ($similarities as $i=>$candidates) {
				$d = $config->data[$f]->data[$i];
				$t = new StdClass();
				$num_candidates = count($candidates);
				if ($num_candidates!=1) {
					if ($num_candidates==0) {
						$t->territory_id = "";
						$t->candidates = "";
					} else {
						$t->territory_id = "";
						$t->candidates = "<ul>\n";
						foreach ($candidates as $candidate_slug=>$percentage) {
							$t->candidates .= "<li>".$candidate_slug." / ".$loc->territories[substr($loc->territories_flip[$candidate_slug],0,2)]->alias."</li>";
						}
						$t->candidates .= "</ul>";
					}
					$t->city = ($d->city) ? $d->city : "(?)";
					if ($d->state) {
						$t->city .= " / ".$d->state;
					}
					$t->title = ($d->url) ? "<a href='".$d->url."'>".$d->title."</a>" : $d->title;
					$table[] = $t;
				} else {
					// Let's assign the territory!
					$d = $config->data[$f]->data[$i];
					if (array_values($candidates)[0] == 100) {
						$territory_id = $territories_id[$i];
					} else {
						$territory_slug = array_keys($candidates);
						$territory_slug = $territory_slug[0];
						$territory_id = $loc->territories_flip[$territory_slug];
					}
					if ($territory_id) {
						$config->data[$f]->data[$i]->territory_id = $territory_id;
						$config->data[$f]->data[$i]->territory_region_id = $loc->territories[$territory_id]->region_id;
						$config->data[$f]->data[$i]->territory_label = $loc->territories[$territory_id]->label;
						$total_updated++;
					} else {
						$t = new StdClass();
						$t->territory_id = "";
						$t->candidates = "";
						$t->city = ($d->city) ? $d->city : "(?)";
						if ($d->state) {
							$t->city .= " / ".$d->state;
						}
						$t->title = ($d->url) ? "<a href='".$d->url."'>".$d->title."</a>" : $d->title;
						$table[] = $t;
					}
				}
			}
			if ($total_updated) {
				if (file_exists("data/data.json")) {
					unlink("data/data.json");
				}
				$r = new StdClass();
				$r->success=1;
				$r->msg = sprintf(_("I've successfully assigned the territory of <span class='label label-warning'>%s items</span>!"), $total_updated);
				if (!$silent) {
					show_alert($r);
				}
				$config->data[$f]->territory_assignment = new StdClass();
				$config->data[$f]->territory_assignment->date = date('Y-m-d');
				$config->data[$f]->territory_assignment->qtty_items_assigned = $total_updated;
				$config->data[$f]->territory_assignment->qtty_items_failed = count($table);
				save_data("data$v.json", $config->data[$f], !$silent);
			}
			if ($table && !$silent) {
				$r = new StdClass();
				$r->error=1;
				$r->msg = sprintf(_("I've found the following <span class='label label-warning'>%s items</span> with problems:"), count($table));
				show_alert($r);
				show_table($table, "table-$f");
			}
		}
	
	} else {
		$r = new StdClass();
		$r->error=1;
		$r->msg = "<p>"._("It's not possible to assign items territories if there are no items or if the territories table wasn't created yet. Click below if you want to create the brazilian territory table.")."</p><p><a class='btn btn-default' href='?action=create_territory_file'>"._("Create table")." <i>territories.csv</i></a></p>";
		show_alert($r);
	}
}

?>
