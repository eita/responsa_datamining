<?php
if (!isset($_GET['unique_id']) && !$_GET['unique_id']) {
	return false;
}
list($source, $local_id) = explode('__',$_GET['unique_id']);
include("functions.php");
include("config.php");
include("init_external_data.php");
//$item = get_single_initiative($source,$local_id);
$params = new StdClass();
$params->sources = array($source);
$params->localId = $local_id;
$item = get_initiatives_from_DB($params, 'singleRaw');
$ret = make_html_modal($item);
if (isset($_GET['callback']) && $_GET['callback']) {
	echo $_GET['callback']."(".json_encode($ret).");";
} else {
	@header("Content-type: application/json; charset=utf-8");
	echo json_encode($ret);
}

?>
