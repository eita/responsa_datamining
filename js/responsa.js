function searchByAjax(text, callResponse)//callback for 3rd party ajax requests
	{
		return $.ajax({
			url: 'ajax_search_initiatives.php',	//read comments in search.php for more information usage
			type: 'GET',
			data: {q: text},
			dataType: 'json',
			success: function(json) {
				callResponse(json);
			}
		});
	}

function openDuplicatesModal(ref, dup) {
	$("#duplicates-body").html("");
	$("#duplicates-modal").modal("show");
	$.ajax({
		url: 'ajax_build_duplicates_merger_modal.php',
		type: 'GET',
		data: {ref_source: ref.source, ref_local_id: ref.local_id, dup_source: dup.source, dup_local_id: dup.local_id},
		dataType: 'json',
		success: function(data) {
			$("#duplicates-body").html(data.html);
			$("#merge_duplicates_form input").click(function(e) {
				var isRef = $(this).attr('value')=='ref';
				var field = $(this).attr('name');
				$("#ref_"+field).toggleClass('highlighted', isRef);
				$("#dup_"+field).toggleClass('highlighted', !isRef);
			});
			$("#duplicates-modal-submit").click(function(e) {
				var mergeOptions = {};
  			$.each($("#merge_duplicates_form input:checked"), function (key, el) {
  				mergeOptions[$(this).attr('name')] = $(this).attr('value');
  			});
  			$.ajax({
					url: 'ajax_merge_duplicates.php',
					type: 'POST',
					data: {ref: ref, dup: dup, mergeOptions: mergeOptions},
					dataType: 'json',
					success: function(data) {
						$("#duplicates-modal").modal("hide");
						if (!data.error) {
							$("#card__"+ref.source+"__"+ref.local_id).hide(500);
							$("#card__"+dup.source+"__"+dup.local_id).hide(500);
						}
						noty({"text":data.msg,"layout":"top","type":"information","timeout":1000});
					}
				});
  			e.preventDefault();
			});
		}
	});
}

var dashboardData = {};
function updateDashboard(firstTime) {
	$.ajax({
		url: 'ajax_get_dashboard_data.php',
		type: 'GET',
		dataType: 'json',
		data: {'firstTime':firstTime},
		success: function(data) {
			if (firstTime) {
				dashboardData = data;
				// INITIATIVES
				smoothToggleText("dashboardDataInitiativesCount", data.initiatives.count);
				smoothToggleText("dashboardDataInitiativesCitiesCount", data.initiatives.citiesCount);
				smoothToggleText("dashboardDataInitiativesStatus", data.initiatives.status);
				smoothToggleText("dashboardDataInitiativesSourcesCount", data.initiatives.sourcesCount);
			}
			// USERS:
			if (data.users.count != dashboardData.users.count || firstTime) {
				smoothToggleText("dashboardDataUsersCount", data.users.count);
				smoothToggleText("dashboardDataUsersCitiesCount", data.users.citiesCount);
				smoothToggleText("dashboardDataUsersStatus", data.users.status);
			}
			if (data.users.time != dashboardData.users.time || firstTime) {
				smoothToggleText("dashboardDataUsersTime", data.users.time);
			}
			// CHALLENGES:
			if (data.challenges.count != dashboardData.challenges.count || firstTime) {
				smoothToggleText("dashboardDataChallengesCount", data.challenges.count);
			}
			// CHALLENGE ANSWERS:
			if (data.challengeAnswers.count != dashboardData.challengeAnswers.count || firstTime) {
				smoothToggleText("dashboardDataChallengeAnswersCount", data.challengeAnswers.count);
				smoothToggleText("dashboardDataChallengeAnswersStatus", data.challengeAnswers.status);
			}
			if (data.challengeAnswers.time != dashboardData.challengeAnswers.time || firstTime) {
				smoothToggleText("dashboardDataChallengeAnswersTime", data.challengeAnswers.time);
			}
			// COMMENTS:
			if (data.comments.count != dashboardData.comments.count || firstTime) {
				smoothToggleText("dashboardDataCommentsCount", data.comments.count);
				smoothToggleText("dashboardDataCommentsStatus", data.comments.status);
			}
			if (data.comments.time != dashboardData.comments.time || firstTime) {
				smoothToggleText("dashboardDataCommentsTime", data.comments.time);
			}
			// UNREVIEWED INITIATIVES:
			if (data.unreviewedInitiatives.count != dashboardData.unreviewedInitiatives.count || firstTime) {
				smoothToggleText("dashboardDataUnreviewedInitiativesCount", data.unreviewedInitiatives.count);
				smoothToggleText("dashboardDataUnreviewedInitiativesStatus", data.unreviewedInitiatives.status);
			}
			if (data.unreviewedInitiatives.time != dashboardData.unreviewedInitiatives.time || firstTime) {
				smoothToggleText("dashboardDataUnreviewedInitiativesTime", data.unreviewedInitiatives.time);
			}
			// PENDING FEEDBACKS:
			if (data.pendingFeedbacks.count != dashboardData.pendingFeedbacks.count || firstTime) {
				smoothToggleText("dashboardDataPendingFeedbacksCount", data.pendingFeedbacks.count);
				smoothToggleText("dashboardDataPendingFeedbacksStatus", data.pendingFeedbacks.status);
			}
			if (data.pendingFeedbacks.time != dashboardData.pendingFeedbacks.time || firstTime) {
				smoothToggleText("dashboardDataPendingFeedbacksTime", data.pendingFeedbacks.time);
			}
			
			
			
			if (firstTime) {
				$("#dashboardDataLoading").hide();
				$("#dashboardData").show(500);
			} else {
				dashboardData = data;
			}
		}
	});
}
function smoothToggleText(id, text) {
	$("#"+id).fadeOut(500, function() {
		$(this).text(text).fadeIn(500);
	});
}

$( document ).ready(function() {
	$(".responsa-category").change(function(e) {
		var values = $(this).attr('name').split(/-(.+)?/);
		var category = $(this).attr('value');
		$.ajax({
			url: 'ajax_update_categories.php',
			type: 'GET',
			data: {source: values[0], local_id: values[1], category: category},
			dataType: 'json',
			success: function(data) {
				noty({"text":data.msg,"layout":"top","type":"information","timeout":1000});
			}
		});
	});
	$(function() {
    $('#max_percentage_for_duplicates').change(function() {
        this.form.submit();
    });
	});
	
	$(".review_initiatives").click(function(e) {
		$(this).parent()
			.html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>")
			.attr('class', 'btn btn-warning btn-lg');
		var objectId = $(this).attr('objectId');
		var action = $(this).attr('action');
		var data = {};
		$.each($("#tr-"+objectId+" .form-control"), function (key, el) {
			if (el.value) {
				var field = el.id.replace("__"+objectId,"");
				var value = el.value;
				data[field] = value;
			}
		});
		$.ajax({
			url: 'ajax_update_initiatives.php',
			type: 'POST',
			data: {
				objectId: objectId,
				action: action,
				data: data
			},
			dataType: 'json',
			success: function(data) {
				noty({"text":data.msg,"layout":"top","type":"information","timeout":1000});
				if (typeof data.success!='undefined' && data.success) {
					$('#tr-'+data.remote_id).hide(1000);
				}
			}
		});
	});
	
	$(".review_feedbacks").click(function(e) {
		$(this).parent()
			.html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>")
			.attr('class', 'btn btn-warning btn-lg');
		var objectId = $(this).attr('objectId');
		var action = $(this).attr('action');
		var data = {};
		$.each($("#tr-"+objectId+" .form-control"), function (key, el) {
			if (el.value) {
				var field = el.id.replace("__"+objectId,"");
				var value = el.value;
				data[field] = value;
			}
		});
		$.ajax({
			url: 'ajax_update_feedbacks.php',
			type: 'POST',
			data: {
				objectId: objectId,
				action: action,
				data: data
			},
			dataType: 'json',
			success: function(data) {
				noty({"text":data.msg,"layout":"top","type":"information","timeout":1000});
				if (typeof data.success!='undefined' && data.success) {
					$('#tr-'+objectId).hide(1000);
				}
			}
		});
	});
	
	$(".modal-wide").on("show.bs.modal", function() {
		var height = $(window).height() - 200;
		$(this).find(".modal-body").css("max-height", height);
	});
	
	function loadlink(){
    $('#links').load('test.php',function () {
         $(this).unwrap();
    });
	}

	if ($("#dashboardData").length) {
		updateDashboard(true);
		setInterval(function(){
		  updateDashboard(false)
		}, 10000);
	}
	
	$(".get-lat-lng-modal").click(function(e) {
		$("#get-latlng-modal").modal("show");
	});
	
});
