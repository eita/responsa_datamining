<?php

/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);
*/
header("Access-Control-Allow-Origin: *"); 

include("functions.php");
include("config.php");
//$res = get_all_initiatives("geoJSON");
$params = new StdClass();
$params->published = 1;
$params->types = array('initiative');
if (isset($_REQUEST['territoryId']) && $_REQUEST['territoryId']) {
	$params->territoryId = $_REQUEST['territoryId'];
}
$type = "csv";
	
if (isset($_REQUEST['categories'])) {
	$params->categories = explode(",",$_REQUEST['categories']);
}


$res = get_initiatives_from_DB($params, $type);
$date = new DateTime();
$fDate = $date->format('Y-m-d');
header("Content-type: application/csv; charset=utf-8");
header("Content-Disposition: attachment; filename=\"dados_responsa_$fDate.csv\"");
$csv = fopen('php://output', 'w');
$titles = array_keys($res[0]);
fputcsv($csv, $titles);
foreach($res as $r) {
    fputcsv($csv, $r);
}
?>
