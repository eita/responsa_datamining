<?php

function edit_published_initiatives() {
	global $config;
	if (isset($_REQUEST['do_it'])) {
		if ($config->isAdmin || $config->valid_actions['edit_published_initiatives']!='admin') {
			if ((isset($_POST["objectLink"]) && $_POST["objectLink"])
					|| (isset($_REQUEST["objectId"]) && $_REQUEST["objectId"])) {
				$ret = do_edit_published_initiatives();
				if ($ret->error) {
					?>
					<h1><?= _("Edit published initiatives") ?></h1>
					<div class="alert alert-danger"><?= $ret->msg ?></div>
					<?php
				}
			}
		} else {
			?>
			<h1><?= _("Edit published initiatives") ?></h1>
			<?php
			show_failed_login_alert();
		}
	} else {
		?>
		<h1><?= _("Edit published initiatives") ?></h1>
		<?php
	}
	?>
		<p><?= _("Here you can edit an existing published initiative") ?></p>
	<?php
	$actionUrl = '?action=edit_published_initiatives&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Edit initiative").'</a>';
	$html = '
		<div class="form-group clearfix">
			<label for="objectLink" class="col-sm-1 control-label">'._('Link').'</label>
			<div class="col-sm-7">
				<input class="form-control" type="text" name="objectLink" placeholder="'._("copy from mapa.consumoresponsavel.org.br").'">
			</div>
			<div class="col-sm-1 text-center"><strong>'._("OR").'</strong></div>
			<label for="objectId" class="col-sm-1 control-label">'._('ObjectId').'</label>
			<div class="col-sm-2">
				<input class="form-control" type="text" name="objectId">
			</div>
		</div>
	';
	show_login($actionUrl, $btn, $html);
}

function do_edit_published_initiatives() {
	global $config;
	
	$objectId = ((isset($_REQUEST["objectId"]) && $_REQUEST["objectId"]))
		? $_REQUEST["objectId"]
		: "";
	$objectLink = (!$objectId && (isset($_POST["objectLink"]) && $_POST["objectLink"]))
		? $_POST["objectLink"]
		: "";
	
	$params = new StdClass();
	if ($objectId) {
		$params->objectId = $objectId;
	}	 else {
		$parts = parse_url($objectLink);
		parse_str($parts['query'], $query);
		list($params->source,$params->localId) = explode("__", $query["id"]);
		if (!$params->source || !$params->localId) {
			$t = new StdClass();
			$t->error = true;
			$t->msg = _("Not enough parameters to procede");
			return $t;
		}
	}
	
	$item = get_initiatives_from_DB($params, 'api');
	if (!$item || $item->error) {
		$t = new StdClass();
		$t->error = 1;
		$t->msg = ($item->error) 
			? $item->error 
			: _("Sorry, I couldn't find the object in Responsa's database with these parameters");
		return $t;
	} elseif (!$item[0]->published || $item[0]->published=='0') {
		include('review_initiatives.php');
		do_review_initiatives($item[0]->remote_id);
		return;
	}
	$objectId = $item[0]->remote_id;
	$t = new StdClass();
	$t->trId = "tr-".$objectId;
	$t->actions = '
		<span class="text-nowrap">
			<a href="#" title="'._("Save").'" class="btn btn-xs btn-success review_initiatives" action="approve" objectId="'.$objectId.'"><i class="glyphicon glyphicon-save"></i></a>
			<a href="#" title="'._("Reject").'" class="btn btn-xs btn-danger review_initiatives" action="reject" objectId="'.$objectId.'"><i class="glyphicon glyphicon-remove"></i></a>
		</span>
	';
	$t->initiative = make_edit_initiative_form($item[0]);
	
	?>
	<h1><?= _("Edit published initiatives") ?></h1>
	<?= show_table(array($t), "review_initiatives") ?>
	<br /><hr /><br />
	<?php
	return true;
}
?>
