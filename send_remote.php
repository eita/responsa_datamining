<?php


function send_remote() {
	global $config;
	if (file_exists("data/send_remote_status.json")) {
		$config->send_remote_status = json_decode(file_get_contents("data/send_remote_status.json"));
		$items_done = array();
		foreach ($config->send_remote_status->items_done as $id=>$items) {
			$items_done[$id] = $items;
		}
		$config->send_remote_status->items_done = $items_done;
	}
	if (file_exists("data/send_remote_cache.json")) {
		$cache = json_decode(file_get_contents("data/send_remote_cache.json"));
		$config->send_remote_cache = array();
		foreach ($cache as $f=>$data) {
			$config->send_remote_cache[$f] = $data;
		}
	}
	if (isset($_REQUEST['do_it'])) {
		if (($config->isAdmin || $config->valid_actions['send_remote']!='admin') || $config->isCron) {
			do_send_remote();
			return;
		} else {
			show_failed_login_alert();
		}
	}
	
	$actionUrl = '?action=send_remote&do_it=1';
	$btnTxt = ($config->send_remote_status)
		? _("Continue the process")
		: _("Start process now");
	$btn = '<button class="btn btn-lg btn-success" type="submit">'.$btnTxt.'</a>';
	?>
	<h1><?= _("Send to Responsa") ?></h1>
	<p><?= _("Time to send data home!") ?></p>
	<p><?= _("This is the manual interface for sending the processed and qualified data to Responsa Database. It can also be triggered by cron. If you really want to do it manually, you will have to reload the page several times") ?></p>
	<h2><?= _("Status of the send process") ?></h2>
	<?php if ($config->send_remote_status) { ?>
		<p><i><?= $config->send_remote_status->msg ?></i>. <?= sprintf(_("I've already updated %s initiatives"),count($config->send_remote_status->items_done)) ?>.</p>
	<?php } else { ?>
		<p><i><?= _("Nothing is being done") ?></i></p>
	<?php } ?>
	<?php
	show_login($actionUrl, $btn);
}

function do_send_remote() {
	global $config;
	$logFile = ($config->isCron)
	 ? "log_cron.csv"
	 : "log.csv";
	save_log($logFile,date('j/m/Y, H\hi\ms\s'));
	save_log($logFile,_("starting to send ResponsaObjects"));
	require("parse_functions.php");	
	if (!isset($config->send_remote_status)) {
		$config->send_remote_status = new StdClass();
		$config->send_remote_status->items_done = array();
	}
	if ($config->send_remote_status->items_done) {
		// Not the first run:
		save_log($logFile,_("I've already sent ".count($config->send_remote_status->items_done)." items to Responsa in previous run(s)."));
	} else {
		// First run:
		save_log($logFile,_("First run..."));
		// Let's find all items that are in mysql and are published but don't exist anymore in data.json. Those will be unpublished.
		$con = connect();
		if (isset($con->error) && $con->error) {
			save_log($logFile,$con->error);
			save_log($logFile,_("Bye."));
			return;
		}
		foreach ($config->data as $f=>$data) {
			$all_local_ids = array();
			foreach ($data->data as $item) {
				$all_local_ids[] = "'".$item->local_id."'";
			}
			// TODO: This is a risky operation if something breaks and $data->data is empty because of some error
			$sql = "SELECT objectId, localId, title FROM ResponsaObject WHERE source='$f' AND published=1 AND localId NOT IN (".implode(",",$all_local_ids).")";
			if (!$tmp = $con->query($sql)) {
				$error = sprintf("Error in query '$sql': %s\n", $con->error);
				$success = false;
				$res = new StdClass();
				$res->error = 1;
				$res->msg = (isset($error)) ? $error : 42;
				show_alert($res);
			} else {
				$items = array();
				while ($row = $tmp->fetch_assoc()) {
					$t = new StdClass();
					$t->objectId = $row['objectId'];
					$t->source = $f;
					$t->localId = $row['localId'];
					$t->title = $row['title'];
					$items[$t->objectId] = $t;
				}
				$unpublished_items = unpublish_remote($items);
			}
		}
	}
	$finished_all = true;
	$n = 0;
	$max_queries_per_round = 50;
	$cron_interval = 10*60; // 10 minutes;
	$initial_time = time();
	foreach ($config->data as $f=>$data) {
		$wrote_source_header = false;
		foreach ($data->data as $i=>$item) {
			$unique_id = unique_id($f,$item->local_id);
			// Skip this item if it was already done in last run:
			if (in_array($unique_id,array_keys($config->send_remote_status->items_done))) {
				continue;
			}
			if (!$wrote_source_header) {
				save_log($logFile,'Source: '.$config->data_sources[$f]." ($f)");
				$wrote_source_header = true;
			}
			save_log($logFile,_("Checking")." ".$item->title." (local_id=".$item->local_id.")");
			$exists = check_if_responsaobject_exists_in_db($f, $item->local_id);
			if ($exists && !isset($exists->error)) {
				save_log($logFile,_("This object exists in Responsa DB! It will be updated.")." Object ID=".$exists['objectId']);
			} elseif (isset($exists->error)) {
				show_alert($exists);
				return false;
			} else {
				$exists = false;
			}
			if (!$exists) {
				save_log($logFile,_("This object doesn't exist in Responsa. Creating it..."));
			}
			/*old script to get only those with old territoryId
			if (strlen($exists['territoryId'])==11) { continue; }*/
			
			// Send to Responsa DB!
			$item->published=true;
			$item->reviewed=false;
			$item->type='initiative';
			$res = update_remote($item, $exists, true);
			$n++;
			$div = ($res->error) ? "danger" : "success"; 
			save_log($logFile,$res->msg);
			$t = new StdClass();
			$t->local_id = $item->local_id;
			$t->remote_id = isset($res->remote_id) ? $res->remote_id : "failed";
			$t->source = $item->source[0]->source;
			$config->send_remote_status->items_done[$unique_id] = $t;
			$config->send_remote_status->msg=_("Processing");
			$config->send_remote_status->status="in_process";
			save_data("send_remote_status.json",$config->send_remote_status,false);
			
			if (($n >= $max_queries_per_round && !$config->isCron)
					|| ($cron_interval<(time()-$initial_time) && $config->isCron) ) {
				$finished_all = false;
				break;
			}
		}
		if (!$finished_all) {
			break;
		}
		//save_log($logFile,'Finished source '.$config->data_sources[$f]);
	}
	
	// Clean send_remote_status if finished
	if ($finished_all) {
		save_log($logFile,"---------------------------------------------");
		save_log($logFile,date('j/m/Y, H\hi\ms\s'));
		save_log($logFile,_("definitively finished the full run"));
		save_log($logFile,"---------------------------------------------");
		unlink("data/send_remote_status.json");
	} else {
		save_log($logFile,"---------------------------------------------");
		save_log($logFile,date('j/m/Y, H\hi\ms\s'));
		save_log($logFile,_("finished this run of sending the objects to Responsa DB"));
		save_log($logFile,"---------------------------------------------");
		header("Refresh:0");
	}
}


?>
