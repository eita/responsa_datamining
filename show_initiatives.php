<?php

function show_initiatives() {
	if (isset($_REQUEST['layout']) && $_REQUEST['layout']=='table') {
		do_show_initiatives_in_table();
	} else {
		do_show_initiatives_in_map();
	}
}
function do_show_initiatives_in_table() {
	global $config;
	?>
	<div class="row" style="margin-bottom:20px">
		<div class="col-lg-12">
			<a href="?action=show_initiatives&layout=map" class="btn btn-success"><?= _("Show results in map") ?></a>
		</div>
	</div>
	<?php
	$items = get_all_initiatives("table");
	show_table($items);
}
function do_show_initiatives_in_map() {
	global $config;
	?>
	<div class="row" style="margin-bottom:20px">
		<div class="col-lg-12">
			<a href="?action=show_initiatives&layout=table" class="btn btn-success"><?= _("Show results in table") ?></a>
		</div>
	</div>
	<div class="row">
		<div id="map" class="col-lg-12"></div>
	</div>
	<script>
		var map = L.map('map').setView([-15.7797200, -47.9297200], 4);
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    	maxZoom: 18,
    	id: 'dtygel.mk0l1gn6',
    	accessToken: 'pk.eyJ1IjoiZHR5Z2VsIiwiYSI6IjY4YjRhNjU1NWJhYTU2OGU1YWQ1OTcxYjRjNDcyNzIxIn0.Go-whnL9yIt3rHicQ5gqbA'
}).addTo(map);
		var markers = L.markerClusterGroup();
		var controlSearch = new L.Control.Search({
			callData: searchByAjax, 
			text:'buscar...', 
			markerLocation: false,
			circleLocation: true,
			zoom: 14
		});
		map.addControl( controlSearch );
		
		lc = L.control.locate({
    	follow: true,
    	strings: {
        title: "Mostre minha área!"
    	}
		}).addTo(map);

		map.on('startfollowing', function() {
    	map.on('dragstart', lc._stopFollowing, lc);
		}).on('stopfollowing', function() {
    	map.off('dragstart', lc._stopFollowing, lc);
		});
		
		var iconFonts = 
		{
		'feiras':{icon:'<?= $config->categories["feiras"][1] ?>', bkgcolor:'darkgreen', color:'#fff'},
		'grupos-de-consumo-responsavel':{icon:'<?= $config->categories["grupos-de-consumo-responsavel"][1] ?>', bkgcolor:'orange', color:'#333'},
		'iniciativas-de-economia-solidaria':{icon:'<?= $config->categories["iniciativas-de-economia-solidaria"][1] ?>', bkgcolor:'red', color:'#fff'},
		'iniciativas-de-agroecologia':{icon:'<?= $config->categories["iniciativas-de-agroecologia"][1] ?>', bkgcolor:'cadetblue', color:'#fff'},
		'restaurantes':{icon:'<?= $config->categories["restaurantes"][1] ?>', bkgcolor:'purple', color:'#333'},
		};

		jQuery.ajax({
  		type: "GET",
  		url: "ajax_get_all_initiatives.php",
  		cache: false,
  		success: function(data){
  			var Points = eval(data);
  			var iconAwesome = "";
     		for (var i = 0; i < Points.length; i++) {
					var a = Points[i];
					iconAwesome = L.AwesomeMarkers.icon({
    				icon: iconFonts[a.category].icon, 
    				prefix: 'fa', 
    				markerColor: iconFonts[a.category].bkgcolor, 
    				iconColor: iconFonts[a.category].color
  				});
					var marker = L.marker(new L.LatLng(a.lat, a.lng), {icon: iconAwesome}, { title: a.title });
					marker.bindPopup(a.popup);
					markers.addLayer(marker);
				}
				var bounds = markers.getBounds();
				//map.fitBounds(bounds);
				map.addLayer(markers);
  		}
		});
		
		function finishedLoading() {
			console.log('done');
	    loader.className = 'done';
  	  setTimeout(function() {
        loader.className = 'hide';
 		  }, 500);
		}
		
	</script>
	<?php
}


?>
