<?php

include("functions.php");
include("config.php");
include("parse_config.php");

$config->debug = (isset($_POST['debug'])) ? true : false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}




$con = connect();
$t = new StdClass();

if (
	!$config->debug && (
	!isset($_POST['action']) || !in_array($_POST['action'],array('save','delete','remove','update')) ||
	!isset($_POST['model']) || !$_POST['model'] || !isset($parseObj->{$_POST['model']}) ||
	!isset($_POST['object_id']) || !$_POST['object_id'] ||
	!isset($_POST['salt']) || $_POST['salt']!=$config->api_salt
)) {
	$t->error = "You didn't send all obligatory fields or the correct password. Bye!";
} else {
	$model = $_POST['model'];
	$fields = $parseObj->{$model};
	$dataRaw = (isset($_POST['data'])) ? json_decode($_POST['data']) : "";
	$action = $_POST['action'];
	$object_id = $_POST['object_id'];
	$success = false;
	$sql = "SELECT * FROM $model WHERE objectId = '$object_id'";
	$res = $con->query($sql);
	if (!$res) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
	} else {
		$obj = $res->fetch_object();
	}

	if ($config->debug) {
		pR($obj);pR($dataRaw);pR($action);
	}

	if ($dataRaw || $action=="delete") {
		$success = true;
		$data = array();
		foreach ($dataRaw as $field=>$value) {
			if (!in_array($field, $fields)) {
				/*save_log("log.csv",'------------------');
				save_log("log.csv",date('d/m/Y at H:i:s'));
				save_log("log.csv","I don't have the field '$field' in my mirror database '$model'. Ignoring this field!");
				save_log("log.csv",'------------------');*/
				// Ignore the unexisting field...
			} else {
        $value_to_send =  (is_object($value) || is_array($value))
    			? "'".json_encode($value)."'"
    			: "'".$con->real_escape_string(from_parse_date_to_mysql_timestamp($value))."'";
				$data["`".$field."`"] = $value_to_send;
			}
		}
		if ($config->debug) {
			pR($fields); pR($dataRaw); pR($data); pR($error);
		}

		if ($obj && $action=='save') {
			$action = 'update';
		} elseif (!$obj && $action=='update') {
			$action = 'save';
		}

		$sql = "";
		switch ($action) {
			case 'save':
				$sql = "INSERT INTO $model (".implode(",",array_keys($data)).") VALUES (".implode(",",array_values($data)).")";
			break;
			case 'update':
				$sql = "UPDATE $model SET ";
				$sql_set = array();
				foreach ($data as $field=>$value) {
					$sql_set[] = "$field = $value";
				}
				$sql.=implode(",",$sql_set);
				$sql.=" WHERE objectId='$object_id'";
			break;
			case 'remove':
			case 'delete':
				$sql = "DELETE FROM $model WHERE objectId='$object_id'";
			break;
		}
		if ($config->debug) {
			$t->sql = $sql;
		}
		if (!$con->query($sql)) {
			$error = sprintf("Error in query: %s\n", $con->error);
			$t->sql = $sql;
			$t->fields = json_encode($fields);
			$t->dataRaw = json_encode($dataRaw);
			$t->data = $_POST['data'];
			$t->action = $action;
			$t->object_id = $object_id;
			$t->model = $model;
			$success = false;
		}
		if ($config->debug) {
			pR($sql);pR($error);
		}
	}
	if ($success!==false) {
		$t->success=1;
	} else {
		$t->error = (isset($error)) ? $error : 42;
		save_log("log.csv",'------------------');
		save_log("log.csv",date('d/m/Y at H:i:s'));
		save_log("log.csv",'Error in parse_api.php: '.$error);
		save_log("log.csv",'------------------');
	}
}

@header("Content-type: application/json; charset=utf-8");
echo json_encode($t);
?>
