<?php

if (isset($_GET['save_csv'])) {
	$config->save_territories_in_csv = true;
	if (isset($_GET['add_first_childs']))
		$config->add_first_childs = true;
	if (isset($_GET['add_state']))
		$config->add_state = true;
	if (isset($_GET['add_nonaccented_field']))
		$config->add_nonaccented_field = true;
} elseif (isset($_GET['save_sql'])) {
	$config->save_territories_in_sql = true;
}

function create_territory_file() {
	global $config;
	if ($config->save_territories_in_sql) {
		create_territory_sql();
		return true;
	}
	$files = array('meso','munic');
	$territories = array();
	$regioes = array(
		1=>"Norte",
		2=>"Nordeste",
		3=>"Sudeste",
		4=>"Sul",
		5=>"Centro-Oeste"
	);
	foreach ($regioes as $id=>$r) {
		$t = new StdClass();
		$t->id = $id;
		$t->name = $r;
		if ($config->add_nonaccented_field) {
			$t->name_noaccent = remove_accents($r);
		}
		$t->parent_id = "";
		$t->type = 'region';
		$territories[$t->id] = $t;
	}
	$UFs = array();
	foreach ($files as $f) {
		$handle = fopen ("data/Brasil/$f.csv","r");
		while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$t = new StdClass();
			if (!$UFs[$line[0]]) {
				$UFs[$line[0]] = true;
				$t->id = substr($line[1],0,2);
				$t->parent_id = $line[1][0];
				$t->name = $line[0];
				if ($config->add_nonaccented_field) {
					$t->name_noaccent = remove_accents($line[0]);
				}
				$t->type = 'state';
				$territories[$t->id] = $t;
				$t = new StdClass();
			}
			$t->id = $line[1];
			$t->name = $line[2];
			if ($config->add_nonaccented_field) {
				$t->name_noaccent = remove_accents($line[2]);
			}
			switch ($f) {
				case 'meso':
					$parent_len = 2;
					$t->type = 'substate_region';
				break;
				case 'munic':
					$parent_len = 4;
					$t->type = 'city';
				break;
			}
			$t->parent_id = substr($t->id,0,$parent_len);
			$territories[$t->id] = $t;
		}
	}
	$first_childs = array();
	if ($config->add_first_childs) {
		foreach ($territories as $id=>$territory) {
			switch (strlen($id)) {
				case 7:
					if (!isset($first_childs[substr($id,0,4)])) {
						$first_childs[substr($id,0,4)] = array();
					}
					$first_childs[substr($id,0,4)][] = $id;
				break;
				case 4:
					if (!isset($first_childs[substr($id,0,2)])) {
						$first_childs[substr($id,0,2)] = array();
					}
					$first_childs[substr($id,0,2)][] = $id;
				break;
				case 2:
					if (!isset($first_childs[substr($id,0,1)])) {
						$first_childs[substr($id,0,1)] = array();
					}
					$first_childs[substr($id,0,1)][] = $id;
				break;
			}
		}
		foreach ($territories as $id=>$territory) {
			$territories[$id]->first_childs = (isset($first_childs[$id])) ? implode(', ',$first_childs[$id]) : "";
		}
	}
	if ($config->add_state) {
		foreach ($territories as $id=>$territory) {
			$territories[$id]->state = (strlen($id)>2) 
				? $territories[substr($id,0,2)]->name 
				: "";
		}
	}
	$territories = array_values($territories);
	if ($config->save_territories_in_csv) {
		if ($config->add_first_childs) {
			$arq_name = "data/Brasil/territories_responsa.csv";
			$fields = array(
				'id', 
				'title', 
				'parent_id', 
				'first_childs'
			);
			if ($config->add_state) {
				$fields[] = 'state';
			}
			if ($config->add_nonaccented_field) {
				$fields[] = 'name_noaccent';
			}
		} else {
			$arq_name = "data/Brasil/territories.csv";
			$fields = array('slug','title','description','parent');
		}
		if ($arq = fopen($arq_name,"w")) {
			fputcsv($arq,$fields);
			$ordered = array();
			$ordered[0] = $ordered[1] = $ordered[2]=$ordered[3]=array();
			foreach ($territories as $loc) {
				switch (strlen($loc->id)) {
					case 1:
						$ordered[0][] = $loc;
					break;
					case 2:
						$ordered[1][] = $loc;
					break;
					case 4:
						$ordered[2][] = $loc;
					break;
					default:
						$ordered[3][] = $loc;
					break;
				}
			}
			ksort($ordered);
			foreach ($ordered as $level) {
				foreach ($level as $item) {
					if ($config->add_first_childs) {
						$values = array(
							$item->id, 
							$item->name, 
							$item->parent_id, 
							$item->first_childs
						);
						if ($config->add_state) {
							$values[] = $item->state;
						}
						if ($config->add_nonaccented_field) {
							$values[] = $item->name_noaccent;
						}
						fputcsv($arq,$values);
					} else {
						fputcsv($arq,array($item->id, $item->name, "", $item->parent_id));
					}
				}
			}
			fclose($arq);
			$r = new StdClass();
			$r->success=1;
			$r->msg = _("File succesfully created!");
			show_alert($r);
		} else {
			$r = new StdClass();
			$r->error=1;
			$r->msg = _("ERROR: I couldn't create the file!")." :-(";
			show_alert($r);
		}
	}
	?>
	<p><a class="btn btn-success btn-sm glyphicon glyphicon-download" href="?action=create_territory_file&save_csv=1"> Save CSV file</a></p>
	<?php
	show_table($territories);
	fclose ($handle);
}

function create_territory_sql() {
	global $config;
	$con = connect();
	$file = "territories_for_sql_with_subdistritos";
	$territories = array();
	$handle = fopen ("data/Brasil/$file.csv","r");
	fgetcsv($handle, 1000, ",");
	while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$t = new StdClass();
		$t->_id = trim($line[0]);
		$t->title = trim($line[1]);
		//$t->title_noaccent = remove_accents($t->title);
		$t->alias = remove_accents(trim($line[2]));
		$t->parent_id = trim($line[3]);
		$t->lat = trim($line[4]);
		$t->lng = trim($line[5]);
		switch (strlen($t->_id)) {
			case 1:
				$t->type = 'region';
			break;
			case 2:
				$t->type = 'state';
			break;
			case 4:
				$t->type = 'substate_region';
			break;
			case 7:
				$t->type = 'city';
			break;
			case 11:
				$t->type = 'subcity_region';
				if (isset($territories[substr($t->_id,0,7)]) && !$territories[substr($t->_id,0,7)]->parent_id) {
					$territories[substr($t->_id,0,7)]->parent_id = $line[6];
				}
			break;
		}
		$territories[$t->_id] = $t;
	}
	$first_childs = array();
	foreach ($territories as $id=>$territory) {
		switch (strlen($id)) {
			case 11:
				$city_id = substr($id,0,7);
				if (!isset($first_childs[$city_id])) {
					$first_childs[$city_id] = array();
				}
				$first_childs[$city_id][] = $id;
				$label = $territories[$city_id]->title . " / " . $territories[substr($id,0,2)]->alias;
				if ($territory->title != $territories[$city_id]->title) {
					$label .= " - ".$territory->title;
				}
				$territories[$id]->label_noaccent = remove_accents($label);
			break;
			case 7:
				if (!isset($first_childs[$territory->parent_id])) {
					$first_childs[$territory->parent_id] = array();
				}
				$first_childs[$territory->parent_id][] = $id;
			break;
			case 4:
				if (!isset($first_childs[substr($id,0,2)])) {
					$first_childs[substr($id,0,2)] = array();
				}
				$first_childs[substr($id,0,2)][] = $id;
			break;
			case 2:
				if (!isset($first_childs[substr($id,0,1)])) {
					$first_childs[substr($id,0,1)] = array();
				}
				$first_childs[substr($id,0,1)][] = $id;
			break;
		}
	}
	foreach ($territories as $id=>$territory) {
		$territories[$id]->first_childs = (isset($first_childs[$id])) ? implode(', ',$first_childs[$id]) : "";
		$territories[$id]->state = (strlen($id)>2) 
			? $territories[substr($id,0,2)]->alias 
			: "";
	}
	$territories = array_values($territories);
	$arq_name_sql = "data/Brasil/territories_responsa.sql";
	$arq_name_csv = "data/Brasil/territories_responsa.csv";
	$fields = array(
		'_id',
		'type',
		'title',
		'alias',
		'parent_id', 
		'first_childs',
		'lat',
		'lng',
		'state',
		'label_noaccent'
	);
	$h_sql = fopen($arq_name_sql,"w");
	$h_csv = fopen($arq_name_csv,"w");
	if ($h_sql && $h_csv) {
		fputcsv($h_csv,$fields);
		$ordered = array();
		$ordered[0] = $ordered[1] = $ordered[2]=$ordered[3]=array();
		foreach ($territories as $loc) {
			switch (strlen($loc->_id)) {
				case 1:
					$ordered[0][] = $loc;
				break;
				case 2:
					$ordered[1][] = $loc;
				break;
				case 4:
					$ordered[2][] = $loc;
				break;
				default:
					$ordered[3][] = $loc;
				break;
			}
		}
		ksort($ordered);
		$sql_fields = "INSERT INTO territories (".implode($fields,", ").") VALUES ";
		foreach ($ordered as $level) {
			foreach ($level as $item) {
				$values_sql = $values_csv = array();
				foreach ($fields as $field) {
					//if ($con->error) {
						$values_sql[] = "'".str_replace("'","''",trim($item->{$field}))."'";
						$values_csv[] = trim($item->{$field});
					//} else {
					//	$values[] = "'".$con->real_escape_string($item->{$field})."'";
					//}
				}
				$sql = $sql_fields . "(" . implode($values_sql,", ") . ");\n";
				fputcsv($h_csv,$values_csv);
				fwrite($h_sql,$sql);
			}
		}
		fclose($h_sql);
		fclose($h_csv);
		$r = new StdClass();
			$r->success=1;
			$r->msg = _("File succesfully created!");
			show_alert($r);
		} else {
			$r = new StdClass();
			$r->error=1;
			$r->msg = _("ERROR: I couldn't create the file!")." :-(";
			show_alert($r);
		}
	}
	?>
	<?php
	show_table($territories);
	fclose ($handle);
}

?>
