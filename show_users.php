<?php

function show_users() {
	if ($config->isAdmin || $config->valid_actions['show_users']!='admin') {
		if (isset($_REQUEST['layout']) && $_REQUEST['layout']=='table') {
			do_show_users_in_table();
		} else {
			do_show_users_in_map();
		}
		return;
	} elseif (isset($_REQUEST['do_it'])) {
		show_failed_login_alert();
	}
	
	?>
		<h1><?= _("Show users") ?></h1>
		<p><?= _("Here you see all users of Responsa") ?></p>
	<?php
	$actionUrl = '?action=show_users&do_it=1';
	$btn = '<button class="btn btn-lg btn-success" type="submit">'._("Show users").'</a>';
	show_login($actionUrl, $btn);
}

function do_show_users_in_table() {
	global $config;
	?>
	<div class="row" style="margin-bottom:20px">
		<div class="col-lg-12">
			<a href="?action=show_users&layout=map&do_it=1" class="btn btn-success"><?= _("Show results in map") ?></a>
		</div>
	</div>
	<?php
	$params = new StdClass();
	$items = get_users_from_DB($params, "table");
	show_table($items);
}
function do_show_users_in_map() {
	global $config;
	?>
	<div class="row" style="margin-bottom:20px">
		<div class="col-lg-12">
			<a href="?action=show_users&layout=table&do_it=1" class="btn btn-success"><?= _("Show results in table") ?></a>
		</div>
	</div>
	<div class="row">
		<div id="map" class="col-lg-12"></div>
	</div>
	<script>
		var map = L.map('map').setView([-15.7797200, -47.9297200], 4);
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    	maxZoom: 10,
    	id: 'dtygel.mk0l1gn6',
    	accessToken: 'pk.eyJ1IjoiZHR5Z2VsIiwiYSI6IjY4YjRhNjU1NWJhYTU2OGU1YWQ1OTcxYjRjNDcyNzIxIn0.Go-whnL9yIt3rHicQ5gqbA'
}).addTo(map);
		var markers = L.markerClusterGroup({maxClusterRadius:40});
		/*var controlSearch = new L.Control.Search({
			callData: searchByAjax, 
			text:'buscar...', 
			markerLocation: false,
			circleLocation: true,
			zoom: 14
		});
		map.addControl( controlSearch );*/
		
		lc = L.control.locate({
    	follow: true,
    	strings: {
        title: "Mostre minha área!"
    	}
		}).addTo(map);

		map.on('startfollowing', function() {
    	map.on('dragstart', lc._stopFollowing, lc);
		}).on('stopfollowing', function() {
    	map.off('dragstart', lc._stopFollowing, lc);
		});
		
		jQuery.ajax({
  		type: "GET",
  		url: "ajax_get_all_users.php",
  		cache: false,
  		success: function(data){
  			var Points = eval(data);
     		for (var i = 0; i < Points.length; i++) {
					var a = Points[i];
					var marker = L.marker(new L.LatLng(a.lat, a.lng), { title: a.title });
					marker.bindPopup(a.popup);
					markers.addLayer(marker);
				}
				var bounds = markers.getBounds();
				//map.fitBounds(bounds);
				map.addLayer(markers);
  		}
		});
		
		function finishedLoading() {
			console.log('done');
	    loader.className = 'done';
  	  setTimeout(function() {
        loader.className = 'hide';
 		  }, 500);
		}
		
	</script>
	<?php
}


?>
