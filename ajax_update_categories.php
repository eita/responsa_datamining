<?php


if (!isset($_GET['source']) || !isset($_GET['local_id']) || !isset($_GET['category'])) {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("There was nothing to update!");
	return $ret;
}

require("functions.php");
require("config.php");
require("init_external_data.php");

$source = $_GET['source'];
$local_id = $_GET['local_id'];
$category = $_GET['category'];

$found = false;
foreach ($config->assigned_categories[$source] as $i=>$cat) {
	if ($cat->local_id==$local_id) {
		$config->assigned_categories[$source][$i]->category = $category;
		$found = true;
		break;
	}
}
if (!$found) {
	$t = new StdClass();
	$t->local_id = $local_id;
	$t->category = $category;
	$config->assigned_categories[$source][] = $t;
}
$updated = update_categories($config->assigned_categories);

$ret = new StdClass();
if ($updated) {
	$ret->success = true;
	$ret->msg = _("Updates saved!");
} else {
	$ret->error = true;
	$ret->msg = _("There was an error doing the update!");
}
@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);

?>
