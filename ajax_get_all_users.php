<?php

include("functions.php");
include("config.php");
if (check_login()) {
	$params = new StdClass();
	$res = get_users_from_DB($params, "map");
} else {
	$res = new StdClass();
	$res->error = true;
	$res->msg = _("only accessible through login");
}
@header("Content-type: application/json; charset=utf-8");
echo json_encode($res);

?>
