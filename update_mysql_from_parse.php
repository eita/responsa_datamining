<?php

/*header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);*/

// I'm only creating this to update all columns in mysql database from model _Installation, since we lost more than 300 data from users because of misconfiguration of the mysql mirror model.

include("functions.php");
include("config.php");
//include("parse_functions.php");
require("misc/parse-php-sdk/autoload.php");
require("parse_config.php");
date_default_timezone_set('UTC');

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseGeoPoint;
use Parse\ParseClient;

if (check_login()) {
	ParseClient::initialize( $config->parse->app_id, $config->parse->rest_key, $config->parse->master_key );
	ParseClient::setServerURL($config->parse->server_url, '1');
	if (!$con) {
		$con = connect();
	}
	if ($con->error) {
		$ret = new StdClass();
		$ret->error = 1;
		$ret->msg = _("Error").": ".$con->error;
	} else {
		$sql = "SELECT objectId FROM _Installation WHERE userId='' and updatedAt < '2016-11-14' order by createdAt DESC";
	
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$ret = new StdClass();
			$ret->error = (isset($error)) ? $error : 42;
		} else {
			$ret = array();
			while ($row = $tmp->fetch_object()) {
				$query = new ParseQuery("_Installation");
				try {
					$query->get($row->objectId);
					$remote = $query->first();
					$osVersion = $remote->get('osVersion');
					$remote->set('androidVersion', $osVersion);
					try {
						$remote->save();
						$res = new StdClass();
						$res->success = true;
						$res->msg = sprintf(_("Object with ID <i>%s</i> updated!"),$row->objectId);
						$ret[] = $res;
					} catch (ParseException $ex) {
						$res = new StdClass();
						$res->error = true;
						$res->msg = sprintf(_("Failed to update object <i>%s</i>, with error message: <i>%s</i>"),$row->objectId, $ex->getMessage());
						$ret[] = $res;
					}
				} catch (ParseException $ex) {  
					$res = new StdClass();
					$res->error = true;
					$res->msg = sprintf(_("Failed to retrieve object <i>%s</i>, with error message: <i>%s</i>"),$row->objectId, $ex->getMessage());
					$ret[] = $res;
				}
			}
		}
	}
} else {
	$ret = new StdClass();
	$ret->error = true;
	$ret->msg = _("only accessible through login");
}
@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);
?>
