<?php
if (function_exists('check_login')) {
	session_start();
}
/*********************************************/
/*********************************************/
/*********************************************/
/*                 Settings                  */
/*********************************************/
/*********************************************/
/*********************************************/
$config = new StdClass();
$config->debug = false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}



$tmp = php_sapi_name();
$config->isCron = ($tmp == 'cgi-fcgi');
// Language:
//$locale = explode(',',($_SERVER['HTTP_ACCEPT_LANGUAGE']));
//setlocale(LC_ALL, $locale[0]);
$locale = isset($_GET['lang']) ? $_GET['lang'] : 'pt_BR';
setlocale(LC_ALL, $locale.".utf8");
bindtextdomain("responsa", "lang");
textdomain("responsa");
bind_textdomain_codeset("responsa", 'UTF-8');
$config->valid_actions = array(
	'update_sources'=>'admin',
	'assign_territory'=>'admin',
	'create_territory_file'=>'admin',
	'find_duplicates'=>'moderator',
	'show_initiatives'=>'user',
	'show_users'=>'user',
	'assign_categories'=>'moderator',
	'send_remote'=>'admin',
	'create_encontre_import_file'=>'admin',
	'review_initiatives'=>'moderator',
	'edit_published_initiatives'=>'moderator',
	'review_feedbacks'=>'moderator',
	'review_comments'=>'moderator',
	'review_challenge_answers'=>'moderator'
);
$config->import_actions = array(
	'update_sources',
	'assign_territory',
	'create_territory_file',
	'find_duplicates',
	'show_initiatives',
	'assign_categories',
	'send_remote',
	'create_encontre_import_file'
);
include('misc/simplehtmldom_1_5/simple_html_dom.php');
$config->data_sources = array(
	'experiencias_ean'=>'ExperienciasEAN',
	'cirandas'=>'Cirandas',
	'feiras_organicas'=>'FeirasOrganicas',
	'muda'=>'Muda',
	'comsol'=>'RedeComSol',
	'gcrs'=>'GCRs',
	'agrorede'=>'Agrorede',
	'hortas_sp'=>'HortasSP',
	'ecosampa'=>'EcoSampa',
	'organicos_no_campo_e_na_mesa'=>'OrganicosNoPrato',
	//'agricultura_urbana'=>'AgriculturaUrbana',
	'responsa'=>'Responsa'
	//,'fbes'=>'FBES'
);
$config->categories = array(
	'feiras'=>array(_("Organic markets"),'shopping-basket'),
	'grupos-de-consumo-responsavel'=>array(_("Groups of responsible consumption"),'group'),
	'iniciativas-de-economia-solidaria'=>array(_("Initiatives in Solidarity Economy"),'gift'),
	'iniciativas-de-agroecologia'=>array(_("Initiatives in Agroecology"),'leaf'),
	'restaurantes'=>array(_("Restaurants with organic ingredients"),'cutlery'),
	'agricultura-urbana'=>array(_("Urban agriculture"),'key'),
	'coletivos-culturais'=>array(_("Cultural collectives"),'sun-o'),
	'lojas'=>array(_("Responsible consumption stores"),'shopping-cart')
);
$config->default_categories = array(
	'experiencias_ean'=>'iniciativas-de-agroecologia',
	'cirandas'=>'iniciativas-de-economia-solidaria',
	'feiras_organicas'=>'feiras',
	'muda'=>'iniciativas-de-agroecologia',
	'comsol'=>'iniciativas-de-economia-solidaria',
	'gcrs'=>'grupos-de-consumo-responsavel',
	'agrorede'=>'iniciativas-de-agroecologia',
	'hortas_sp'=>'iniciativas-de-agroecologia',
	'ecosampa'=>'iniciativas-de-agroecologia',
	'organicos_no_campo_e_na_mesa'=>'restaurantes',
	//'agricultura_urbana'=>'agricultura-urbana'
	//'fbes'=>'conheca'
);

$config->max_percentage_for_duplicates = 90;

/*********************************************/
/*********************************************/
/*********************************************/
/*                 Parameters                */
/*********************************************/
/*********************************************/
/*********************************************/
if (isset($_GET['action']) && in_array($_GET['action'], array_keys($config->valid_actions))) {
	$config->action = $_GET['action'];

	// Import data from jsons!
	if (in_array($config->action, $config->import_actions)) {
		include "init_external_data.php";
	}

	// Include action!
	include $config->action.".php";
}

if (isset($_GET['data_sources']) && $_GET['data_sources']) {
	$config->sources = explode(',',$_GET['data_sources']);
} else {
	$config->sources = array_keys($config->data_sources);
}

// Sub Menus to update data_sources:
$submenus_update_data_sources = array();
$submenus_update_data_sources[] = array(_("Update all"),"?action=update_sources");
foreach ($config->data_sources as $f=>$v) {
	$submenus_update_data_sources[] = array(
		$v,"?action=update_sources&data_sources=$f"
	);
}
// Sub Menus do assign categories:
$submenus_assign_categories = array();
foreach ($config->data_sources as $f=>$v) {
	$submenus_assign_categories[] = array(
		$v,"?action=assign_categories&do_it=1&data_source=$f"
	);
}
// Main Menu
$config->main_menu = array(
	'home'=>array(_("Home"),"index.php","home","default","moderator"),
	'show_initiatives'=>array(_("See all initiatives"),"?action=show_initiatives","globe","default","moderator"),
	'show_users'=>array(_("See users"),"?action=show_users","user","default","moderator"),
	'update_data_sources_top'=>array(_("Update external data from sources"),"","plus","accordion","admin",$submenus_update_data_sources),
	'assign_territory'=>array(_("Define territories of the initiatives"),"?action=assign_territory","globe","default","admin"),
	'assign_categories'=>array(_("Assign categories"),"?action=assign_categories","flag","default","moderator"),
	'find_duplicates'=>array(_("Find duplicates"),"?action=find_duplicates","eye-open","default","moderator"),
	'send_remote'=>array(_("Send to Responsa"),"?action=send_remote","upload","default","admin"),
	'review_initiatives'=>array(_("Review initiatives"),"?action=review_initiatives","check","default","moderator"),
	'edit_published_initiatives'=>array(_("Edit published initiatives"),"?action=edit_published_initiatives","check","default","moderator"),
	'review_feedbacks'=>array(_("Review feedbacks"),"?action=review_feedbacks","check","default","moderator"),
	'review_comments'=>array(_("Review comments"),"?action=review_comments","check","default","moderator"),
	'review_challenge_answers'=>array(_("Review challenge answers"),"?action=review_challenge_answers","check","default","moderator"),
	//'create_territory_file'=>array(_("Create territories file"),"?action=create_territory_file","globe","default"),
	//'create_encontre_import_file'=>array(_("Create CSV for Encontre's import"),"?action=create_encontre_import_file","download-alt","default")
);

//Fields:
$config->fields = array(
	'local_id'=>_("Local ID"),
	'url'=>_("url"),
	'title'=>_("Title"),
	'description'=>_("Description"),
	'lat'=>_("lat"),
	'lng'=>_("lng"),
	'address'=>_("Address"),
	'zip_code'=>_("Zip Code"),
	'phone1'=>_("Phone number")." 1",
	'phone2'=>_("Phone number")." 2",
	'email'=>_("Email"),
	'website'=>_("Official website"),
	'facebook'=>_("Facebook"),
	'twitter'=>_("Twitter"),
	'city'=>_("City"),
	'state'=>_("State"),
	'country'=>_("Country"),
	'created_at'=>_("Created at"),
	'updated_at'=>_("Updated at"),
	'avatar'=>_("Image"),
	'territory'=>_("Territory"),
	'source'=>_("Source"),
	'sources'=>_("Sources"),
	'details'=>_("Details"),
	'category'=>_("Category"),
	'actions'=>_("Actions"),
	'initiative'=>_("Initiative"),
	'owner'=>_("Owner"),
	'reviewer_internal_comments'=>_("Reviewer internal comments"),
	'name'=>_("Name"),
	'phone'=>_("Phone number")
);

//$config->salt='eitaquebeleza#2016~49fdjjlkjjjs[^ss';
$config->salt = "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3";

/* Limites do Brasil:
Longitude:
Esquerda: 73°59’32”
Direita: 34°47’30”

Latitude:
Acima: 5°16’20”
Abaixo: 33°45’03"
*/

$config->mapDB = array(
	"objectId" => "remote_id",
	"createdAt" => "created_at",
	"updatedAt" => "updated_at",
	"title" => "title",
	"type" => "type",
	"titleSlug" => "title_slug",
	"description" => "description",
	"category" => "category",
	"avatarImage" => "avatar",
	"territoryId" => "territory_id",
	"territoryRegionId" => "territory_region_id",
	"territoryLabel" => "territory_label",
	"address" => "address",
	"zipCode" => "zip_code",
	"geoLocation" => "geolocation",
	"localId" => "local_id",
	"source" => "source",
	"sourceCreatedAt" => "source_created_at",
	"sourceUpdatedAt" => "source_updated_at",
	"sources" => "sources",
	"url" => "url",
	"phone1" => "phone1",
	"phone2" => "phone2",
	"email" => "email",
	"website" => "website",
	"facebook" => "facebook",
	"twitter" => "twitter",
	"countComments" => "count_comments",
	"countFavorites" => "count_favorites",
	"ownerId" => "owner_id",
	"published" => "published",
	"reviewed" => "reviewed",
	"reviewerNotes" => "reviewer_notes",
	"reviewerInternalComments" => "reviewer_internal_comments"
);

$config->fieldsToUpdate = $config->mapDB;
unset($config->fieldsToUpdate['countComments'], $config->fieldsToUpdate['countFavorites'], $config->fieldsToUpdate['reviewerNotes'], $config->fieldsToUpdate['reviewerInternalComments']);
$config->fieldsToUpdate['lat']='lat';
$config->fieldsToUpdate['lng']='lng';



?>
