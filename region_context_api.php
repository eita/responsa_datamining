<?php

include("functions.php");
include("config.php");
include("parse_functions.php");
include("parse_config.php");

$config->debug = false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}

$ret = new StdClass();
if (!isset($_GET['territoryId']) || !$_GET['territoryId'] ) {
	$ret->error = "You didn't send all obligatory fields or the correct model. Bye!";
} else {
	$loc = get_territories();
	$territoryId = $_GET['territoryId'];
	$cityId = substr($territoryId,0,7);
	$regionId = $loc->territories[$territoryId]->region_id;
	
	$territoryName = $loc->territories[$territoryId]->name;
	$cityName = $loc->territories[$cityId]->name;
	
	$con = connect();
	if ($config->debug) {
		pR($territoryName);pR($cityName);
	}
	
	if ($territoryName == $cityName) {
		//We must sum all initiatives in city and in territory:
		$sql = "SELECT COUNT(territoryId) AS qtde FROM ResponsaObject where SUBSTRING(territoryId,1,7)='$cityId' AND TYPE = 'initiative' AND published = '1'";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$initiatives_in_city = $tmp->fetch_assoc();
			$ret->initiatives_in_city = $initiatives_in_city['qtde'];
		}
		// Get all people in city and in subcity itself:
		$sql = "SELECT DISTINCT a.objectId FROM _User a, _Installation b WHERE SUBSTRING(territoryId,1,7)='$cityId' AND a.objectId=b.userId";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$ret->people_in_city = $tmp->num_rows;
		}


	} else {
		// Get initiatives in territory:
		$sql = "SELECT COUNT(territoryId) AS qtde FROM ResponsaObject where territoryId='$territoryId' AND TYPE = 'initiative' AND published = '1'";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$initiatives_in_territory = $tmp->fetch_assoc();
			$ret->initiatives_in_territory = $initiatives_in_territory['qtde'];
		}
		// Get people in territory:
		$sql = "SELECT DISTINCT a.objectId FROM _User a, _Installation b WHERE territoryId='$territoryId' AND a.objectId=b.userId";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$ret->people_in_territory = $tmp->num_rows;
		}
		// Get initiatives in city, except those in subcity itself:
		$sql = "SELECT COUNT(territoryId) AS qtde FROM ResponsaObject where SUBSTRING(territoryId,1,7)='$cityId' AND territoryId!='$territoryId' AND TYPE = 'initiative' AND published = '1'";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$initiatives_in_city = $tmp->fetch_assoc();
			$ret->initiatives_in_city = $initiatives_in_city['qtde'];
		}
		// Get people in city except those in subcity itself:
		$sql = "SELECT DISTINCT a.objectId FROM _User a, _Installation b WHERE SUBSTRING(territoryId,1,7)='$cityId' AND territoryId!='$territoryId' AND a.objectId=b.userId";
		if ($config->debug) {
			pR($sql);
		}
		$res = new StdClass();
		if (!$tmp = $con->query($sql)) {
			$error = sprintf("Error in query '$sql': %s\n", $con->error);
			$success = false;
			$res = new StdClass();
			$res->error = (isset($error)) ? $error : 42;
		} else {
			$ret->people_in_city = $tmp->num_rows;
		}
	}



	// Get initiatives in region, except those in your city:
	$sql = "SELECT COUNT(territoryId) AS qtde FROM ResponsaObject where territoryRegionId='$regionId' AND SUBSTRING(territoryId,1,7)!='$cityId' AND TYPE = 'initiative' AND published = '1'";
	if ($config->debug) {
		pR($sql);
	}
	$res = new StdClass();
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$success = false;
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$initiatives_in_region = $tmp->fetch_assoc();
		$ret->initiatives_in_region = $initiatives_in_region['qtde'];
	}
	
	// Get people in region, except those in territory_id itself:
	$sql = "SELECT DISTINCT a.objectId FROM _User a, _Installation b WHERE a.territoryRegionId='$regionId' AND SUBSTRING(territoryId,1,7)!='$cityId' AND a.objectId=b.userId";
	if ($config->debug) {
		pR($sql);
	}
	$res = new StdClass();
	if (!$tmp = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$success = false;
		$res = new StdClass();
		$res->error = (isset($error)) ? $error : 42;
	} else {
		$ret->people_in_region = $tmp->num_rows;
	}
	
}
	
	@header("Content-type: application/json; charset=utf-8");
	echo json_encode($ret);
?>
