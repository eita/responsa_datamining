<?php


include("functions.php");
include("config.php");
include("parse_config.php");
include("parse_functions.php");

$config->debug = false;

if ($config->debug) {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	error_reporting(E_ERROR | E_PARSE);
	echo 'debug mode';
}


$con = connect();

if (
	(
	!isset($_GET['model']) || !$_GET['model'] || !isset($parseObj->{$_GET['model']}) || 
	($_GET['model']=='Challenge' && (!isset($_GET['userId']) || !$_GET['userId'])) ||
	!isset($_GET['q']) || !$_GET['q'] )) {
	$ret = new StdClass();
	$ret->error = "You didn't send all obligatory fields or the correct model. Bye!";
} else {
	$model = $_GET['model'];
	$q = $_GET['q'];
	
	switch ($model) {
		case 'Challenge':
			$userId = $_GET['userId'];
			$search_fields = "a.title, a.description";
			$return_fields = "a.objectId, a.title, a.createdAt, a.description, a.territoryId, a.territoryLabel, b.name authorName, b.objectId authorId, a.relevance, a.answerCount, a.upVotes, a.correctAnswerId, d.description as correctAnswerDescription, c.favorite, c.vote";
			$rank_fields = array();
			$from = " LEFT JOIN ChallengeUserJoin c ON c.challengeId=a.objectId AND c.userId='$userId'";
			$from .= " LEFT JOIN ChallengeAnswer d ON d.objectId=a.correctAnswerId";
			$from .= ", _User b";
			$where = " AND a.published=1 AND a.authorId = b.objectId";
			$orderby = "a.title";
		break;
		case 'initiative':
			$search_fields = "a.title, a.description, a.territoryLabel";
			$return_fields = "a.objectId, a.title, SUBSTR(a.description,1,100) as description, a.category, a.territoryId, a.territoryLabel, a.avatarImage, a.countComments, MATCH (a.title) AGAINST ('".$q."') AS rel1, MATCH (a.description) AGAINST ('".$q."') AS rel2, MATCH (a.territoryLabel) AGAINST ('".$q."') AS rel3";
			$rank_fields = array("rel1", "rel2", "rel3");
			$from = "";
			$where = " AND a.published=1 AND a.type='initiative'";
			$orderby = "(rel1*5)+(rel2*1.3)+(rel3*0.5)";
			$model = "ResponsaObject";
		break;
		case 'gathering':
			$search_fields = "a.title, a.description, a.territoryLabel";
			$return_fields = "a.objectId, a.title, SUBSTR(a.description,1,100) as description, a.territoryId, a.territoryLabel, a.avatarImage, a.countComments, DATE_FORMAT(a.dateTimeStart, '%Y-%m-%dT%TZ') AS 'dateTimeStart', DATE_FORMAT(a.dateTimeFinish, '%Y-%m-%dT%TZ') AS 'dateTimeFinish', a.ownerId, b.name AS ownerName, MATCH (a.title) AGAINST ('".$q."') AS rel1, MATCH (a.description) AGAINST ('".$q."') AS rel2, MATCH (a.territoryLabel) AGAINST ('".$q."') AS rel3";
			$rank_fields = array("rel1", "rel2", "rel3");
			$from = " LEFT JOIN _User b ON a.ownerId = b.objectId";
			$where = " AND a.published=1 AND a.type='gathering' AND a.dateTimeFinish >= CURDATE()";
			$orderby = "(rel1*5)+(rel2*1.3)+(rel3*0.5)";
			$model = "ResponsaObject";
		break;
		case 'ResponsaObject':
			$search_fields = "a.title, a.description, a.territoryLabel";
			$return_fields = "a.objectId, a.title, SUBSTR(a.description,1,100) as description, a.category, a.territoryId, a.territoryLabel, a.avatarImage, a.countComments, a.type, DATE_FORMAT(a.dateTimeStart, '%Y-%m-%dT%TZ') AS 'dateTimeStart', DATE_FORMAT(a.dateTimeFinish, '%Y-%m-%dT%TZ') AS 'dateTimeFinish', MATCH (a.title) AGAINST ('".$q."') AS rel1, MATCH (a.description) AGAINST ('".$q."') AS rel2, MATCH (a.territoryLabel) AGAINST ('".$q."') AS rel3";
			$rank_fields = array("rel1", "rel2", "rel3");
			$from = "";
			$where = " AND a.published=1 AND (a.dateTimeFinish >= CURDATE() OR a.dateTimeFinish IS NULL)";
			$orderby = "(rel1*10)+(rel2*1)+(rel3*1)";
		break;
	}
	
	$sql = "SELECT $return_fields FROM $model a $from WHERE MATCH($search_fields) AGAINST ('+".$q."*' IN BOOLEAN MODE) $where";
	if ($orderby) {
		$sql .= " ORDER BY $orderby DESC";
	}
	$sql .= " LIMIT 20";
	
	if (isset($_GET['show_sql'])) {
		pR($sql);
		exit;
	}
	
	if (!$res = $con->query($sql)) {
		$error = sprintf("Error in query '$sql': %s\n", $con->error);
		$success = false;
		$ret = new StdClass();
		$ret->error = (isset($error)) ? $error : 42;
	} else {
		$ret = array();
		while ($row = $res->fetch_assoc()) {
			$t = $row;
			if ($rank_fields) {
				foreach ($rank_fields as $rank_field) {
					unset($t[$rank_field]);
				}
			}
			if (isset($t['createdAt']) && $t['createdAt']) {
				$createdAt = new \DateTime($row['createdAt']);
				$t['createdAt'] = getParseDateFormat($createdAt);
			}
			if (isset($t['updatedAt']) && $t['updatedAt']) {
				$updatedAt = new \DateTime($row['updatedAt']);
				$t['updatedAt'] = getParseDateFormat($updatedAt);
			}
			$ret[] = $t;
		}
	}
}

@header("Content-type: application/json; charset=utf-8");
echo json_encode($ret);
?>
